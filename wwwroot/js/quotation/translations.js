
function config($translateProvider) {

    $translateProvider
        .translations('es', {
            "Massive Pre Alert Service - Anicam Box Express":"Servicio de Pre Alerta masiva - Anicam Box Express",
            "MASSIVE PRE-ALERT": "PRE ALERTA MASIVA",
            "In this module you can pre-alert massively, please download the example template in excel, complete the information requested in the document and upload it to be processed in the system and receive the results.": "En este modulo podras pre alertar de forma masiva, por favor descarga la plantilla ejemplo en excel, completa la información solicitada en el documento y sube el mismo para luego ser procesado en el sistema y recibir los resultados.",
            "Last files":"Ultimos archivos",
            "Example file.xls":"Excel de ejemplo.xls",
            "Example file consolidate.xls":"Excel de ejemplo para consolidar.xls",
            "Uploaded on":"Cargado el",
            "Requirements":"Requisitos",
            "Validate":"Validar",
            "Validating":"Validando...",
            "Email":"Email",
            "You must validate the email before pre-alert":"Debes validar el email antes de pre alertar",

            "Internal Services":"Servicios Internos",
            "Below you will find the list of services developed for internal and exclusive operations aimed at Anicam staff":"A continuación encontrarás la lista de los servicios desarrollados en para operaciones internas y exclusivas dirigidas a personal de Anicam",
            "Times Used":"Veces utilizado",
            "Go to Module":"Ir al Modulo",

            "Amazon Services":"Servicios Amazon",
            "Tracking Update":"Actualiza Trackings",
            "Update the tracking of a period of time of purchases made in Amazon to the administrator of Anicam":"Actualiza el seguimiento de un periodo de tiempo de las compras realizadas en amazon al administrador de Anicam",
            "Update the tracking of a period of time of purchases made in eBay to the administrator of Anicam":"Actualiza el seguimiento de un periodo de tiempo de las compras realizadas en eBay al administrador de Anicam",

            "Compare Costs":"Compara tus Costos",
            "Compare Amazon costs with the order system registered in Anicam":"Compara y analiza los costos de compras en Amazon con sistema de ordenes registradas en Anicam",

            "SixSixOne Services":"Servicios de Sixsixone",
            "BTI Services":"Servicios de BTI",

            "Ebay Services":"Servicios eBay",
            "Shopping Report":"Reporte de Compras",
            "EBay shopping reports with orders and filters":"Reporte de compras en eBay con órdenes y filtros",

            "Billing Services":"Servicios de Facturación",
            "Costs Report by Guide":"Informe de Costos por Guia",
            "Consult the costs and details of an order by the guide number. This service only applies to Colombia with the logistic operator Red Servi":"Consulta los costos y detalles de un pedido por el número de guía. Este servicio solo se aplica a Colombia con el operador logistico Red Servi",

            "Shipping Quote - RedServi":"Cotización Guia - Redservi",
            "Check the value of a national shipment with the RedServi logistics operator with the option to generate the guide to the order number":"Consulta el valor de un envío nacional con el operador logístico RedServi con opción de generar la guía al número de orden",

            "Pre-Alert Services":"Servicios Pre-alerta",

            "Massive Pre-alerts":"Pre-alertas masivas",
            "Service to pre-alert a bulk number of packages, each package will correspond to an order number":"Servicio para pre-alertar una cantidad por mayor de paquetes, cada paquete correspondera a un numero de orden",
            "Pre purchase alert":"Pre-alerta de compras",
            "Service to pre-alert a wholesale quantity of purchases, each purchase will correspond to an order number":"Servicio para pre-alertar una cantidad por mayor de compras, cada compra correspondera a un numero de orden",

            "Picture Services": "Servicio de imagenes",

            "Product Download":"Descarga de productos",
            "Update Stock":"Actualizar Stock",
            "Download products with description, weights and corresponding measures to an excel":"Descarga a un excel productos con descripcion, pesos y medidas correspondientes",
            "Upload stock products, quantity and corresponding costs to an excel":"Cargue productos en stock, cantidad y costos correspondientes en un Excel",
            "Product Synchronization":"Sincronizar Productos",
            "Synchronize products to B2B with description, weights and corresponding measures to an excel":"Sincroniza productos al B2B con descripcion, pesos y medidas correspondientes",

            "Servientrega Costs and Business Lines":"Costos Servientrega y BL",
            "Compare the costs, total value and obtain the business lines of anicam with the Servientrega reports":"Compara los costos, valor total y obten las lineas de negocio de anicam con los reportes de Servientrega ",

            "Back to Top":"Ir a Arriba"
        })
        .translations('en', {
            "Massive Pre Alert Service - Anicam Box Express":"Massive Pre Alert Service - Anicam Box Express",
            "MASSIVE PRE-ALERT": "MASSIVE PRE-ALERT",
            "In this module you can pre-alert massively, please download the example template in excel, complete the information requested in the document and upload it to be processed in the system and receive the results.": "In this module you can pre-alert massively, please download the example template in excel, complete the information requested in the document and upload it to be processed in the system and receive the results.",
            "Last files":"Last files",
            "Example file.xls":"Example file.xls",
            "Uploaded on":"Cargado el",
            "Requirements":"Requirements",
            "Validate":"Validate",
            "Validating":"Validating...",
            "Email":"Email",
            "You must validate the email before pre-alert":"You must validate the email before pre-alert",

            "Internal Services":"Internal Services",
            "Below you will find the list of services developed for internal and exclusive operations aimed at Anicam staff":"Below you will find the list of services developed for internal and exclusive operations aimed at Anicam staff"
        });

    $translateProvider.preferredLanguage('es');
}

angular
    .module('anicamquote')
    .config(config);
