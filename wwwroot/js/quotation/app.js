/**
 * anicamstore.com WebSite Angular Configuration
 *
 */
(function () {
    angular.module('anicamquote', [
        //'ui.router',                    // Routing
        //'oc.lazyLoad',                  // ocLazyLoad
        'ui.bootstrap',                 // Ui Bootstrap
        'pascalprecht.translate',       // Angular Translate
        'ngIdle',                       // Idle timer
        'ngSanitize',                  // ngSanitize
        'ui.select',
        'angularFileUpload'
    ])

})();

// Other libraries are loaded dynamically in the config.js file using the library ocLazyLoad