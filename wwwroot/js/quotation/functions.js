var addCart = function (asin,quantity,v)
{
    angular.element('#MainCtrl').scope().addToCart(asin,quantity,v);
};



var General = function() {
    "use strict";

    var toReda = function () {
        $('a[data-id=acnt_]').click(function () {
            angular.element('#MainCtrl').scope().toLogOutConfirm();
        });
    };

//function to return the querystring parameter with a given name.
    var getParameterByNamer = function(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };

// function to allow a button or a link to open a tab
var runShowTab = function(e) {
    if ($(".show-tab").length) {
        $('.show-tab').on('click', function(e) {
            e.preventDefault();
            var tabToShow = $(this).attr("href");
            if ($(tabToShow).length) {
                $('a[href="' + tabToShow + '"]').tab('show');
            }
        });
    }
    if (getParameterByNamer('tabId').length) {
        $('a[href="#' + getParameterByNamer('tabId') + '"]').tab('show');
    }
};
return {
    //main function to initiate template pages
    init: function() {
        runShowTab();
        toReda()
    }
}
}();