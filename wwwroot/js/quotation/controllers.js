/**
 * anicamstore.com Web Site Angular
 *
 * Main controller.js file
 * Define controllers with data used in Inspinia theme
 *
 *
 * Functions (controllers)
 *  - productDetailController
 *
 *
 */

/**
 * productDetailController - controller
 * Contains data to get
 *
 */
function MainCtrl($scope, $api, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize) {

    $scope.errorWs = false;
    $scope.qOptions = $rellSelect(10);
    $scope.qCategories=[];
    $scope.qCouriers=[];
    $scope.product=[];
    $scope.type=[];
    $scope.terminate=false;
    $scope.btnVal='Validar';

    $scope.user={
        user_id: $globalParameters.user_id
    };

    $scope.aditionals = {
        "email" : ""
    };

    $scope.noCity=false;
    $scope.noState=false;
    $scope.privacyLink="https://goo.gl/Ze8S5Q";
    var country_locale='CO';

    if ($globalParameters.business_line_id==="59E4B193-4216-4C58-944D-4BD33A77EA02" || $scope.user.user_id==="40F9E230-E43E-4A9B-9D1F-6B245EC100A5"){
        country_locale='AR';
        $scope.privacyLink="https://drive.google.com/file/d/1uE3W6L70iJSOVYFggeNTgppK4vQEKsxZ/view";
    }

    const dataT = {
        username: $globalParameters.api_client,
        password: $globalParameters.api_pswd,
        grant_type: $globalParameters.api_grant_type
    };
    $api.call('token','POST',dataT,true,true).then(function (rt){

        var data = {
            token: rt.access_token,
            country:country_locale
        };

        $api.getCategory(data).then(function(r) {
            $.each(r, function(index, value){
                //($scope.qCategories).push({name : value.description, value : value.code, iva : value.tax, ivaHtml : '<strong>IVA : </strong>'+value.tax+ '%', arancel : value.tariff, arancelHtml : '<strong>Arancel : </strong>'+value.tariff+ '%'});
                // ($scope.qCategories).push({name : value.Description, value : value.ProductTypeName, iva : value.Iva, ivaHtml : '<strong>IVA : </strong>'+value.Iva+ '%', arancel : value.tariff, arancelHtml : '<strong>Arancel : </strong>'+value.tariff+ '%'});
                ($scope.qCategories).push({name : value.description, value : value.code, iva : value.tax, ivaHtml : '<strong>IVA : </strong>'+value.tax+ '%', arancel : value.tariff, arancelHtml : '<strong>Arancel : </strong>'+value.tariff+ '%'});
            });
        }, function() {
            // error
            return $scope.failWsConsume();
        });

        $api.getCarrier({country_code: country_locale}).then(function (rs) {

            $.each(rs, function(index, value){
                $scope.qCouriers.push({name:value.description, value:value.code});
            });

        }, function() {
            // error
            return $scope.failWsConsume();
        });

    }, function() {
        // error
        return $scope.failWsConsume();
    });




    //If AR Country
    if (country_locale==='AR'){


        $scope.qUnidadesPeso = [
            {name:'Libras Americanas', value:'lb'},
            {name:'Kilos', value:'kl'}
        ];
        $scope.qUnidadesMedida = [
            {name:'Pulgadas', value:'in'},
            {name:'Centimetros', value:'cm'}
        ];
        $scope.quote = {
            unidadPeso : "kl",
            unidadMedida : "cm",
            country : country_locale,
            iva : 19,
            countryOrg : "US",
            stateOrg : "FL",
            cityOrg : "555324",
            business_line_id : $globalParameters.business_line_id
        };

        $scope.qProducts = [{value: 1}];

        $scope.product[0] = {
            unidadPeso : "kl",
            unidadMedida : "cm",
            weight: null,
            length: null,
            width: null,
            height: null,
            category : {
                name: "Envíos de entrega rápida o mensajería expresa",
                value: "9807.20.00.00",
                iva: 0,
                ivaHtml : '<strong>IVA : </strong>0%',
                arancel : 10,
                arancelHtml : '<strong>Arancel : </strong>10%'}
        };

        $scope.type[0]={
            weight : "kl",
            measure : 'cm'
        };


    }
    else if(country_locale==='CO'){

        $scope.qUnidadesPeso = [
            {name:'Libras Americanas', value:'lb'},
            {name:'Kilos', value:'kl'}
        ];
        $scope.qUnidadesMedida = [
            {name:'Pulgadas', value:'in'},
            {name:'Centimetros', value:'cm'}
        ];
        $scope.quote = {
            unidadPeso : "lb",
            unidadMedida : "in",
            country : country_locale,
            iva : 19,
            countryOrg : "US",
            stateOrg : "FL",
            cityOrg : "555324",
            business_line_id : $globalParameters.business_line_id
        };


        $scope.qProducts = [{value: 1}];

        $scope.product[0] = {
            unidadPeso : "lb",
            unidadMedida : "in",
            weight: null,
            length: null,
            width: null,
            height: null,
            category : {
                name: "Envíos de entrega rápida o mensajería expresa",
                value: "9807.20.00.00",
                iva: 19,
                ivaHtml : '<strong>IVA : </strong>19%',
                arancel : 10,
                arancelHtml : '<strong>Arancel : </strong>10%'}
        };

        $scope.type[0]={
            weight : "lb",
            measure : 'in'
        };

    }

    $scope.qBusiness = [
        {name:'DHL', value:'DHL'},
        {name:'FEDEX', value:'FEDEX'},
        {name:'UPS', value:'UPS'},
        {name:'USPS', value:'USPS'},
        {name:'OTHER', value:'OTHER'}
    ];

    $scope.qReferences = [
        {name:'Envelope', value:'ENVELOPE'},
        {name:'Pallet', value:'PALLET'},
        {name:'Box', value:'BOX'},
        {name:'Crate', value:'CRATE'},
        {name:'Others', value:'OTHER'}
    ];



    $scope.total = {
        declare : 0,
        declareLocal : 0,
        duties : 0,
        logistic_us : 0,
        taxes : 0,
        handling : 0,
        insurance : 0,
        merchan_service : 0,
        local_delivery : 0,
        national_delivery : 0,
        redespachos : 0,
        total : 0
    };

    $scope.f= {
        name: 'Ningun archivo seleccionado!'
    };

    $scope.fBill= {
        name: 'Ninguna factura seleccionada!'
    };

    $api.getTRM().then(function(r) {
        r = parseFloat(r).toFixed(2);
        $scope.quote.trm = r;

    }, function() {
        // error
        return $scope.failWsConsume();
    });

    $api.getCountries().then(function(r) {
        $scope.qCountries = [];
        $.each(r, function(index, value){
            ($scope.qCountries).push({name : value.name, value : value.locale});
        });
    }, function() {
        // error
        return $scope.failWsConsume();
    });



    $scope.qProducts = [{value: 1}];


    /*$scope.uploadFiles = function(file, errFiles) {
        $scope.f = file;
        console.log(errFiles);
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
            file.upload = Upload.upload({
                url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
                headers: {
                    'Content-Type' : 'image/jpeg'
                },
                method: 'POST',
                data:{file:file}
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                });
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 *
                    evt.loaded / evt.total));
            });
        }
    };*/


    $scope.alert= {
        business_line_id: $globalParameters.business_line_id,
        carrier: "",
        external_guide: "",
        tracking: "",
        url_attached: "",
        category_product: "Pre alertado por anicamboxexpress.com",
        order_type: $globalParameters.order_type,
        declare_value : 0,
        comment: "",
        notification:true,
        store: false,
        sandbox:false,
        place:3,
        order_references: [],
        order_packages:[],
        order_costs:[]
    };


    $api.getStates({country_code : $scope.quote.country}).then(function(r) {
        $scope.qStates = [];
        if(r[0]){
            var rState = r[0].states;
            $.each(rState, function(index, value){
                ($scope.qStates).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.id+')'});
            });
        }else{
            $api.getStates({country_code : $scope.quote.country}).then(function(r) {
                $scope.qStates = [];
                if(r[0]){
                    var rState = r[0].states;
                    $.each(rState, function(index, value){
                        ($scope.qStates).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.id+')'});
                    });
                }else{
                    return $scope.failWsConsume();
                }
            }, function() {
                // error
                return $scope.failWsConsume();
            });
        }
    }, function() {
        // error
        return $scope.failWsConsume();
    });

    $scope.qCities = [];

    $scope.updateStates = function (item) {

        const dataT = {
            username: $globalParameters.api_client,
            password: $globalParameters.api_pswd,
            grant_type: $globalParameters.api_grant_type
        };
        $api.call('token','POST',dataT,true,true).then(function (rt){

            var data = {
                token: rt.access_token,
                country:item
            };

            if($scope.alert.shipping !== undefined){
                $scope.alert.shipping[0].country_locale=item;
                console.log($scope.alert);
            }

            $api.getCategory(data).then(function(r) {
                $scope.qCategories=[];
                if(r.length>0){

                    $.each(r, function(index, value){
                        ($scope.qCategories).push({name : value.description, value : value.code, iva : value.tax, ivaHtml : '<strong>IVA : </strong>'+value.tax+ '%', arancel : value.tariff, arancelHtml : '<strong>Arancel : </strong>'+value.tariff+ '%'});
                    });
                }

                $scope.qStates = [];
                $scope.qCities = [];
                $api.getStates({country_code : data.country}).then(function(r) {

                    if(r===null||r===""||r===undefined){
                        $scope.noState=true;
                    }else{
                        var rState = r[0].states;
                        if (rState.length === 0){
                            $scope.noState = true;
                            $scope.noCity=true;
                        }else{
                            $scope.noState = false;
                            $scope.noCity=false;
                            $.each(rState, function(index, value){
                                ($scope.qStates).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.id+')'});
                            });
                        }
                    }

                }, function() {
                    // error
                    $scope.noState = false;
                    $scope.noCity=false;
                    //return $scope.failWsConsume();
                });

            }, function() {
                // error
                return $scope.failWsConsume();
            });
        }, function() {
            // error
            return $scope.failWsConsume();
        });


    };

    $scope.updateCities = function (item) {
        $scope.qCities = [];
        if(item !== undefined){

            console.log("Este es el item de city ", item);

            item = item.split("(");
            item = item[1].split(")");

            $api.getCities({state_code : item[0]}).then(function(r) {

                if(r===null||r===""||r===undefined){
                    $scope.noCity=true;
                }else{
                    var rCity = r[0].cities;
                    if (rCity.length === 0){
                        $scope.noCity=true;
                    }else{
                        $scope.noCity=false;
                        $.each(rCity, function(index, value){
                            ($scope.qCities).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.code+')'});
                        });
                    }
                }


            }, function() {
                // error
                return $scope.failWsConsume();
            });
        }
    };


    $scope.CurrentDate = new Date();

    $scope.changeIVA = function (obj,$event) {
        var currentElement = obj.target;
        console.log(obj.$select.selected);
        console.log($scope.product[0].category);
        //$scope.alert.iva = parseInt(val.iva);
    };

    // Add product pre alert of quotation
    $scope.addProduct = function () {

        const cont = $scope.qProducts.length;
        $scope.qProducts.push({value: cont+1});

        if(country_locale==='AR'){

            $scope.product[cont] = {
                unidadPeso : "kl",
                unidadMedida : "cm",
                weight: null,
                length: null,
                width: null,
                height: null,
                category : {
                    name: "Envíos de entrega rápida o mensajería expresa",
                    value: "9807.20.00.00",
                    iva: 0,
                    ivaHtml : '<strong>IVA : </strong>0%',
                    arancel : 10,
                    arancelHtml : '<strong>Arancel : </strong>10%'}
            };

            $scope.type[cont]={
                weight : "kl",
                measure : 'cm'
            };
        }else if (country_locale==='CO'){
            $scope.product[cont] = {
                unidadPeso : "lb",
                unidadMedida : "in",
                weight: null,
                length: null,
                width: null,
                height: null,
                category : {
                    name: "Envíos de entrega rápida o mensajería expresa",
                    value: "9807.20.00.00",
                    iva: 19,
                    ivaHtml : '<strong>IVA : </strong>19%',
                    arancel : 10,
                    arancelHtml : '<strong>Arancel : </strong>10%'}
            };

            $scope.type[cont]={
                weight : "lb",
                measure : 'in'
            };
        }
    };

    $scope.changeWeight = function (index) {

        if($scope.product[index].unidadPeso==='lb'){
            $scope.type[index].weight = 'lb';
        }else{
            $scope.type[index].weight = 'kl';
        }
    };

    $scope.changeMeasure = function (index) {
        if($scope.product[index].unidadMedida==='in'){
            $scope.type[index].measure = 'in';
        }else{
            $scope.type[index].measure = 'cm';
        }
    };

    $scope.changeCourier = function (val) {
        console.log(val);
    };

    $scope.failWsConsume = function () {

    $scope.errorWs = true;
    $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
    alert($scope.errorWsMsg);

};
    $scope.refresh = function(){
        $window.location.reload();
    };

    $scope.updateRedes = function (val) {

        if(!val){
            $scope.total = {
                declare : 0,
                declareLocal : 0,
                duties : 0,
                logistic_us : 0,
                taxes : 0,
                handling : 0,
                insurance : 0,
                merchan_service : 0,
                local_delivery : 0,
                national_delivery : 0,
                redespachos : 0,
                total : 0
            };
            $scope.loading=true;
            $scope.result=[];
            $scope.disMsg = false;
            $scope.showForm=true;

            var aditional='',body='';

            var dCity = $scope.alert.shipping[0].city;
            if($scope.alert.shipping[0].country_locale==="CO"||$scope.alert.shipping[0].country_locale==="PY"||$scope.alert.shipping[0].country_locale==="CN"||$scope.alert.shipping[0].country_locale==="AR"){
                dCity = dCity.split("(");
                $scope.redesCity = $capitalize(dCity[0]);
                dCity = dCity[1].split(")");
                dCity = dCity[0];
            }else{
                $scope.redesCity=$scope.alert.shipping[0].city;
            }

            var dState = $scope.alert.shipping[0].state;
            if($scope.alert.shipping[0].country_locale==="CO"||$scope.alert.shipping[0].country_locale==="PY"||$scope.alert.shipping[0].country_locale==="CN"||$scope.alert.shipping[0].country_locale==="AR"){
                dState = dState.split("(");
                $scope.redesState = $capitalize(dState[0]);
                dState = dState[1].split(")");
                dState = dState[0];
            }else{
                $scope.redesState =  $scope.alert.shipping[0].state;
            }


            const trm = parseFloat(($scope.quote.trm).replace(",", "")).toFixed(2);

            $.each($scope.qProducts, function(index, value){
                $scope.result[index]={
                    duties:0,
                    logistic_us:0,
                    taxes:0,
                    duties_taxes:0,
                    handling:0,
                    insurance:0,
                    merchan_service:0,
                    local_delivery:0,
                    local_redservi:0,
                    local_servientrega:0,
                    local_envia:0,
                    local_dhl:0,
                    national_delivery:0,
                    total:0
                };

                var priceDeclarate = parseFloat(($scope.product[index].declare_value).replace(",", "")).toFixed(2);  // Pendiente si se suma el handling


                const iva=0;
                $scope.iva =0;

                var weightLb=0,weightKl=0,measurePl={},measureCm={};
                if($scope.product[index].unidadPeso==='lb'){
                    weightLb = $scope.product[index].weight; weightKl = $scope.product[index].weight*0.453592;
                }else if ($scope.product[index].unidadPeso==='kl'){
                    weightLb = $scope.product[index].weight*2.20462; weightKl = $scope.product[index].weight;
                }

                if($scope.product[index].unidadMedida==='in'){
                    measurePl = {
                        length : $scope.product[index].length,
                        height : $scope.product[index].height,
                        width : $scope.product[index].width
                    };
                    measureCm = {
                        length : $scope.product[index].length*2.54,
                        height : $scope.product[index].height*2.54,
                        width : $scope.product[index].width*2.54
                    }
                }else if ($scope.product[index].unidadMedida==='cm'){
                    measurePl = {
                        length : $scope.product[index].length*0.393701,
                        height : $scope.product[index].height*0.393701,
                        width : $scope.product[index].width*0.393701
                    };
                    measureCm = {
                        length : $scope.product[index].length,
                        height : $scope.product[index].height,
                        width : $scope.product[index].width
                    }
                }


                var weight = (weightKl).toFixed(2); // Peso de libras a kilos
                var length = (measureCm.length).toFixed(2);  // Medidas de pulgadas a cm
                var height = (measureCm.height).toFixed(2);  // Medidas de pulgadas a cm
                var width = (measureCm.width).toFixed(2);  // Medidas de pulgadas a cm

                var dataSC = {
                    product_asin : '',
                    product_weight : weightLb,
                    product_type : $scope.product[index].category.value,
                    product_price : $scope.product[index].declare_value.replace(",", ""),
                    product_length: measurePl.length,  // Largo
                    product_height: measurePl.height,  // Alto
                    product_width:  measurePl.width,  //Ancho
                    seller_prefix : '049'
                };


                const dataT = {
                    username: $globalParameters.api_client,
                    password: $globalParameters.api_pswd,
                    grant_type: $globalParameters.api_grant_type
                };
                $api.call('token','POST',dataT,true,true).then(function (rt){

                    var dataS = {
                        token:rt.access_token,
                        tariff: $scope.product[index].category.value,
                        weight_lb: weightLb,
                        height_in: measurePl.height,
                        length_in: measurePl.length,
                        width_in: measurePl.width,
                        declare_value: $scope.product[index].declare_value.replace(",", ""),
                        delivery_country_locale: $scope.quote.country,
                        user_id: $scope.user_id,
                        place: $globalParameters.place_default
                    };

                    console.log('Part one...', dataS);

                    $api.shippingCosts(dataS).then(function(response) {

                        const r = (response.d);

                        if(r.status ===200){

                            $scope.result[index].duties=r.response.duties_tariff;
                            $scope.result[index].logistic_us=r.response.logistic_us;
                            $scope.result[index].taxes=r.response.iva_vat;
                            $scope.result[index].duties_taxes=r.response.duties_tariff+r.response.iva_vat;

                            $scope.result[index].handling=r.response.handling_custome;
                            $scope.result[index].insurance=r.response.insurance;
                            $scope.result[index].merchan_service=r.response.merchan_service;

                            if (dState === '1' || dState === '2'){
                                aditional = '0';
                            }


                            body = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="/distribucion/webservices/ws_cotizadorEnvios.php" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">' +
                                '<soapenv:Header/>' +
                                '<soapenv:Body>' +
                                '<ws:WebServiceCotizadorEnvios.ws_cotizarEnvio soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">' +
                                '<user xsi:type="xsd:string">'+$globalParameters.user_redservi+'</user>' +
                                '<password xsi:type="xsd:string">'+$globalParameters.pswd_redservi+'</password>' +
                                '<DTOCotizadorEnvios xsi:type="ws:DTOCotizadorEnvios">' +
                                '<codCliente xsi:type="xsd:string">'+$globalParameters.nit_cliente_redservi+'</codCliente>' +
                                '<idModoTransporte xsi:type="xsd:string">'+$globalParameters.mt_redservi+'</idModoTransporte>' +
                                '<idFormaPago xsi:type="xsd:string">'+$globalParameters.fp_redservi+'</idFormaPago>' +
                                '<codPoblacionOrigen xsi:type="xsd:string">'+$globalParameters.origen_redservi+'</codPoblacionOrigen>' +
                                '<codPoblacionDestino xsi:type="xsd:string">'+String(aditional)+String(dCity)+'</codPoblacionDestino>'+
                                '<ListaUnidadesDeclaradas xsi:type="ws:ArrayListUnidadesEnvioDeclaradas" soapenc:arrayType="ws:DTOUnidadEnvioDeclarada[]">';



                            //console.log(value);
                            body += '<DTOUnidadEnvioDeclarada>'+
                                '<cantidad xsi:type="xsd:string">'+ String("1") +'</cantidad>'+
                                '<largoCm xsi:type="ws:string">'+ String(length) +'</largoCm>'+
                                '<altoCm xsi:type="xsd:string">'+ String(height) +'</altoCm>'+
                                '<anchoCm xsi:type="xsd:string">'+ String(width) +'</anchoCm>'+
                                '<pesoKilos xsi:type="xsd:string">'+ String(weight) +'</pesoKilos>'+
                                '<valorDeclarado xsi:type="xsd:string">'+ String(parseFloat((priceDeclarate+iva)*trm).toFixed(2)) +'</valorDeclarado>'+
                                '</DTOUnidadEnvioDeclarada>';


                            body += '</ListaUnidadesDeclaradas>' +
                                '</DTOCotizadorEnvios>' +
                                '</ws:WebServiceCotizadorEnvios.ws_cotizarEnvio>' +
                                '</soapenv:Body>' +
                                '</soapenv:Envelope>';

                            //console.log($parseXML(body));

                            $api.quotationShipments(body).then(function(response) {

                                var res = $parseXML(response);
                                // console.log(res);
                                var valCod = res.getElementsByTagName("codigo");

                                if (valCod[0].textContent === '0') {
                                    $scope.fail=false;
                                    $scope.redLocal=true;

                                    var arniveles = res.getElementsByTagName("ValorCobradoTotal");

                                    $scope.result[index].local_delivery=parseFloat((arniveles[0].textContent)*1.25)/trm;

                                    $scope.result[index].total=parseFloat($scope.result[index].duties_taxes)+parseFloat($scope.result[index].handling)+parseFloat($scope.result[index].insurance)+
                                        parseFloat($scope.result[index].merchan_service)+parseFloat($scope.result[index].logistic_us);

                                    $scope.result[index].total+=parseFloat($scope.result[index].local_delivery);


                                    $scope.total.declare += $scope.product[index].declare_value;
                                    $scope.total.declareLocal += $scope.result[index].total*trm;
                                    $scope.total.duties += $scope.result[index].duties;
                                    $scope.total.logistic_us += $scope.result[index].logistic_us;
                                    $scope.total.taxes += $scope.result[index].taxes;
                                    $scope.total.handling += $scope.result[index].handling;
                                    $scope.total.insurance += $scope.result[index].insurance;
                                    $scope.total.merchan_service += $scope.result[index].merchan_service;
                                    $scope.total.local_delivery += $scope.result[index].local_delivery;
                                    $scope.total.national_delivery += 0;
                                    $scope.total.redespachos += $scope.result[index].local_delivery + $scope.result[index].national_delivery;
                                    $scope.total.total += $scope.result[index].total;

                                    $scope.loading=false;
                                    $scope.terminate=true;

                                }else {
                                    $scope.fail=true;
                                    $scope.loading=false;
                                    $scope.terminate=true;
                                    $scope.disMsg = true;
                                }

                            }, function() {
                                // error
                                return $scope.failWsConsume();
                            });

                        }

                    }, function() {
                        // error
                        return $scope.failWsConsume();
                    });

                }, function() {
                    // error
                    return $scope.failWsConsume();
                });


            });
        }

    };



    $scope.changequoteCity = function(city,state){

        if($scope.alert.shipping !== undefined){
            $scope.alert.shipping[0].city=city;
            $scope.alert.shipping[0].state=state;
            console.log($scope.alert);
        }

    };

    $scope.submitQuoteForm = function(isValid) {
        console.log($scope.quote.city.indexOf('('));
        $scope.submitted = true;
        // check to make sure the form is completely valid
        if (isValid) {

            $scope.total = {
                declare : 0,
                custome_dutie_fee : 0,
                declareLocal : 0,
                duties : 0,
                logistic_us : 0,
                taxes : 0,
                handling : 0,
                insurance : 0,
                merchan_service : 0,
                local_delivery : 0,
                local_redservi: 0,
                local_servientrega: 0,
                local_envia: 0,
                local_dhl: 0,
                national_delivery : 0,
                redespachos : 0,
                total : 0
            };

            $scope.redLocal=false;
            $scope.carriers={
                redservi:false,
                servient:false,
                envia:false
            };
            $scope.loading=true;
            $scope.result=[];
            var aditional='',body='';

            var dCity = $scope.quote.city;
            var dState = $scope.quote.state;

            if(dState.indexOf('(')!==(-1) && dState.indexOf(')')!==(-1)){
                dCity = dCity.split("(");
                $scope.redesCity = $capitalize(dCity[0]);
                dCity = dCity[1].split(")");
                dCity = dCity[0];

                dState = dState.split("(");
                $scope.redesState = $capitalize(dState[0]);
                dState = dState[1].split(")");
                dState = dState[0];

            }else{
                $scope.redesCity=$scope.quote.city;
                $scope.redesState = $scope.quote.state;
            }

            if($scope.quote.country==="AR" && dState==="56"){
                $scope.redLocal=true;
            }

            const trm = parseFloat(($scope.quote.trm).replace(",", "")).toFixed(2);

            $.each($scope.qProducts, function(index, value){


                $scope.result[index]={
                    duties:0,
                    custome_dutie_fee:0,
                    logistic_us:0,
                    taxes:0,
                    duties_taxes:0,
                    handling:0,
                    insurance:0,
                    merchan_service:0,
                    local_delivery:0,
                    local_redservi: 0,
                    local_servientrega: 0,
                    local_envia: 0,
                    local_dhl: 0,
                    national_delivery:0,
                    total:0
                };

                var declaredValue = $scope.product[index].declare_value;  // Pendiente si se suma el handling
                declaredValue = parseFloat(declaredValue);

                const iva=0;
                $scope.iva =0;

                var weightLb=0,weightKl=0,measurePl={},measureCm={};

                if($scope.product[index].unidadPeso==='lb' && $scope.quote.country === "AR"){

                    // weightLb = $scope.product[index].weight*2;
                    weightLb = $scope.product[index].weight*454;

                    // weightKl = ($scope.product[index].weight*0.445);
                    weightKl = $scope.product[index].weight*0.45;

                }else if($scope.product[index].unidadPeso==='lb'){
                    weightLb = $scope.product[index].weight; weightKl = $scope.product[index].weight*0.453592;
                }else if($scope.product[index].unidadPeso==='kl' && $scope.quote.country === "AR"){

                   /* if( (($scope.product[index].weight.toFixed(2)-$scope.product[index].weight.toFixed()) > 0.00) && (($scope.product[index].weight.toFixed(2)-$scope.product[index].weight.toFixed()) < 0.50) ){
                        weightLb = (($scope.product[index].weight)+(0.50-($scope.product[index].weight.toFixed(2)-$scope.product[index].weight.toFixed())))-0.25;
                        console.log(weightLb);
                    }else{
                        weightLb = $scope.product[index].weight.toFixed(2);
                    }*/

                    weightLb=$scope.product[index].weight*1000;
                    weightLb=parseFloat(weightLb);
                    console.log(weightLb);

                    /*weightLb = (weightLb*4); */

                    weightKl = $scope.product[index].weight;

                } else if ($scope.product[index].unidadPeso==='kl'){
                    weightLb = $scope.product[index].weight*2.20462; weightKl = $scope.product[index].weight;
                }

                if($scope.product[index].unidadMedida==='in'){
                    measurePl = {
                        length : $scope.product[index].length,
                        height : $scope.product[index].height,
                        width : $scope.product[index].width
                    };
                    measureCm = {
                        length : $scope.product[index].length*2.54,
                        height : $scope.product[index].height*2.54,
                        width : $scope.product[index].width*2.54
                    }
                }else if ($scope.product[index].unidadMedida==='cm'){
                    measurePl = {
                        length : $scope.product[index].length*0.393701,
                        height : $scope.product[index].height*0.393701,
                        width : $scope.product[index].width*0.393701
                    };
                    measureCm = {
                        length : $scope.product[index].length,
                        height : $scope.product[index].height,
                        width : $scope.product[index].width
                    }
                }


               /* var weight = (weightKl).toFixed(2); // Peso de libras a kilos
                var length = (measureCm.length).toFixed(2);  // Medidas de pulgadas a cm
                var height = (measureCm.height).toFixed(2);  // Medidas de pulgadas a cm
                var width = (measureCm.width).toFixed(2);  // Medidas de pulgadas a cm*/



                var dataSC = {
                    product_asin : '',
                    product_weight : weightLb,
                    product_type : $scope.product[index].category.value,
                    product_price : declaredValue,
                    product_length: measurePl.length,  // Largo
                    product_height: measurePl.height,  // Alto
                    product_width:  measurePl.width,  //Ancho
                    seller_prefix : '049'
                };


                const dataT = {
                    username: $globalParameters.api_client,
                    password: $globalParameters.api_pswd,
                    grant_type: $globalParameters.api_grant_type
                };
                $api.call('token','POST',dataT,true,true).then(function (rt){

                    if($scope.aditionals.email==="" && $scope.quote.country==='EC'){
                        $scope.user.user_id="246C88D6-70ED-41FF-9BFF-72B5B1985A0F";
                    }
                    /*console.log($scope.aditionals);
                    console.log($scope.user);
                    console.log(weightLb);*/

                    console.log('este es el peso dado ...', weightLb.toFixed(2));

                    var place = $globalParameters.place_default;

                    if($scope.quote.country==="PE") {
                        place = 8;
                    }

                    var dataS = {
                        token: rt.access_token,
                        tariff: $scope.product[index].category.value,
                        weight_lb: weightLb.toFixed(2),
                        height_in: measurePl.height.toFixed(2),
                        length_in: measurePl.length.toFixed(2),
                        width_in: measurePl.width.toFixed(2),
                        declare_value: $scope.product[index].declare_value,
                        delivery_country_locale: $scope.quote.country,
                        delivery_state: $scope.redesState,
                        delivery_city: $scope.redesCity,
                        user_id: $scope.user.user_id,
                        place: place
                    };

                    console.log('Part two...', dataS);
                    console.log($scope.quote);

                    $api.shippingCosts(dataS).then(function(response) {

                        const r = (response.d);

                        if(r.status ===200){


                            $scope.result[index].duties=r.response.duties_tariff;
                            $scope.result[index].custome_dutie_fee=r.response.custome_dutie_fee;
                            $scope.result[index].logistic_us=r.response.logistic_us;
                            $scope.result[index].taxes=r.response.iva_vat;
                            $scope.result[index].duties_taxes=r.response.duties_tariff+r.response.iva_vat;


                          /*  if($scope.quote.country === "AR" && $scope.product[index].unidadPeso==='lb' && (r.response.item_weight/2) >= 22 && (r.response.item_weight/2) < 44){
                                $scope.result[index].handling=r.response.handling_custome+10;
                            }else if ($scope.quote.country === "AR" && $scope.product[index].unidadPeso==='kl' && (r.response.item_weight/2) >= 20 && (r.response.item_weight/2) < 40){
                                $scope.result[index].handling=r.response.handling_custome+10;
                            } else if ($scope.quote.country === "AR" && $scope.product[index].unidadPeso==='lb' && (r.response.item_weight/2) >= 44){
                                $scope.result[index].handling=r.response.handling_custome+20;
                            }else if ($scope.quote.country === "AR" && $scope.product[index].unidadPeso==='kl' && (r.response.item_weight/2) >= 40){
                                $scope.result[index].handling=r.response.handling_custome+20;
                            } else{*/
                                $scope.result[index].handling=r.response.handling_custome;
                            /*}*/

                            $scope.result[index].insurance=r.response.insurance;
                            $scope.result[index].merchan_service=r.response.merchan_service;

                            var dataQuote={
                                delivery_country: $scope.quote.country,
                                city_code: dCity,
                                items: [{
                                    quantity: 1,
                                    length: parseFloat(measurePl.length.toFixed(2)),
                                    height: parseFloat(measurePl.height.toFixed(2)),
                                    width: parseFloat(measurePl.width.toFixed(2)),
                                    weight: parseFloat(weightLb.toFixed(2)),
                                    declare_Value: dataS.declare_value
                                }]
                            };

                            if($scope.quote.country==="CO"){

                               /* if (dState === '1' || dState === '2'){
                                    aditional = '0';
                                }*/

                                $api.quoteRedservi(dataQuote).then(function(rRed) {

                                    if(rRed.status===200){
                                        $scope.redLocal=true;
                                        $scope.carriers.redservi=true;
                                        $scope.result[index].local_redservi=parseFloat(rRed.data.price);
                                        console.log($scope.result[index].local_redservi);
                                    }

                                    $api.quoteEnvia(dataQuote).then(function(rEnv) {

                                        if(rEnv.status===200){
                                            $scope.redLocal=true;
                                            $scope.carriers.envia=true;
                                            $scope.result[index].local_envia=parseFloat(rEnv.data.price);
                                            console.log($scope.result[index].local_envia);
                                        }

                                        $api.quoteServientrega(dataQuote).then(function(rSer) {

                                            if(rSer.status===200){
                                                $scope.redLocal=true;
                                                $scope.carriers.servient=true;
                                                $scope.result[index].local_servientrega=parseFloat(rSer.data.price);
                                                console.log($scope.result[index].local_servientrega);
                                            }

                                            if($scope.redLocal){
                                                $scope.fail=false;
                                                $scope.result[index].total=parseFloat($scope.result[index].duties_taxes)+
                                                    parseFloat($scope.result[index].handling)+
                                                    parseFloat($scope.result[index].insurance)+
                                                    parseFloat($scope.result[index].merchan_service)+

                                                    parseFloat($scope.result[index].logistic_us)+
                                                    parseFloat($scope.result[index].custome_dutie_fee);

                                                //$scope.result[index].total+=parseFloat($scope.result[index].local_delivery);

                                                $scope.total.declare += $scope.product[index].declare_value;
                                                $scope.total.declareLocal += $scope.result[index].total*trm;
                                                $scope.total.duties += $scope.result[index].duties;
                                                $scope.total.custome_dutie_fee += $scope.result[index].custome_dutie_fee;
                                                $scope.total.logistic_us += $scope.result[index].logistic_us;
                                                $scope.total.taxes += $scope.result[index].taxes;
                                                $scope.total.handling += $scope.result[index].handling;
                                                $scope.total.insurance += $scope.result[index].insurance;
                                                $scope.total.merchan_service += $scope.result[index].merchan_service;
                                                $scope.total.local_delivery += $scope.result[index].local_delivery;
                                                $scope.total.local_redservi += $scope.result[index].local_redservi;
                                                $scope.total.local_servientrega += $scope.result[index].local_servientrega;
                                                $scope.total.local_envia += $scope.result[index].local_envia;
                                                $scope.total.local_dhl += $scope.result[index].local_dhl;
                                                $scope.total.national_delivery += 0;
                                                $scope.total.redespachos += $scope.result[index].local_delivery + $scope.result[index].national_delivery;
                                                $scope.total.total += $scope.result[index].total;

                                                $scope.loading=false;
                                                $scope.terminate=true;
                                            }else{
                                                $scope.fail=true;
                                                $scope.loading=false;
                                                $scope.terminate=true;
                                            }

                                        },function(){
                                            $scope.failWsConsume();
                                        });

                                    },function(){
                                       $scope.failWsConsume();
                                    });

                                },function(){
                                    $scope.failWsConsume();
                                });



                         /*       body = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="/distribucion/webservices/ws_cotizadorEnvios.php" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">' +
                                    '<soapenv:Header/>' +
                                    '<soapenv:Body>' +
                                    '<ws:WebServiceCotizadorEnvios.ws_cotizarEnvio soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">' +
                                    '<user xsi:type="xsd:string">'+$globalParameters.user_redservi+'</user>' +
                                    '<password xsi:type="xsd:string">'+$globalParameters.pswd_redservi+'</password>' +
                                    '<DTOCotizadorEnvios xsi:type="ws:DTOCotizadorEnvios">' +
                                    '<codCliente xsi:type="xsd:string">'+$globalParameters.nit_cliente_redservi+'</codCliente>' +
                                    '<idModoTransporte xsi:type="xsd:string">'+$globalParameters.mt_redservi+'</idModoTransporte>' +
                                    '<idFormaPago xsi:type="xsd:string">'+$globalParameters.fp_redservi+'</idFormaPago>' +
                                    '<codPoblacionOrigen xsi:type="xsd:string">'+$globalParameters.origen_redservi+'</codPoblacionOrigen>' +
                                    '<codPoblacionDestino xsi:type="xsd:string">'+String(aditional)+String(dCity)+'</codPoblacionDestino>'+
                                    '<ListaUnidadesDeclaradas xsi:type="ws:ArrayListUnidadesEnvioDeclaradas" soapenc:arrayType="ws:DTOUnidadEnvioDeclarada[]">';



                                //console.log(value);
                                body += '<DTOUnidadEnvioDeclarada>'+
                                    '<cantidad xsi:type="xsd:string">'+ String("1") +'</cantidad>'+
                                    '<largoCm xsi:type="ws:string">'+ String(length) +'</largoCm>'+
                                    '<altoCm xsi:type="xsd:string">'+ String(height) +'</altoCm>'+
                                    '<anchoCm xsi:type="xsd:string">'+ String(width) +'</anchoCm>'+
                                    '<pesoKilos xsi:type="xsd:string">'+ String(weight) +'</pesoKilos>'+
                                    '<valorDeclarado xsi:type="xsd:string">'+ String(parseFloat((declaredValue+iva)*trm).toFixed(2)) +'</valorDeclarado>'+
                                    '</DTOUnidadEnvioDeclarada>';


                                body += '</ListaUnidadesDeclaradas>' +
                                    '</DTOCotizadorEnvios>' +
                                    '</ws:WebServiceCotizadorEnvios.ws_cotizarEnvio>' +
                                    '</soapenv:Body>' +
                                    '</soapenv:Envelope>';

                                //console.log($parseXML(body));

                                $api.quotationShipments(body).then(function(response) {

                                    var res = $parseXML(response);
                                    // console.log(res);
                                    var valCod = res.getElementsByTagName("codigo");

                                    if (valCod[0].textContent === '0') {
                                        $scope.fail=false;
                                        $scope.redLocal=true;

                                        var arniveles = res.getElementsByTagName("ValorCobradoTotal");

                                        $scope.result[index].local_delivery=parseFloat((arniveles[0].textContent)*1.25)/(trm-50);

                                        $scope.result[index].total=parseFloat($scope.result[index].duties_taxes)+parseFloat($scope.result[index].handling)+parseFloat($scope.result[index].insurance)+
                                            parseFloat($scope.result[index].merchan_service)+parseFloat($scope.result[index].logistic_us)+parseFloat($scope.result[index].custome_dutie_fee);

                                        $scope.result[index].total+=parseFloat($scope.result[index].local_delivery);

                                        $scope.total.declare += $scope.product[index].declare_value;
                                        $scope.total.declareLocal += $scope.result[index].total*trm;
                                        $scope.total.duties += $scope.result[index].duties;
                                        $scope.total.custome_dutie_fee += $scope.result[index].custome_dutie_fee;
                                        $scope.total.logistic_us += $scope.result[index].logistic_us;
                                        $scope.total.taxes += $scope.result[index].taxes;
                                        $scope.total.handling += $scope.result[index].handling;
                                        $scope.total.insurance += $scope.result[index].insurance;
                                        $scope.total.merchan_service += $scope.result[index].merchan_service;
                                        $scope.total.local_delivery += $scope.result[index].local_delivery;
                                        $scope.total.national_delivery += 0;
                                        $scope.total.redespachos += $scope.result[index].local_delivery + $scope.result[index].national_delivery;
                                        $scope.total.total += $scope.result[index].total;

                                        $scope.loading=false;
                                        $scope.terminate=true;


                                    }else {
                                        $scope.fail=true;
                                        $scope.loading=false;
                                        $scope.terminate=true;
                                    }

                                }, function() {
                                    // error
                                    return $scope.failWsConsume();
                                });*/

                            }
                            else if($scope.quote.country==="MX" || $scope.quote.country==="CL" || $scope.quote.country==="CR" || $scope.quote.country==="PE")
                            {
                                // Asigno nuevos parametros
                                Object.assign(dataQuote, {shipper_country: 'US', city_name: dataQuote.city_code});

                                $api.quoteDhl(dataQuote).then(function(rDhl) {

                                    if(rDhl.status===200){

                                        $scope.fail=false;
                                        $scope.dhl=true;

                                        $scope.result[index].total=parseFloat($scope.result[index].duties_taxes)+parseFloat($scope.result[index].handling)+parseFloat($scope.result[index].insurance)+
                                            parseFloat($scope.result[index].merchan_service)/*+parseFloat($scope.result[index].logistic_us)*/;

                                            let dhl=0;
                                            dhl += rDhl.data.price;

                                            /*+ rDhl.data.guide_cute + rDhl.data.shipping_charge + rDhl.data.total_tax_amount;*/
                                            /*if(rDhl.data.shipping_extra_charges.length>0) {
                                                $.each(rDhl.data.shipping_extra_charges, function(index, value){
                                                    dhl += value.charge_value;
                                                });
                                            }*/

                                        $scope.result[index].local_delivery=r.response.local_delivery;
                                        $scope.result[index].local_dhl=parseFloat(dhl);
                                        $scope.result[index].national_delivery=r.response.national_delivery;

                                        if($scope.redLocal===true){
                                            $scope.result[index].national_delivery=0;
                                            $scope.result[index].total+=parseFloat($scope.result[index].local_delivery);
                                        }else{
                                            $scope.result[index].local_delivery=0;
                                            $scope.result[index].total+=parseFloat($scope.result[index].national_delivery);
                                        }

                                        $scope.total.declare += $scope.product[index].declare_value;
                                        $scope.total.declareLocal += $scope.result[index].total*trm;
                                        $scope.total.duties += $scope.result[index].duties;
                                        $scope.total.logistic_us += $scope.result[index].logistic_us;
                                        $scope.total.taxes += $scope.result[index].taxes;
                                        $scope.total.handling += $scope.result[index].handling;
                                        $scope.total.insurance += $scope.result[index].insurance;
                                        $scope.total.merchan_service += $scope.result[index].merchan_service;
                                        $scope.total.local_delivery += $scope.result[index].local_delivery;
                                        $scope.total.local_redservi += $scope.result[index].local_redservi;
                                        $scope.total.local_sesrvientrega += $scope.result[index].local_servientrega;
                                        $scope.total.local_envia += $scope.result[index].localenvia;
                                        $scope.total.local_dhl += $scope.result[index].local_dhl;
                                        $scope.total.national_delivery += $scope.result[index].national_delivery;
                                        $scope.total.redespachos += $scope.result[index].local_delivery + $scope.result[index].national_delivery;
                                        $scope.total.total += $scope.result[index].total;

                                        $scope.loading=false;
                                        $scope.terminate=true;

                                    }else{
                                        $scope.fail=true;
                                        $scope.loading=false;
                                        $scope.terminate=true;
                                    }

                                }, function(){
                                    $scope.failWsConsume();
                                });

                            }
                            else
                            {

                                $scope.fail=false;

                                $scope.result[index].total=parseFloat($scope.result[index].duties_taxes)+parseFloat($scope.result[index].handling)+parseFloat($scope.result[index].insurance)+
                                    parseFloat($scope.result[index].merchan_service)+parseFloat($scope.result[index].logistic_us);

                                $scope.result[index].local_delivery=r.response.local_delivery;
                                $scope.result[index].local_dhl=r.response.local_delivery;
                                $scope.result[index].national_delivery=r.response.national_delivery;

                                if($scope.redLocal===true){
                                    $scope.result[index].national_delivery=0;
                                    $scope.result[index].total+=parseFloat($scope.result[index].local_delivery);
                                }else{
                                    $scope.result[index].local_delivery=0;
                                    $scope.result[index].total+=parseFloat($scope.result[index].national_delivery);
                                }

                                $scope.total.declare += $scope.product[index].declare_value;
                                $scope.total.declareLocal += $scope.result[index].total*trm;
                                $scope.total.duties += $scope.result[index].duties;
                                $scope.total.logistic_us += $scope.result[index].logistic_us;
                                $scope.total.taxes += $scope.result[index].taxes;
                                $scope.total.handling += $scope.result[index].handling;
                                $scope.total.insurance += $scope.result[index].insurance;
                                $scope.total.merchan_service += $scope.result[index].merchan_service;
                                $scope.total.local_delivery += $scope.result[index].local_delivery;
                                $scope.total.local_redservi += $scope.result[index].local_redservi;
                                $scope.total.local_sesrvientrega += $scope.result[index].local_servientrega;
                                $scope.total.local_envia += $scope.result[index].localenvia;
                                $scope.total.local_dhl += $scope.result[index].local_dhl;
                                $scope.total.national_delivery += $scope.result[index].national_delivery;
                                $scope.total.redespachos += $scope.result[index].local_delivery + $scope.result[index].national_delivery;
                                $scope.total.total += $scope.result[index].total;

                                $scope.loading=false;
                                $scope.terminate=true;
                            }

                        }

                    }, function() {
                        // error
                        return $scope.failWsConsume();
                    });

                },function() {
                    // error
                    return $scope.failWsConsume();
                });

            });

        }};


    $scope.validUsrStatus=false;
    $scope.terminateAlr=false;


    $scope.validUsr = function () {
        $scope.btnVal='Validando...';
        $scope.loadVal=true;
        const dataT = {
            username: $globalParameters.api_client,
            password: $globalParameters.api_pswd,
            grant_type: $globalParameters.api_grant_type
        };
        $api.call('token','POST',dataT,true,true).then(function (r){

            const dataLogin={
                username: $scope.aditionals.email,
                password: $scope.aditionals.password,
                token: r.access_token
            };

            $api.loginUser(dataLogin).then(function (rlog) {

                if(rlog.message && rlog.error) {
                    $scope.sucMsg = false;
                    $scope.validUsrStatus = false;
                    $scope.errMsg = rlog.message;
                }else if(rlog.account_number){

                    const data = {
                        email: $scope.aditionals.email,
                        token: r.access_token
                    };

                    $api.validateLocker(data).then(function (r1) {

                        if(r1.message && r1.error){
                            $scope.sucMsg=false;
                            $scope.validUsrStatus=false;
                            $scope.errMsg = r1.message;
                        }
                        else if (r1.account_number){
                            $scope.errMsg=false;
                            $scope.sucMsg = 'El e-mail es valido y el número de casillero es: '+r1.account_number;
                            $scope.validUsrStatus=true;

                            $scope.alert.user_id= r1.user_id;
                            var data1 = {
                                token : data.token,
                                user_id : r1.user_id
                            };

                            $api.detailUser(data1).then(function (rdetail) {

                                $scope.imgProfile=rdetail.avatar;
                                $scope.msgCost="El valor de este servicio es gratis";
                                if(rdetail.user_name !== undefined){
                                    $scope.alert.billing =
                                        [{
                                            "customer": rdetail.display_name,
                                            "email": rdetail.email,
                                            "identification": rdetail.identification,
                                            "country_locale": rdetail.billing[0].country_locale,
                                            "state": rdetail.billing[0].state,
                                            "city": rdetail.billing[0].city,
                                            "address": rdetail.billing[0].address,
                                            "zip_code": rdetail.billing[0].zip_code,
                                            "phone_1": rdetail.billing[0].phone_1,
                                            "phone_2": rdetail.billing[0].phone_2
                                        }];


                                    $scope.alert.shipping =
                                        [{
                                            "customer": rdetail.display_name,
                                            "email": rdetail.email,
                                            "identification": rdetail.identification,
                                            "country_locale" : $scope.quote.country,
                                            "state" : $scope.quote.state,
                                            "city" : $scope.quote.city,
                                            "address": rdetail.shipping[0].address,
                                            "zip_code": rdetail.shipping[0].zip_code,
                                            "phone_1": rdetail.shipping[0].phone_1,
                                            "phone_2": rdetail.shipping[0].phone_2
                                        }];

                                    $scope.user=rdetail;

                                    // console.log($scope.user);

                                }else if (rdetail.status === 404 ){
                                    alert('Error al traer los datos del usuario');
                                }else {
                                    $scope.failWsConsume();
                                }
                            }, function () {
                                // error
                                return $scope.failWsConsume();
                            });
                        }

                        $scope.loadVal=false;
                        $scope.btnVal='Validar';

                    }, function () {
                        // error
                        return $scope.failWsConsume();
                    });
                }
                $scope.loadVal=false;
                $scope.btnVal='Validar';

            }, function (){
                return $scope.failWsConsume();
            });

        }, function () {
            // error
            return $scope.failWsConsume();
        });
    };

    $scope.toAlert = function () {
        const el = $('#bi-tab');
      el.trigger( "click" );
        $('#sc-tab').removeClass('active');
      el.addClass('active');
    };

    $scope.toQuote = function () {
        const el = $('#sc-tab');
        el.trigger( "click" );
        $('#bi-tab').removeClass('active');
        el.addClass('active');
    };

    $scope.toAlertScroll = function () {
        $("html, body").animate({scrollTop:"1610px"});
    };

    $scope.submitalertForm = function (isValid) {
        $scope.submittedalert = true;

        // check to make sure the form is completely valid
        if (isValid) {
            $scope.loadingAlr=true;
            var countryName;
            console.log($scope.result);

            $api.getCountry({id: $scope.alert.shipping[0].country_locale}).then(function (r) {

                countryName = r[0].name;
                $scope.alert.shipping[0].country_locale=r[0].locale;

                var dCity = $scope.alert.shipping[0].city;
                dCity = dCity.split("(");
                dCity = $capitalize(dCity[0]);
                $scope.alert.shipping[0].city=dCity;

                var dState = $scope.alert.shipping[0].state;
                dState = dState.split("(");
                dState = $capitalize(dState[0]);
                $scope.alert.shipping[0].state=dState;


                $.each($scope.qProducts, function(index, value){

                    //console.log($scope.product[index]);

                    /*$scope.result[index]={
                        duties:0,
                        logistic_us:0,
                        taxes:0,
                        duties_taxes:0,
                        handling:0,
                        insurance:0,
                        merchan_service:0,
                        local_delivery:0,
                        national_delivery:0,
                        total:0
                    };*/

                    var weightLb=0,weightKl=0,measurePl={},measureCm={};
                    if($scope.product[index].unidadPeso==='lb'){
                        weightLb = $scope.product[index].weight; weightKl = $scope.product[index].weight*0.453592;
                    }else if ($scope.product[index].unidadPeso==='kl'){
                        weightLb = $scope.product[index].weight*2.20462; weightKl = $scope.product[index].weight;
                    }


                    if($scope.product[index].unidadMedida==='in'){
                        measurePl = {
                            length : $scope.product[index].length !== null ? $scope.product[index].length:1,
                            height : $scope.product[index].height !== null ? $scope.product[index].height:1,
                            width : $scope.product[index].width !== null ? $scope.product[index].width:1
                        };
                        measureCm = {
                            length : $scope.product[index].length !== null ? $scope.product[index].length*2.54:1,
                            height : $scope.product[index].height !== null ? $scope.product[index].height*2.54:1,
                            width : $scope.product[index].width !== null ? $scope.product[index].width*2.54:1
                        }
                    }else if ($scope.product[index].unidadMedida==='cm'){
                        measurePl = {
                            length : $scope.product[index].length !== null ? $scope.product[index].length*0.393701:1,
                            height : $scope.product[index].height !== null ? $scope.product[index].height*0.393701:1,
                            width : $scope.product[index].width !== null ? $scope.product[index].width*0.393701:1
                        };
                        measureCm = {
                            length : $scope.product[index].length !== null ? $scope.product[index].length:1,
                            height : $scope.product[index].height !== null ? $scope.product[index].height:1,
                            width : $scope.product[index].width !== null ? $scope.product[index].width:1
                        }
                    }


                    var weight = parseFloat(weightLb);
                    var length = parseFloat(measurePl.length);
                    var height = parseFloat(measurePl.height);
                    var width = parseFloat(measurePl.width);

                    $scope.alert.declare_value += parseFloat($scope.product[index].declare_value);

                    var redespacho=0, nameRed='';
                    if($scope.redLocal===true){
                        redespacho=parseFloat($scope.result[index].local_delivery);
                        nameRed="LOCAL";
                    }else{
                        redespacho=parseFloat($scope.result[index].national_delivery);
                        nameRed="NACIONAL";
                    }

                    $scope.alert.order_packages.push(
                        {
                            reference: $scope.product[index].reference,
                            weight_lb: weight,
                            length_in: length,
                            width_in: width,
                            height_in: height,
                            image: "",
                            tracking: $scope.product[index].tracking,
                            shipper: $scope.product[index].shipper,
                            carrier: $scope.product[index].carrier,
                            description: $scope.product[index].description,
                            declare_value: parseFloat($scope.product[index].declare_value),
                            tariff: $scope.product[index].category.value,
                            product_type_name: ""
                        }
                    );

                    /*$scope.alert.order_costs.push(
                        {
                            dien: "DIEN000109",
                            quantity: 1,
                            price: $scope.result[index].total !== null && $scope.result[index].total !== undefined ? parseFloat($scope.result[index].total-redespacho):1,
                            comment: "AIR FREIGHT TO "+countryName +', '+dState +' ('+dCity+')'
                        },
                        {
                            dien: "DIENAC0092",
                            quantity: 1,
                            price: redespacho !== null && redespacho !== undefined ? parseFloat(redespacho):1,
                            comment: "REDESPACHO "+nameRed+" A " + countryName +', '+dState +' ('+dCity+')'
                        }
                    );*/

                });


                const dataT = {
                    username: $globalParameters.api_client,
                    password: $globalParameters.api_pswd,
                    grant_type: $globalParameters.api_grant_type
                };
                $api.call('token','POST',dataT,true,true).then(function (r){

                    $scope.alert.token=r.access_token;

                    delete $scope.alert.email;
                    delete $scope.alert.description;
                    delete $scope.alert.tycalert;

                    $scope.alert.business_line_id=$scope.user.business_line_id;

                    $api.preAlert($scope.alert).then(function (ral) {
                        $scope.loadingAlr=false;
                        $scope.terminateAlr=true;

                        if(ral.error){
                            $scope.alrFail=true;
                            $scope.alrMsg = ral.cause;
                        }else{
                            $scope.alrSuc=true;
                            $scope.alrMsg = ral.comment+' con el número: '+ral.order_number;
                        }


                    }, function () {
                        // error
                        return $scope.failWsConsume();
                    });
                }, function () {
                    // error
                    return $scope.failWsConsume();
                });

            }, function () {
                // error
                return $scope.failWsConsume();
            });

        }
    };

    $scope.refresh = function () {
        $window.location.reload();
    };

}

function RecoveryCtrl($scope, $api, getParameter, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize,$compile) {

    $scope.tracking={};
    $scope.btnVal='Check';
    $scope.reclaim=false;

    $scope.errorWs = false;
    $scope.loading = false;
    $scope.haveAtr = false;
    $scope.qOptions = $rellSelect(10);
    $scope.qCategories=[];
    $scope.product=[];
    $scope.editPackage=false;
    $scope.type=[];
    $scope.terminate=false;

    $scope.noCity=false;
    $scope.noState=false;
    $scope.toEdit=false;



    $scope.user={
        user_id: $globalParameters.user_id
    };

    $scope.qUnidadesPeso = [
        {name:'Libras Americanas', value:'lb'},
        {name:'Kilos', value:'kl'}
    ];

    $scope.qUnidadesMedida = [
        {name:'Pulgadas', value:'in'},
        {name:'Centimetros', value:'cm'}
    ];

    $scope.qBusiness = [
        {name:'DHL', value:'DHL'},
        {name:'FEDEX', value:'FEDEX'},
        {name:'UPS', value:'UPS'},
        {name:'USPS', value:'USPS'},
        {name:'OTHER', value:'OTHER'}
    ];

    $scope.qReferences = [
        {name:'Envelope', value:'ENVELOPE'},
        {name:'Pallet', value:'PALLET'},
        {name:'Box', value:'BOX'},
        {name:'Crate', value:'CRATE'},
        {name:'Others', value:'OTHER'}
    ];

    $scope.qCouriers=[];
    var country_locale='CO';
    $api.getCarrier({country_code: country_locale}).then(function (rs) {

        $.each(rs, function(index, value){
            $scope.qCouriers.push({name:value.description, value:value.code});
        });

    }, function() {
        // error
        return $scope.failWsConsume();
    });

    $scope.imgProfile="";


    /*$scope.changeCourier = function (val) {
        console.log(val);
    };*/

    $scope.f= {
        name: 'Ningun archivo seleccionado!'
    };

    $scope.fBill= {
        name: 'Ninguna factura seleccionada!'
    };

    /*$scope.uploadFiles = function(file, errFiles) {
        $scope.f = file;
        console.log(errFiles);
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
            file.upload = Upload.upload({
                url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
                headers: {
                    'Content-Type' : 'image/jpeg'
                },
                method: 'POST',
                data:{file:file}
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                });
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 *
                    evt.loaded / evt.total));
            });
        }
    };*/

    /*$scope.uploadBill = function(file, errFiles) {
        $scope.fBill = file;
        console.log(errFiles);
        $scope.errBill = errFiles && errFiles[0];
        if (file) {
            file.upload = Upload.upload({
                url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
                headers: {
                    'Content-Type' : 'image/jpeg'
                },
                method: 'POST',
                data:{file:file}
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                });
            }, function (response) {
                if (response.status > 0)
                    $scope.errorBillMsg = response.status + ': ' + response.data;
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 *
                    evt.loaded / evt.total));
            });
        }
    };*/

    $api.getTRM().then(function(r) {
        r = parseFloat(r).toFixed(2);
        $scope.aditionals.trm = r;

    }, function() {
        // error
        return $scope.failWsConsume();
    });

    const dataT = {
        username: $globalParameters.api_client,
        password: $globalParameters.api_pswd,
        grant_type: $globalParameters.api_grant_type
    };
    $api.call('token','POST',dataT,true,true).then(function (rt){

        var data = {
            token: rt.access_token,
            country:'CO'
        };

        $api.getCategory(data).then(function(r) {
            $.each(r, function(index, value){
                //($scope.qCategories).push({name : value.description, value : value.code, iva : value.tax, ivaHtml : '<strong>IVA : </strong>'+value.tax+ '%', arancel : value.tariff, arancelHtml : '<strong>Arancel : </strong>'+value.tariff+ '%'});
                // ($scope.qCategories).push({name : value.Description, value : value.ProductTypeName, iva : value.Iva, ivaHtml : '<strong>IVA : </strong>'+value.Iva+ '%', arancel : value.tariff, arancelHtml : '<strong>Arancel : </strong>'+value.tariff+ '%'});
                ($scope.qCategories).push({name : value.description, value : value.code, iva : value.tax, ivaHtml : '<strong>IVA : </strong>'+value.tax+ '%', arancel : value.tariff, arancelHtml : '<strong>Arancel : </strong>'+value.tariff+ '%'});
            });
        }, function() {
            // error
            return $scope.failWsConsume();
        });
    }, function() {
        // error
        return $scope.failWsConsume();
    });


    $api.getCountries().then(function(r) {
        $scope.qCountries = [];
        $.each(r, function(index, value){
            ($scope.qCountries).push({name : value.name, value : value.locale});
        });
    }, function() {
        // error
        return $scope.failWsConsume();
    });

    $scope.aditionals = {
        "country" : "CO",
        "iva" : 19,
        "countryOrg" : "US",
        "stateOrg" : "FL",
        "cityOrg" : "555324",
        "first_name" : "",
        "last_name" : ""
    };

    $scope.aditionals_consignee = [];
    $scope.qStatesArr = [];
    $scope.qCitiesArr = [];

    $scope.recovery= {
        "business_line_id": "",
        "carrier": "REDSERVI",
        "external_guide": "",
        "order_number": "",
        "total_amount":0,
        "comment": "",
        "order_package":[],
        "order_costs":[]
    };

    $api.getStates({country_code : $scope.aditionals.country}).then(function(r) {
        $scope.qStates = [];
        $scope.qCities = [];
        if(r[0]){
            var rState = r[0].states;
            $.each(rState, function(index, value){
                ($scope.qStates).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.id+')'});
            });
        }else{
            $api.getStates({country_code : $scope.aditionals.country}).then(function(r) {
                $scope.qStates = [];
                $scope.qCities = [];
                if(r[0]){
                    var rState = r[0].states;
                    $.each(rState, function(index, value){
                        ($scope.qStates).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.id+')'});
                    });
                }else{
                    return $scope.failWsConsume();
                }
            }, function() {
                // error
                return $scope.failWsConsume();
            });
        }
    }, function() {
        // error
        return $scope.failWsConsume();
    });



    $scope.updateStates = function (item,isArray,index) {

        $api.getCarrier({country_code: item}).then(function (rs) {
            $scope.qCouriers=[];
            $.each(rs, function(index, value){
                $scope.qCouriers.push({name:value.description, value:value.code});
            });

        }, function() {
            // error
            return $scope.failWsConsume();
        });

        $api.getStates({country_code : item}).then(function(r) {

            if(r===null||r===""||r===undefined){
                $scope.noState=true;
            }else{
                var rState = r[0].states;
                if (rState.length === 0){
                    $scope.noState = true;
                    $scope.noCity=true;
                }else if(isArray){

                    $scope.qStatesArr[index]=[];
                    $.each(rState, function(index2, value){
                    $scope.qStatesArr[index].push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.id+')'});
                    });
                }else{
                    $scope.qStates = [];
                    $scope.qCities = [];
                    $scope.noState = false;
                    $scope.noCity=false;
                    $.each(rState, function(index, value){
                        ($scope.qStates).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.id+')'});
                    });
                }
            }

        }, function() {
            // error
            $scope.noState = false;
            $scope.noCity=false;
            //return $scope.failWsConsume();
        });
    };

    $scope.updateCities = function (item,isArray,index) {


        if(item !== undefined){

            item = item.split("(");
            item = item[1].split(")");

            $api.getCities({state_code : item[0]}).then(function(r) {

                if(r===null||r===""||r===undefined){
                    $scope.noCity=true;
                }else{
                    var rCity = r[0].cities;
                    if (rCity.length === 0){
                        $scope.noCity=true;
                    }else if(isArray){

                        $scope.qCitiesArr[index]=[];
                        $scope.noCity=false;
                        $.each(rCity, function(index2, value){
                            $scope.qCitiesArr[index].push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.code+')'});
                        });
                    }else{
                        $scope.qCities = [];
                        $scope.noCity=false;
                        $.each(rCity, function(index, value){
                            ($scope.qCities).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.code+')'});
                        });
                    }
                }


            }, function() {
                // error
                return $scope.failWsConsume();
            });
        }
    };


    $scope.CurrentDate = new Date();

    $scope.changeIVA = function (obj,$event) {
        var currentElement = obj.target;
        console.log(obj.$select.selected);
        //$scope.alert.iva = parseInt(val.iva);

    };

    //Add product of Recovery
    $scope.addProduct = function () {

        console.log('se usa');
        const cont = $scope.qProducts.length;
        $scope.qProducts.push({value: cont+1});

        $scope.product[cont] = {
            "unidadPeso" : "lb",
            "unidadMedida" : "in",
            "weight": null,
            "length": null,
            "width": null,
            "height": null,
            "carrier": "",
            "product_type_name": "9807.20.00.00"
        };

        $scope.type[cont]={
            "weight" : "lb",
            "measure" : 'in'
        };

        $scope.product[cont].category = {
            "name": "Envíos de entrega rápida o mensajería expresa",
            "value": "9807.20.00.00",
            "iva": 19,
            "ivaHtml" : '<strong>IVA : </strong>19%',
            "arancel" : 10,
            "arancelHtml" : '<strong>Arancel : </strong>10%'};

    };

    $scope.changeWeight = function (index) {

        if($scope.product[index].unidadPeso==='lb'){
            $scope.type[index].weight = 'lb';
        }else{
            $scope.type[index].weight = 'kl';
        }
    };

    $scope.changeMeasure = function (index) {
        if($scope.product[index].unidadMedida==='in'){
            $scope.type[index].measure = 'in';
        }else{
            $scope.type[index].measure = 'cm';
        }
    };

    $scope.failWsConsume = function () {

        $scope.errorWs = true;
        $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
        alert($scope.errorWsMsg);

    };
    $scope.refresh = function(){
        $window.location.reload();
    };

    $scope.toChangeForm = function(val){

        $scope.terminateAlr=false;
        $scope.alrSuc=false;
        $scope.alrFail=false;
        $scope.successGuide=false;

        if(val==='recovery'){
            $scope.title="RECOVER UNIDENTIFIED PACKAGE";
            $scope.sTitle="Recupera tu paquete no identificado";
            $scope.toEdit=false;
        }else if(val==='edit'){
            $scope.title="EDIT YOUR PACKAGE";
            $scope.sTitle="Edita tu(s) paquetes";
            $scope.toEdit=true;
        }
    };

    $scope.validUsrStatus=false;
    $scope.terminateAlr=false;

    $scope.submitTrackingForm = function(isValid) {
        $scope.submitted = true;
        // check to make sure the form is completely valid
        if (isValid) {
            $scope.loading=true;
            $api.tracing({id : $scope.tracking.id}).then(function(r) {

                if(r.status===200){
                    if(r.items.length<=0){
                        $scope.noFound=true;
                    }else if(r.packages.length>0){

                        $scope.reclaim = r.reclaim;

                        $scope.recovery.order_number=r.items[0].order_number;
                        $scope.userEdit=r.items[0].user_id;
                        $scope.qProducts=r.packages;

                        $.each(r.packages, function(index, value){



                            $scope.product[index] = {
                                "unidadPeso" : "lb",
                                "unidadMedida" : "in",
                                "weight": value.weight_lb,
                                "length": value.length_in,
                                "width": value.width_in,
                                "height": value.height_in,
                                "carrier": value.carrier,
                                "description": value.description,
                                "declare_value": parseFloat(value.declare_value.toFixed(2)),
                                "product_type_name": "9807.20.00.00"
                            };

                            $scope.type[index]={
                                "weight" : "lb",
                                "measure" : 'in'
                            };

                            $scope.product[index].category = {
                                "name": "Envíos de entrega rápida o mensajería expresa",
                                "value": "9807.20.00.00",
                                "iva": 0,
                                "ivaHtml" : '<strong>IVA : </strong>19%',
                                "arancel" : 10,
                                "arancelHtml" : '<strong>Arancel : </strong>10%'};

                            if(value.declare_value <= 1){
                                $scope.editPackage=true;
                                $scope.product[index].declare_value=parseFloat(value.declare_value.toFixed(2));
                            }

                        });



                    }

                    $scope.fail=false;
                    $scope.success=true;
                    var arr=[];

                    $.each(r.items, function(index, value){
                        arr.push(value.comment);
                    });
                    $scope.listTracking=r.items;
                    $scope.myHTML=arr;
                    $scope.loading=false;



                }else{
                    $scope.loading=false;
                    $scope.fail=true;
                    $scope.success=false;
                }

            }, function() {
                // error
                return $scope.failWsConsume();
            });
        }
    };

    $scope.validUsr = function () {
        $scope.btnVal='Checking...';
        $scope.loadVal=true;
        const dataT = {
            username: $globalParameters.api_client,
            password: $globalParameters.api_pswd,
            grant_type: $globalParameters.api_grant_type
        };
        $api.call('token','POST',dataT,true,true).then(function (r){

            var data = {
                email: $scope.aditionals.email,
                token: r.access_token
            };

            $api.validateLocker(data).then(function (r1) {

                if(r1.message !== undefined && r1.error===true){
                    $scope.sucMsg=false;
                    $scope.validUsrStatus=false;
                    $scope.errMsg = r1.message;
                }else if (r1.account_number !== undefined){
                    $scope.errMsg=false;
                    $scope.sucMsg = 'The email is valid and the locker number is: '+r1.account_number;
                    $scope.validUsrStatus=true;

                    $scope.recovery.user_id= r1.user_id;
                    $scope.recovery.business_line_id= r1.business_line_id;
                    var data1 = {
                        token : data.token,
                        user_id : r1.user_id
                    };


                    $api.detailUser(data1).then(function (rdetail) {

                        $scope.imgProfile=rdetail.avatar;
                        $scope.msgCost="The price of this service is $ "+parseFloat(rdetail.claim_package_cost.toFixed(2)) +" USD";
                        $scope.recoveryCost=parseFloat(rdetail.claim_package_cost.toFixed(2));

                        if(rdetail.user_name !== undefined){
                            $scope.recovery.billing =
                                [{
                                    "customer": rdetail.display_name,
                                    "email": rdetail.email,
                                    "identification": rdetail.identification,
                                    "country_locale": rdetail.billing[0].country_locale,
                                    "state": rdetail.billing[0].state,
                                    "city": rdetail.billing[0].city,
                                    "address": rdetail.billing[0].address,
                                    "zip_code": rdetail.billing[0].zip_code,
                                    "phone_1": rdetail.billing[0].phone_1,
                                    "phone_2": rdetail.billing[0].phone_2
                                }];

                            console.log($scope.product);

                            $.each($scope.qProducts, function(index, value){


                                    $scope.product[index].consignee_name= rdetail.display_name;
                                    $scope.product[index].consignee_address= rdetail.shipping[0].address;
                                    $scope.product[index].consignee_identification= rdetail.identification;
                                    $scope.product[index].consignee_phone= rdetail.shipping[0].phone_1;
                                    $scope.product[index].consignee_country= "";
                                    $scope.product[index].consignee_state= "";
                                    $scope.product[index].consignee_city= "";
                                    $scope.product[index].consignee_zip_code= rdetail.shipping[0].zip_code;


                                $scope.aditionals_consignee[index]={
                                    "first_name":rdetail.first_name,
                                    "last_name":rdetail.last_name
                                };

                            });

                            $scope.recovery.shipping =
                                [{
                                    "customer": rdetail.display_name,
                                    "email": rdetail.email,
                                    "identification": rdetail.identification,
                                    "country_locale" : $scope.aditionals.country,
                                    "state" : rdetail.shipping[0].state,
                                    "city" : rdetail.shipping[0].city,
                                    "address": rdetail.shipping[0].address,
                                    "zip_code": rdetail.shipping[0].zip_code,
                                    "phone_1": rdetail.shipping[0].phone_1,
                                    "phone_2": rdetail.shipping[0].phone_2
                                }];

                            $scope.aditionals.first_name=rdetail.first_name;
                            $scope.aditionals.last_name=rdetail.last_name;

                            $scope.recovery.business_line_id=rdetail.business_line_id;

                            $scope.user=rdetail;

                        }else if (rdetail.status === 404 ){
                            alert('The user\'s data is not completely registered');
                        }else {
                            $scope.failWsConsume();
                        }
                    }, function () {
                        // error
                        return $scope.failWsConsume();
                    });
                }

                $scope.loadVal=false;
                $scope.btnVal='Check';

            }, function () {
                // error
                return $scope.failWsConsume();
            });

        }, function () {
            // error
            return $scope.failWsConsume();
        });
    };






    $scope.submitRecoveryForm = function(isValid) {
        $scope.submittedrecovery = true;
        // check to make sure the form is completely valid
        $.each($scope.qProducts, function(index, value) {
            console.info('aditional package info',value);
            console.log($scope.product[index]);
        });

        var dCity, dBCity, dState, dBState, dCountry, dBCountry, cCity, cState;
        console.log($scope.recovery);


        if (isValid) {

            $scope.loadingAlr=true;



          if($scope.toEdit===true){

              var sumWeight=0;


              $.each($scope.qProducts, function(index, value) {


                  if(!$scope.product[index].consignee_country){
                      $scope.product[index].consignee_country=$scope.recovery.shipping[0].country_locale;
                  }

                  if(!$scope.product[index].consignee_state){
                      $scope.product[index].consignee_state=$scope.recovery.shipping[0].state;
                  }
                      if($scope.product[index].consignee_state.indexOf('(')!==(-1)){
                      $scope.product[index].consignee_state=$scope.product[index].consignee_state.split("(");
                      $scope.product[index].consignee_state=$scope.product[index].consignee_state[0];
                      }


                  if(!$scope.product[index].consignee_city){
                      $scope.product[index].consignee_city=$scope.recovery.shipping[0].city;
                  }
                      if($scope.product[index].consignee_city.indexOf('(')!==(-1)){
                          $scope.product[index].consignee_city=$scope.product[index].consignee_city.split("(");
                          $scope.product[index].consignee_city=$scope.product[index].consignee_city[0];
                      }

                      if(!$scope.product[index].carrier){
                          $scope.product[index].carrier="REDSERVI";
                      }

                      if(parseFloat($scope.product[index].declare_value)<2.00){
                          $scope.product[index].declare_value=2.00;
                      }

                  cCity = $scope.product[index].consignee_city;
                  cCity = cCity.split("(");
                  cCity = $capitalize(cCity[0]);
                  $scope.product[index].consignee_city=cCity;

                  cState = $scope.product[index].consignee_state;
                  cState = cState.split("(");
                  cState = $capitalize(cState[0]);
                  $scope.product[index].consignee_state=cState;

                  $scope.product[index].consignee_name=$scope.aditionals_consignee[index].first_name+' '+$scope.aditionals_consignee[index].last_name;


                  $scope.recovery.order_package.push(
                      {
                          "consignee_name": $scope.product[index].consignee_name,
                          "consignee_address": $scope.product[index].consignee_address,
                          "consignee_identification": $scope.product[index].consignee_identification,
                          "consignee_phone": $scope.product[index].consignee_phone,
                          "consignee_country": $scope.product[index].consignee_country,
                          "consignee_state": $scope.product[index].consignee_state,
                          "consignee_city": $scope.product[index].consignee_city,
                          "consignee_zip_code": $scope.product[index].consignee_zip_code,
                          "description" : $scope.product[index].description,
                          "declare_value": parseFloat($scope.product[index].declare_value),
                          "tariff": $scope.product[index].category.value,
                          "package_id": value.package_id,
                          "carrier": $scope.product[index].carrier
                      }
                  );

                  $scope.recovery.total_amount+=parseFloat($scope.product[index].declare_value);
                  $scope.recovery.total_amount=parseFloat($scope.recovery.total_amount.toFixed(2));

                  sumWeight+=parseFloat($scope.product[index].weight.toFixed(2));
              });

              $scope.recovery.order_costs.push(
                  {
                      "dien": "DIENAC0121",
                      "quantity": $scope.qProducts.length,
                      "price": $scope.recoveryCost,
                      "comment": "FEE FOR UPDATE PACKAGE"
                  }
              );

              if($scope.recovery.total_amount===0){
                  $scope.recovery.total_amount=2.00;
              }


                  console.log($scope.recovery);
                  console.log(sumWeight);
              console.info('Variables adicional: ',$scope.aditionals);

              if($scope.recovery.total_amount<=2000 || sumWeight<=110){
                  const dataT = {
                      username: $globalParameters.api_client,
                      password: $globalParameters.api_pswd,
                      grant_type: $globalParameters.api_grant_type
                  };
                  $api.call('token','POST',dataT,true,true).then(function (r){

                      $scope.recovery.token = r.access_token;

                      $api.updatePackage($scope.recovery).then(function (response) {


                          if(response.order_number===$scope.recovery.order_number){

                              $scope.submitTrackingForm(true);
                              $scope.loadingAlr = false;
                              $scope.terminateAlr = true;

                              $scope.alrSuc = true;
                              $scope.alrMsg = "Your package(s) has been successfully edited";
                              $scope.submitTrackingForm(true);

                          }else if(response.status===500 && response.cause){
                              $scope.submitTrackingForm(true);
                              $scope.loadingAlr = false;
                              $scope.terminateAlr = true;
                              $scope.alrFail=true;
                              $scope.alrMsg = response.cause;
                          }else{
                              $scope.submitTrackingForm(true);
                              $scope.loadingAlr = false;
                              $scope.terminateAlr = true;
                              $scope.alrFail=true;
                              // $scope.alrMsg = "The package could not be updated, please try again later.";
                              $scope.alrMsg = JSON.stringify(response.cause);
                              
                          }

                          $scope.toEdit=false;
                          $scope.submitTrackingForm(true);

                      }, function () {
                          // error
                          return $scope.failWsConsume();
                      });

                  },function () {
                      // error
                      return $scope.failWsConsume();
                  });
              }else{

                  $scope.submitTrackingForm(true);
                  $scope.loadingAlr = false;
                  $scope.terminateAlr = true;
                  $scope.alrFail=true;
                  $scope.alrMsg = "The package(s) can not be updated because the total declared value is greater than 2000 USD or the total weight exceeds 110 pounds";

              }



              $scope.loading=false;
              $scope.terminate=true;

          }else {
              $scope.successGuide=false;
              $scope.successMsgGuide=[];
              $scope.terminateGuide=false;
              $scope.uriDwnld="";
              $scope.loadingGuide=false;

              $scope.total = {
                  declare : 0,
                  declareLocal : 0,
                  duties : 0,
                  logistic_us : 0,
                  taxes : 0,
                  handling : 0,
                  insurance : 0,
                  merchan_service : 0,
                  local_delivery : 0,
                  national_delivery : 0,
                  redespachos : 0,
                  total : 0
              };

              $scope.flete=0;
              $scope.redespacho=0;

              $scope.originPlace = {
                  country: $('#countryOrg option:selected').text(),
                  state: $('#stateOrg option:selected').text(),
                  city: $('#cityOrg option:selected').text()
              };


              $scope.result=[];


              const trm = parseFloat(($scope.aditionals.trm).replace(",", ""));


                      $.each($scope.qProducts, function(index, value){

                          var weightLb=0,weightKl=0,measurePl={},measureCm={},quantity=0,bCity,bState;

                          $scope.result[index]={
                              duties:0,
                              logistic_us:0,
                              taxes:0,
                              duties_taxes:0,
                              handling:0,
                              insurance:0,
                              merchan_service:0,
                              local_delivery:0,
                              national_delivery:0,
                              total:0
                          };


                          if($scope.product[index].unidadPeso==='lb'){
                              console.info('Peso encontrado para lb',$scope.product[index].weight);
                              weightLb = $scope.product[index].weight; weightKl = $scope.product[index].weight*0.453592;
                          }else if ($scope.product[index].unidadPeso==='kl'){
                              console.info('Peso encontrado para kl',$scope.product[index].weight);
                              weightLb = $scope.product[index].weight*2.20462; weightKl = $scope.product[index].weight;
                          }

                          if($scope.product[index].unidadMedida==='in'){
                              measurePl = {
                                  length : $scope.product[index].length !== null ? $scope.product[index].length:1,
                                  height : $scope.product[index].height !== null ? $scope.product[index].height:1,
                                  width : $scope.product[index].width !== null ? $scope.product[index].width:1
                              };
                              measureCm = {
                                  length : $scope.product[index].length !== null ? $scope.product[index].length*2.54:1,
                                  height : $scope.product[index].height !== null ? $scope.product[index].height*2.54:1,
                                  width : $scope.product[index].width !== null ? $scope.product[index].width*2.54:1
                              }
                          }else if ($scope.product[index].unidadMedida==='cm'){
                              measurePl = {
                                  length : $scope.product[index].length !== null ? $scope.product[index].length*0.393701:1,
                                  height : $scope.product[index].height !== null ? $scope.product[index].height*0.393701:1,
                                  width : $scope.product[index].width !== null ? $scope.product[index].width*0.393701:1
                              };
                              measureCm = {
                                  length : $scope.product[index].length !== null ? $scope.product[index].length:1,
                                  height : $scope.product[index].height !== null ? $scope.product[index].height:1,
                                  width : $scope.product[index].width !== null ? $scope.product[index].width:1
                              }
                          }


                         /* var weight = parseFloat(weightLb);
                          var length = parseFloat(measurePl.length);
                          var height = parseFloat(measurePl.height);
                          var width = parseFloat(measurePl.width);*/


                          // console.log(dataSC);

                          const dataT = {
                              username: $globalParameters.api_client,
                              password: $globalParameters.api_pswd,
                              grant_type: $globalParameters.api_grant_type
                          };
                          $api.call('token','POST',dataT,true,true).then(function (rt){

                              bCity = $scope.recovery.billing[0].city;
                              bCity = bCity.split("(");
                              bCity = (bCity[0]).toUpperCase();


                              bState = $scope.recovery.billing[0].state;
                              bState = bState.split("(");
                              bState = (bState[0]).toUpperCase();

                              $scope.recovery.billing[index]= {
                                  address: $scope.recovery.billing[0].address,
                                  city: bCity,
                                  country_locale: $scope.recovery.billing[0].country_locale,
                                  customer: $scope.aditionals.first_name+' '+$scope.aditionals.last_name,
                                  email: $scope.aditionals.email,
                                  identification: $scope.recovery.billing[0].identification,
                                  phone_1: $scope.recovery.billing[0].phone_1,
                                  phone_2: $scope.recovery.billing[0].phone_2,
                                  state: bState,
                                  zip_code: $scope.recovery.billing[0].zip_code
                              };

                              console.info('Prouct information',$scope.product[index]);
                              console.info('Sender information - Origin ', $scope.recovery.billing[0]);

                              dCity = $scope.product[index].consignee_city;
                              dBCity = $scope.recovery.billing[0].city;
                              if(dCity){
                                  dCity = dCity.split("(");
                                  if(dCity[1]){
                                      cCity = dCity[1].split(")");
                                      cCity = cCity[0];
                                      console.log(cCity);
                                  }else{
                                      cCity=0;
                                      console.log(dCity);
                                  }
                                  dCity = (dCity[0]).toUpperCase();
                              }else if (dBCity) {
                                dCity = dBCity.trim();
                              } else{
                                  dCity="BOGOTA"
                              }

                              dState = $scope.product[index].consignee_state;
                              dBState = $scope.recovery.billing[0].state;
                              if(dState){
                                  dState = dState.split("(");
                                  if(dState[1]){
                                      cState = dState[1].split(")");
                                      cState = cState[0];
                                      console.log(cState);
                                  }else{
                                      cState=0;
                                      console.log(cState);
                                  }

                                  dState = (dState[0]).toUpperCase();
                              }else if(dBState) {
                                  dState = dBState.trim();
                              } else {
                                  dState = "BOGOTA D.C"
                              }

                              dCountry = $scope.product[index].consignee_country;
                              dBCountry = $scope.recovery.billing[0].country_locale;
                              if(dCountry){
                                  dCountry = dCountry.trim();
                              }else if (dBCountry) {
                                  dCountry = dBCountry.trim();
                              }else {
                                  dCountry = "CO";
                              }


                              $scope.product[index].consignee_name=$scope.aditionals_consignee[index].first_name+' '+$scope.aditionals_consignee[index].last_name;

                              $scope.recovery.shipping[index]= {
                                  address: $scope.product[index].consignee_address,
                                  city: dCity,
                                  country_locale: dCountry,
                                  customer: $scope.product[index].consignee_name,
                                  email: $scope.product[index].consignee_email,
                                  identification: $scope.product[index].consignee_identification,
                                  phone_1: $scope.product[index].consignee_phone,
                                  phone_2: $scope.product[index].consignee_phone2,
                                  state: dState,
                                  zip_code: $scope.product[index].consignee_zip_code
                              };

                              $scope.recovery.order_package[index]=
                                  {
                                      package_id : value.package_id,
                                      description : $scope.product[index].description,
                                      declare_value: parseFloat($scope.product[index].declare_value),
                                      tariff: $scope.product[index].category.value
                                  };

                              //parseFloat($scope.product[index].declare_value);
                              /*$scope.recovery.shipping[0].state=dState;*/

                              var dataS = {
                                  token: rt.access_token,
                                  tariff: $scope.product[index].category.value,
                                  weight_lb: weightLb.toFixed(2),
                                  height_in: measurePl.height.toFixed(2),
                                  length_in: measurePl.length.toFixed(2),
                                  width_in: measurePl.width.toFixed(2),
                                  declare_value: $scope.product[index].declare_value,
                                  delivery_country_locale: dCountry,
                                  user_id: $scope.user.user_id,
                                  delivery_state: dState,
                                  delivery_city: dCity,
                                  place: $globalParameters.place_default
                              };

                              console.log('Part three...', dataS);
                              // console.log(dataS);


                              $api.shippingCosts(dataS).then(function(response) {

                                  const r = (response.d);

                                  if (r.status === 200 && r.response.status!==422) {


                                      $scope.result[index].duties=r.response.duties_tariff;
                                      $scope.result[index].logistic_us=r.response.logistic_us;
                                      $scope.result[index].taxes=r.response.iva_vat;
                                      $scope.result[index].duties_taxes=r.response.duties_tariff+r.response.iva_vat;

                                      $scope.result[index].handling=r.response.handling_custome;
                                      $scope.result[index].insurance=r.response.insurance;
                                      $scope.result[index].merchan_service=r.response.merchan_service;

                                      $scope.flete+=parseFloat($scope.result[index].duties_taxes)+parseFloat($scope.result[index].handling)+
                                          parseFloat($scope.result[index].insurance)+parseFloat($scope.result[index].merchan_service)+parseFloat($scope.result[index].logistic_us);

                                      $scope.redespacho=r.response.local_delivery;


                                      /*$scope.recovery.order_package.push(
                                          {
                                              "description" : $scope.product[index].description,
                                              "declare_value": parseFloat($scope.product[index].declare_value),
                                              "tariff": "8443.99.00.00"
                                          }
                                      );*/

                                      $scope.flete !== null && !isNaN($scope.flete) ? $scope.flete=parseFloat($scope.flete.toFixed(2)):$scope.flete=1;

                                      $api.getCountry({id: $scope.product[index].consignee_country}).then(function (r) {

                                          countryName = r[0].name;
                                          $scope.recovery.order_costs.push(
                                              {
                                                  "dien": "DIEN000109",
                                                  "quantity": 1,
                                                  "price": $scope.flete,
                                                  "comment": "AIR FREIGHT FROM "+ ($scope.originPlace.country).toUpperCase() +"("+($scope.originPlace.city).toUpperCase()+") TO "+countryName.toUpperCase()
                                              }
                                              /*{
                                                  "dien": "DIENAC0092",
                                                  "quantity": 1,
                                                  "price": $scope.redespacho !== null && $scope.redespacho !== undefined ? parseFloat($scope.redespacho.toFixed(2)):1,
                                                  "comment": "NATIONAL SHIPMENT TO "+dCity+","+dState
                                              }*/);

                                          quantity++;

                                          if(parseInt(index)===($scope.qProducts).length-1){


                                              //console.log($parseXML(body));
                                              $scope.recovery.carrier=$scope.product[0].carrier;
                                              $scope.recovery.external_guide = "";

                                              $scope.successGuide=true;
                                              $scope.successMsgGuide=["The local redispatch service does not apply"];
                                              $scope.uriDwnld="#";

                                              const dataT = {
                                                  username: $globalParameters.api_client,
                                                  password: $globalParameters.api_pswd,
                                                  grant_type: $globalParameters.api_grant_type
                                              };
                                              $api.call('token','POST',dataT,true,true).then(function (r){

                                                  $scope.recovery.token=r.access_token;

                                                  console.log($scope.flete);
                                                  console.log($scope.redespacho);

                                                  $scope.recovery.total_amount = parseFloat($scope.flete.toFixed(2))+parseFloat($scope.redespacho.toFixed(2));

                                                  if($scope.recovery.total_amount<=5){
                                                      $scope.recovery.total_amount=6;
                                                  }

                                                  $api.recoveryOrder($scope.recovery).then(function (ral) {

                                                      $scope.loadingAlr=false;
                                                      $scope.terminateAlr=true;

                                                      if(ral.error){
                                                          $scope.alrFail=true;
                                                          $scope.alrMsg = ral.cause;
                                                      }else{
                                                          $scope.alrSuc=true;
                                                          $scope.alrMsg = ral.comment+' with the Order Number: '+ral.order_number;
                                                          $scope.submitTrackingForm(true);
                                                      }

                                                      console.log($scope.recovery);


                                                  }, function () {
                                                      // error
                                                      return $scope.failWsConsume();
                                                  });

                                                  console.log($scope.recovery);
                                              }, function () {
                                                  // error
                                                  return $scope.failWsConsume();
                                              });



                                          }
                                      },function(){

                                      });
                                  }else {
                                      $scope.loadingAlr=false;
                                      $scope.terminateAlr=true;
                                      $scope.alrFail=true;
                                      $scope.alrMsg = r.response.cause;
                                  }
                              }, function() {
                                  // error
                                  return $scope.failWsConsume();
                              });

                          },function() {
                              // error
                              return $scope.failWsConsume();
                          });


                      });



          }

        }
    };

}

function AlertCtrl($scope, $api, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize,FileUploader,$GenerateParams) {

    $scope.errorWs = false;
    $scope.loading = false;
    $scope.haveAtr = false;
    $scope.qOptions = $rellSelect(10);
    $scope.qCategories=[];
    $scope.qCouriers=[];
    $scope.product=[];
    $scope.type=[];
    $scope.terminate=false;
    $scope.btnVal='Validar';

    $scope.noCity=false;
    $scope.noState=false;

    $scope.qUnidadesPeso = [
        {name:'Libras Americanas', value:'lb'},
        {name:'Kilos', value:'kl'}
    ];

    $scope.qUnidadesMedida = [
        {name:'Pulgadas', value:'in'},
        {name:'Centimetros', value:'cm'}
    ];

    $scope.qBusiness = [
        {name:'DHL', value:'DHL'},
        {name:'FEDEX', value:'FEDEX'},
        {name:'UPS', value:'UPS'},
        {name:'USPS', value:'USPS'},
        {name:'OTHER', value:'OTHER'}
    ];

    $scope.qReferences = [
        {name:'Envelope', value:'ENVELOPE'},
        {name:'Pallet', value:'PALLET'},
        {name:'Box', value:'BOX'},
        {name:'Crate', value:'CRATE'},
        {name:'Others', value:'OTHER'}
    ];

    $scope.qOrderTypes = [
        {name:'Carga', value:'6E81648D-3A37-4BD3-B349-2FE7D169A479'},
        {name:'Currier', value:'260221E9-D6B7-4517-A103-5D6C47058CC3'}
    ];

    $scope.qChargeTypes = [
        {name: 'Carga general', value: 'Carga general'},
        {name: 'Carga a Granel', value: 'Carga a Granel'},
        {name: 'Carga perecedera', value: 'Carga perecedera'},
        {name: 'Carga frágil', value: 'Carga frágil'},
        {name: 'Carga peligrosa: Colocar subcasillas: Descripción* UN* CLASS *GRUPO', value: 'Carga peligrosa: Colocar subcasillas: Descripción* UN* CLASS *GRUPO'},
        {name: 'Carga Sobredimensionada y/o pesada', value: 'Carga Sobredimensionada y/o pesada'},
        {name: 'Carga Reefer', value: 'Carga Reefer'},
        {name: 'Carga en Open Top o Flatrack', value: 'Carga en Open Top o Flatrack'},
        {name: 'Animales Vivos', value: 'Animales Vivos'}
    ];


    $scope.f= {
        name: 'Ningun archivo seleccionado!'
    };

    $scope.fBill= {
        name: 'Ninguna factura seleccionada!'
    };

    $scope.imgProfile="";


    $api.getTRM().then(function(r) {
        r = parseFloat(r).toFixed(2);
        $scope.aditionals.trm = r;

    }, function() {
        // error
        return $scope.failWsConsume();
    });

    $scope.qProducts = [{value: 1}];

    const dataT = {
        username: $globalParameters.api_client,
        password: $globalParameters.api_pswd,
        grant_type: $globalParameters.api_grant_type
    };
    $api.call('token','POST',dataT,true,true).then(function (rt){

        const data = {
            token: rt.access_token,
            country:'CO'
        };

        $api.getCategory(data).then(function(r) {
        //$api.callTkn('v1/tariffs/'+data.country,'GET',{},true,false,data.token).then(function (r){
            $.each(r, function(index, value){
                //($scope.qCategories).push({name : value.description, value : value.code, iva : value.tax, ivaHtml : '<strong>IVA : </strong>'+value.tax+ '%', arancel : value.tariff, arancelHtml : '<strong>Arancel : </strong>'+value.tariff+ '%'});
                // ($scope.qCategories).push({name : value.Description, value : value.ProductTypeName, iva : value.Iva, ivaHtml : '<strong>IVA : </strong>'+value.Iva+ '%', arancel : value.tariff, arancelHtml : '<strong>Arancel : </strong>'+value.tariff+ '%'});
                ($scope.qCategories).push({name : value.description, value : value.code, iva : value.tax, ivaHtml : '<strong>IVA : </strong>'+value.tax+ '%', arancel : value.tariff, arancelHtml : '<strong>Arancel : </strong>'+value.tariff+ '%'});
            });
        }, function() {
            // error
            return $scope.failWsConsume();
        });
    }, function() {
        // error
        return $scope.failWsConsume();
    });

    const data1 = {
        token: 'G85WtEfs_rXPoM6Xzau0f73iPQdBLsI2Blh7QpeE9Q3eUbDr_eUNG5zL1PDzB6VqtQQTodJ95zSAlFdgUMPCu3nO0Sx6AddaGyBaRkBGuSDqp_Pc65TRtfPAtaMzTfY56wWWrdM1lYRo7H_a15zYRXMAtgibYPLPcC-QoEHqVNObJP7D5QL4hX81RBD3QjPfnzz9HHYIW9B2v96Ccn_ys2marsSQ-JHRum_MBiGgV-bJBwUdnyRiPNF_LkbYp07FhULlRKCu_M4FEppSjtrcCg',
        country:'CO'
    };

    /*$api.callTkn('v1/tariffs/'+data1.country,'GET',{},true,false,data1.token).then(function (r){
        console.log(r);
        }, function() {
        return $scope.failWsConsume();
    });*/


    $api.getCountries().then(function(r) {
        $scope.qCountries = [];
        $scope.qCountries[0] = [];
        $.each(r, function(index, value){
            ($scope.qCountries).push({name : value.name, value : value.locale});
            ($scope.qCountries[0]).push({name : value.name, value : value.locale});
        });
    }, function() {
        // error
        return $scope.failWsConsume();
    });


    $scope.aditionals = {
        country: "CO",
        iva: 19,
        countryOrg: "US",
        stateOrg: "FL",
        cityOrg: "555324"
    };

    $scope.qCouriers = [];

    $api.getCarrier({country_code: $scope.aditionals.country}).then(function (rs) {

        $.each(rs, function(index, value){
            $scope.qCouriers.push({name:value.description, value:value.code});
        });

    }, function() {
        // error
        return $scope.failWsConsume();
    });


    $scope.alert= {
        business_line_id: $globalParameters.business_line_id,
        carrier: "",
        external_guide: "",
        tracking: "",
        url_attached: "",
        purchase_order: "",
        category_product: "Pre alertado por anicamboxexpress.com",
        customer_reference: "",
        order_type: $globalParameters.order_type,
        declare_value : 0,
        comment: "",
        notification:true,
        store: false,
        sandbox:false,
        place:3,
        order_references: [],
        order_packages:[],
        order_costs:[]
    };

    $scope.qProducts = [{value: 1}];

    $scope.product[0] = {
        unidadPeso: "lb",
        unidadMedida: "in",
        weight: null,
        length: null,
        width: null,
        height: null,
        category : {
            name: "Envíos de entrega rápida o mensajería expresa",
            value: "9807.20.00.00",
            iva: 19,
            ivaHtml : '<strong>IVA : </strong>19%',
            arancel : 10,
            arancelHtml : '<strong>Arancel : </strong>10%'}
    };

    $scope.type[0]={
        weight : "lb",
        measure : 'in'
    };


    $api.getStates({country_code : $scope.aditionals.country}).then(function(r) {
        $scope.qStates = [];
        $scope.qStates[0] = [];
        if(r[0]){
            var rState = r[0].states;
            $.each(rState, function(index, value){
                ($scope.qStates).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.id+')'});
                ($scope.qStates[0]).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.id+')'});
            });
        }else{
            $api.getStates({country_code : $scope.aditionals.country}).then(function(r) {
                $scope.qStates = [];
                $scope.qStates[0] = [];
                if(r[0]){
                    var rState = r[0].states;
                    $.each(rState, function(index, value){
                        ($scope.qStates).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.id+')'});
                        ($scope.qStates[0]).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.id+')'});
                    });
                }else{
                    console.log("Error con los estados");
                }
            }, function() {
                // error
                //return $scope.failWsConsume();
            });
        }
    }, function() {
        // error
        //return $scope.failWsConsume();
    });

    $scope.qCities = [];

    $scope.updateStates = function (item,indState) {
        console.log("Actualiza estados",indState);

        const dataT = {
            username: $globalParameters.api_client,
            password: $globalParameters.api_pswd,
            grant_type: $globalParameters.api_grant_type
        };
        $api.call('token','POST',dataT,true,true).then(function (rt){

            var data = {
                token: rt.access_token,
                country:item
            };

            $api.getCategory(data).then(function(r) {
                $scope.qCategories=[];
                $.each(r, function(index, value){
                    ($scope.qCategories).push({name : value.description, value : value.code, iva : value.tax, ivaHtml : '<strong>IVA : </strong>'+value.tax+ '%', arancel : value.tariff, arancelHtml : '<strong>Arancel : </strong>'+value.tariff+ '%'});
                });

                $api.getCarrier({country_code: item}).then(function(r) {
                    $scope.qCouriers=[];
                    if(r.length>0){
                        $.each(r, function(index, value){
                            $scope.qCouriers.push({name:value.description, value:value.code});
                        });
                    }

                    $api.getStates({country_code : item}).then(function(r) {

                        if(r===null||r===""||r===undefined){
                            $scope.noState=true;
                        }else{
                            var rState = r[0].states;
                            if (rState.length === 0){
                                $scope.noState = true;
                                $scope.noCity=true;
                            }else{
                                $scope.noState = false;
                                $scope.noCity=false;

                                if(indState>=0){
                                    console.log("Entra a estados por producto");
                                    $scope.qStates[indState] = [];
                                    $scope.qCities[indState] = [];
                                    $.each(rState, function(index, value){
                                        ($scope.qStates[indState]).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.id+')'});
                                    });
                                }else{
                                    console.log("no Entra a estados por producto");
                                    $scope.qStates = [];
                                    $scope.qCities = [];
                                    $.each(rState, function(index, value){
                                        ($scope.qStates).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.id+')'});
                                    });
                                }

                            }
                        }

                    }, function() {
                        // error
                        $scope.noState = false;
                        $scope.noCity=false;
                        //return $scope.failWsConsume();
                    });

                }, function() {
                    // error
                    return $scope.failWsConsume();
                });

            }, function() {
                // error
                return $scope.failWsConsume();
            });
        }, function() {
            // error
            return $scope.failWsConsume();
        });




    };

    $scope.updateCities = function (item,indCity) {


        if(item !== undefined){

            item = item.split("(");
            item = item[1].split(")");

            $api.getCities({state_code : item[0]}).then(function(r) {

                if(r===null||r===""||r===undefined){
                    $scope.noCity=true;
                }else{
                    var rCity = r[0].cities;
                    if (rCity.length === 0){
                        $scope.noCity=true;
                    }else{
                        $scope.noCity=false;
                        if(indCity>=0){
                            console.log("Entra a ciudades por producto");
                            $scope.qCities[indCity] = [];
                            $.each(rCity, function(index, value){
                                ($scope.qCities[indCity]).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.code+')'});
                            });
                        }else{
                            console.log("No Entra a ciudades por producto");
                            $scope.qCities = [];
                            $.each(rCity, function(index, value){
                                ($scope.qCities).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.code+')'});
                            });
                        }

                    }
                }


            }, function() {
                // error
                return $scope.failWsConsume();
            });
        }
    };


    $scope.CurrentDate = new Date();

    $scope.changeIVA = function (obj,$event) {
        var currentElement = obj.target;
        console.log(obj.$select.selected);
        //$scope.alert.iva = parseInt(val.iva);

    };

    // Add product pre-alert only
    $scope.addProduct = function (item) {

        $api.getCountries().then(function(r) {
            $scope.qCountries[cont] = [];
            $.each(r, function(index, value){
                ($scope.qCountries[cont]).push({name : value.name, value : value.locale});
            });
        }, function() {
            // error
            return $scope.failWsConsume();
        });

        $api.getStates({country_code : $scope.product[0].consignee_country}).then(function(r) {
            $scope.qStates[cont] = [];
            if(r[0]){
                var rState = r[0].states;
                $.each(rState, function(index, value){
                    ($scope.qStates[cont]).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.id+')'});
                });
            }else{
                $api.getStates({country_code : $scope.product[0].consignee_country}).then(function(r) {
                    $scope.qStates[cont] = [];
                    if(r[0]){
                        var rState = r[0].states;
                        $.each(rState, function(index, value){
                            ($scope.qStates[cont]).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.id+')'});
                        });
                    }else{
                        return $scope.failWsConsume();
                    }
                }, function() {
                    // error
                    return $scope.failWsConsume();
                });
            }
        }, function() {
            // error
            return $scope.failWsConsume();
        });


        const cont = $scope.qProducts.length;
        $scope.qProducts.push({value: cont+1});

        if(!item) {
            $scope.product[cont] = {
                consignee_customer: $scope.product[0].consignee_customer,
                consignee_email: $scope.product[0].consignee_email,
                consignee_identification: $scope.product[0].consignee_identification,
                consignee_country: $scope.product[0].consignee_country,
                consignee_address: $scope.product[0].consignee_address,
                consignee_zip_code: $scope.product[0].consignee_zip_code,
                consignee_phone: $scope.product[0].consignee_phone,
                unidadPeso: "lb",
                unidadMedida: "in",
                weight: null,
                length: null,
                width: null,
                height: null,
                category : {
                    name: "Envíos de entrega rápida o mensajería expresa",
                    value: "9807.20.00.00",
                    iva: 19,
                    ivaHtml : '<strong>IVA : </strong>19%',
                    arancel : 10,
                    arancelHtml : '<strong>Arancel : </strong>10%'}
            };
        }else {
            $scope.product[cont] = item;
        }


        $scope.type[cont]={
            weight : "lb",
            measure : 'in'
        };

    };

    // Clone product for charge only
    $scope.cloneProduct = function (quantity) {
        console.log(quantity);
        $scope.cloned = false;
        if($scope.product[0].weight) {
            for (var i = 1; i < quantity; i++) {
                $timeout(function () {
                    $scope.addProduct($scope.product[0]);
                }, 500);

            }
            console.log($scope.product);
            alert('El producto se ha clonado ' + quantity + ' veces satisfactoriamente. Total de productos: ' + quantity);
            $scope.cloned = true;
        }else {
            alert('Debes tener la información obligatoria completa para poder clonar un producto!')
        }
    }

    $scope.changeWeight = function (index) {

        if($scope.product[index].unidadPeso==='lb'){
            $scope.type[index].weight = 'lb';
        }else{
            $scope.type[index].weight = 'kl';
        }
    };

    $scope.changeMeasure = function (index) {
        if($scope.product[index].unidadMedida==='in'){
            $scope.type[index].measure = 'in';
        }else{
            $scope.type[index].measure = 'cm';
        }
    };

    $scope.failWsConsume = function () {

        $scope.errorWs = true;
        $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
        alert($scope.errorWsMsg);

    };
    $scope.refresh = function(){
        $window.location.reload();
    };


    $scope.validUsrStatus=false;
    $scope.terminateAlr=false;
    $scope.validUsr = function () {
        $scope.btnVal='Validando...';
        $scope.loadVal=true;
        const dataT = {
            username: $globalParameters.api_client,
            password: $globalParameters.api_pswd,
            grant_type: $globalParameters.api_grant_type
        };
        $api.call('token','POST',dataT,true,true).then(function (r){

            var data = {
                email: $scope.aditionals.email,
                token: r.access_token
            };

            $api.validateLocker(data).then(function (r1) {

                if(r1.message !== undefined && r1.error===true){
                    $scope.sucMsg=false;
                    $scope.validUsrStatus=false;
                    $scope.errMsg = r1.message;
                }else if (r1.account_number !== undefined){
                    $scope.errMsg=false;
                    $scope.sucMsg = 'El e-mail es valido y el número de casillero es: '+r1.account_number;
                    $scope.validUsrStatus=true;


                    $scope.alert.user_id= r1.user_id;
                    var data1 = {
                        token : data.token,
                        user_id : r1.user_id
                    };

                    $api.detailUser(data1).then(function (rdetail) {

                        $scope.imgProfile=rdetail.avatar;
                        $scope.msgCost="El valor de este servicio es gratis";

                        if(rdetail.user_name !== undefined){
                            $scope.alert.billing =
                                [{
                                    customer: rdetail.display_name,
                                    email: rdetail.email,
                                    identification: rdetail.identification,
                                    country_locale: rdetail.billing[0].country_locale,
                                    state: rdetail.billing[0].state,
                                    city: rdetail.billing[0].city,
                                    address: rdetail.billing[0].address,
                                    zip_code: rdetail.billing[0].zip_code,
                                    phone_1: rdetail.billing[0].phone_1,
                                    phone_2: rdetail.billing[0].phone_2
                                }];


                                $scope.product[0].consignee_customer= rdetail.display_name;
                                $scope.product[0].consignee_email= rdetail.email;
                                $scope.product[0].consignee_identification= rdetail.identification;
                                $scope.product[0].consignee_country= rdetail.billing[0].country_locale;
                                $scope.product[0].consignee_address= rdetail.billing[0].address;
                                $scope.product[0].consignee_zip_code= rdetail.billing[0].zip_code;
                                $scope.product[0].consignee_phone= rdetail.billing[0].phone_1;


                            $scope.alert.shipping =
                                [{
                                    customer: rdetail.display_name,
                                    email: rdetail.email,
                                    identification: rdetail.identification,
                                    country_locale: $scope.aditionals.country,
                                    state : rdetail.shipping[0].state,
                                    city : rdetail.shipping[0].city,
                                    address: rdetail.shipping[0].address,
                                    zip_code: rdetail.shipping[0].zip_code,
                                    phone_1: rdetail.shipping[0].phone_1,
                                    phone_2: rdetail.shipping[0].phone_2
                                }];

                            $scope.user=rdetail;


                        }else if (rdetail.status === 404 ){
                            alert('Error al traer los datos del usuario');
                        }else {
                            $scope.failWsConsume();
                        }
                    }, function () {
                        // error
                        return $scope.failWsConsume();
                    });
                }

                $scope.loadVal=false;
                $scope.btnVal='Validar';

            }, function () {
                // error
                return $scope.failWsConsume();
            });

        }, function () {
            // error
            return $scope.failWsConsume();
        });
    };



    var uplPicture = $scope.uplPicture = new FileUploader({
        url: $globalParameters.AWSUrl+'upload/file',
        formData: [{
            dir:"pAlertPicProduct",
            date:date.format(new Date(),'DDMMYYYY_HHmmss')
        }]
    });

    // FILTERS TO PRODUCT IMAGE

    uplPicture.filters.push({
        name: 'image',
        fn: function(item, options) {
            console.log(item);
            return item.type.indexOf('image/')!==(-1);
        }
    });


    var uplPurchase = $scope.uplPurchase = new FileUploader({
        url: $globalParameters.AWSUrl+'upload/file',
        formData: [{
            dir:"pAlertPurchaseComp",
            date:date.format(new Date(),'DDMMYYYY_HHmmss')
        }]
    });

    var uplAttach = $scope.uplAttach = new FileUploader({
        url: $globalParameters.AWSUrl+'upload/file',
        formData: [{
            dir:"pAlertAttachProduct",
            date:date.format(new Date(),'DDMMYYYY_HHmmss')
        }]
    });

    // CALLBACKS

    uplPicture.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uplPicture.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uplPicture.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
        console.info("Este es el archivo de la foto de producto... ",$scope.uplPicture)
    };

    // CALLBACKS

    uplPurchase.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uplPurchase.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uplPurchase.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
        console.info("Este es el archivo de la foto de la factura de compra... ",$scope.uplPurchase)
    };


    $scope.submitalertForm = function (isValid) {
        $scope.submittedalert = true;
        //console.log($scope.uplPicture.queue);
        // check to make sure the form is completely valid
        if (isValid) {

            $scope.loadingAlr=true;
            $scope.redLocal=false;
            $scope.result=[];
            var countryName;

            $api.getCountry({id: $scope.alert.shipping[0].country_locale}).then(function (r) {

                countryName = r[0].name;
                $scope.alert.shipping[0].country_locale=r[0].locale;

                var dCity = $scope.alert.shipping[0].city;
                dCity = dCity.split("(");
                dCity = $capitalize(dCity[0]);
                $scope.alert.shipping[0].city=dCity.toUpperCase();

                var dState = $scope.alert.shipping[0].state;
                dState = dState.split("(");
                dState = $capitalize(dState[0]);
                $scope.alert.shipping[0].state=dState.toUpperCase();



                $.each($scope.qProducts, function(index, value){


                    $scope.result[index]={
                        duties:0,
                        logistic_us:0,
                        taxes:0,
                        duties_taxes:0,
                        handling:0,
                        insurance:0,
                        merchan_service:0,
                        local_delivery:0,
                        national_delivery:0,
                        total:0
                    };
                    var weightLb=0,weightKl=0,measurePl={},measureCm={};


                    if($scope.product[index].unidadPeso==='lb'){
                        weightLb = $scope.product[index].weight; weightKl = $scope.product[index].weight*0.453592;
                    }else if ($scope.product[index].unidadPeso==='kl'){
                        weightLb = $scope.product[index].weight*2.20462; weightKl = $scope.product[index].weight;
                    }


                    if($scope.product[index].unidadMedida==='in'){
                        measurePl = {
                            length : $scope.product[index].length !== null ? $scope.product[index].length:1,
                            height : $scope.product[index].height !== null ? $scope.product[index].height:1,
                            width : $scope.product[index].width !== null ? $scope.product[index].width:1
                        };
                        measureCm = {
                            length : $scope.product[index].length !== null ? $scope.product[index].length*2.54:1,
                            height : $scope.product[index].height !== null ? $scope.product[index].height*2.54:1,
                            width : $scope.product[index].width !== null ? $scope.product[index].width*2.54:1
                        }
                    }else if ($scope.product[index].unidadMedida==='cm'){
                        measurePl = {
                            length : $scope.product[index].length !== null ? $scope.product[index].length*0.393701:1,
                            height : $scope.product[index].height !== null ? $scope.product[index].height*0.393701:1,
                            width : $scope.product[index].width !== null ? $scope.product[index].width*0.393701:1
                        };
                        measureCm = {
                            length : $scope.product[index].length !== null ? $scope.product[index].length:1,
                            height : $scope.product[index].height !== null ? $scope.product[index].height:1,
                            width : $scope.product[index].width !== null ? $scope.product[index].width:1
                        }
                    }


                    var weight = parseFloat(weightLb);
                    var length = parseFloat(measurePl.length);
                    var height = parseFloat(measurePl.height);
                    var width = parseFloat(measurePl.width);
                    var image="",urlAttach="";

                    $scope.alert.declare_value += parseFloat($scope.product[index].declare_value);

                    var redespacho=0, nameRed='';
                    if($scope.redLocal===true){
                        redespacho=parseFloat($scope.result[index].local_delivery);
                        nameRed="LOCAL";
                    }else{
                        redespacho=parseFloat($scope.result[index].national_delivery);
                        nameRed="NACIONAL";
                    }

                    if($scope.uplPicture.queue[index]){
                        $scope.uplPicture.queue[index].upload();
                        image="https://anicam.ws:3006/files/"+$scope.uplPicture.queue[index].formData[0].dir+'_'+$scope.uplPicture.queue[index].formData[0].date+'_'+$scope.uplPicture.queue[index].file.name;
                    }

                    if($scope.uplAttach.queue[index]){
                        $scope.uplAttach.queue[index].upload();
                        urlAttach="https://anicam.ws:3006/files/"+$scope.uplAttach.queue[index].formData[0].dir+'_'+$scope.uplAttach.queue[index].formData[0].date+'_'+$scope.uplAttach.queue[index].file.name;
                    }

                    var coState=$scope.product[index].consignee_state.split("(");
                    coState=coState[0].toUpperCase();

                    var coCity=$scope.product[index].consignee_city.split("(");
                    coCity=coCity[0].toUpperCase();

                    $scope.alert.order_packages.push(
                        {
                            reference: $scope.product[index].reference,
                            weight_lb: weight,
                            length_in: length,
                            width_in: width,
                            height_in: height,
                            image: image,
                            tracking: $scope.product[index].tracking,
                            shipper: $scope.product[index].shipper,
                            carrier: $scope.product[index].carrier,
                            external_guide: $scope.product[index].external_guide,
                            url_attached: urlAttach,
                            description: $scope.product[index].description,
                            declare_value: parseFloat($scope.product[index].declare_value),
                            tariff: $scope.product[index].category.value,
                            product_type_name: "",
                            external_id: $scope.product[index].external_id,
                            consignee_name:$scope.product[index].consignee_customer,
                            consignee_address:$scope.product[index].consignee_address,
                            consignee_identification:$scope.product[index].consignee_identification,
                            consignee_phone:$scope.product[index].consignee_phone,
                            consignee_country:$scope.product[index].consignee_country,
                            consignee_state:coState,
                            consignee_city:coCity,
                            consignee_zip_code:$scope.product[index].consignee_zip_code
                        }
                    );

                    /*$scope.alert.order_costs.push(
                        {
                            dien: "DIEN000109",
                            quantity: 1,
                            price: $scope.result[index].total !== null && $scope.result[index].total !== undefined ? parseFloat($scope.result[index].total-redespacho):1,
                            comment: "AIR FREIGHT TO "+countryName +', '+dState +' ('+dCity+')'
                        },
                        {
                            dien: "DIENAC0092",
                            quantity: 1,
                            price: redespacho !== null && redespacho !== undefined ? parseFloat(redespacho):1,
                            comment: "REDESPACHO "+nameRed+" A " + countryName +', '+dState +' ('+dCity+')'
                        }
                    );*/

                });


                const dataT = {
                    username: $globalParameters.api_client,
                    password: $globalParameters.api_pswd,
                    grant_type: $globalParameters.api_grant_type
                };
                $api.call('token','POST',dataT,true,true).then(function (r){

                    $scope.alert.token=r.access_token;
                    $scope.alert.business_line_id=$scope.user.business_line_id;

                    if($scope.uplPurchase.queue[0]){

                        $scope.uplPurchase.queue[0].upload();
                        $scope.alert.url_attached="https://anicam.ws:3006/files/"+$scope.uplPurchase.queue[0].formData[0].dir+'_'+$scope.uplPurchase.queue[0].formData[0].date+'_'+$scope.uplPurchase.queue[0].file.name;
                        $scope.alert.purchase_order="https://anicam.ws:3006/files/"+$scope.uplPurchase.queue[0].formData[0].dir+'_'+$scope.uplPurchase.queue[0].formData[0].date+'_'+$scope.uplPurchase.queue[0].file.name;
                    }


                    $api.preAlert($scope.alert).then(function (ral) {
                        $scope.loadingAlr=false;
                        $scope.terminateAlr=true;

                        if(ral.error){
                            $scope.alrFail=true;
                            $scope.alrMsg = ral.cause;
                        }else{
                            $scope.alrSuc=true;
                            $scope.alrMsg = ral.comment+' with the Order Number: '+ral.order_number;
                        }

                    }, function () {
                        // error
                        return $scope.failWsConsume();
                    });
                }, function () {
                    // error
                    return $scope.failWsConsume();
                });

            }, function () {
                // error
                return $scope.failWsConsume();
            });


        }
    };

    $scope.refresh = function () {
        $window.location.reload();
    };

}

function CutGuide($scope, $api, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize, FileUploader) {


    $scope.loading = false;
    $scope.terminate=false;
    $scope.result={};

    $scope.loadingOvrr=false;
    $scope.fail=false;
    $scope.failOvrr=false;

    $scope.serRedes=true;
    $scope.loadingAlr=false;
    $scope.failAlr=false;
    $scope.successAlr=false;
    $scope.failMsgAlr=[];
    $scope.successMsgAlr=[];
    $scope.successCost=false;
    $scope.failCost=false;
    $scope.failMsgCost=false;
    $scope.successMsgCost=[];
    $scope.terminateGuide=false;
    $scope.uriDwnld="";
    $scope.loadingGuide=false;

    $scope.guide={};
    $scope.successMsgOvrr="";
    $scope.failMsgOvrr="";
    $scope.successOvrr=false;
    $scope.failOvrr=false;


    $api.getTRM().then(function(r) {
        r = parseFloat(r).toFixed(2);
        $scope.trm = r;

    }, function() {
        // error
        return $scope.failWsConsume();
    });



    $scope.qProducts = [{value: 1}];

    $scope.submitTrackingForm = function(isValid) {
        $scope.submitted = true;
        // check to make sure the form is completely valid
        if (isValid) {
            $scope.loading=true;
            $scope.successMsgOvrr="";
            $scope.failMsgOvrr="";
            $scope.successOvrr=false;
            $scope.failOvrr=false;
            $api.detailOrder($scope.tracking.id).then(function(r) {


                    if(r.packages!== undefined && r.packages.length>=1){

                        $scope.result=r;
                        $scope.packages=r.packages;
                        $scope.consignee=r.packages.consignee;
                        $scope.shipper=r.packages.shipper;

                        $scope.aditionals={
                            country_locale : "CO",
                            state : "",
                            city : ""
                        };

                        $api.getCountries().then(function(r) {
                            $scope.qCountries = [];
                            $.each(r, function(index, value){
                                ($scope.qCountries).push({name : value.name, value : value.locale});
                            });
                        }, function() {
                            // error
                            return $scope.failWsConsume();
                        });

                        $api.getStates({country_code : $scope.consignee.country_locale}).then(function(r) {
                            $scope.qStates = [];
                            if(r[0]){
                                var rState = r[0].states;
                                $.each(rState, function(index, value){
                                    ($scope.qStates).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.id+')'});
                                });
                            }else{
                                $api.getStates({country_code : $scope.consignee.country_locale}).then(function(r) {
                                    $scope.qStates = [];
                                    if(r[0]){
                                        var rState = r[0].states;
                                        $.each(rState, function(index, value){
                                            ($scope.qStates).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.id+')'});
                                        });
                                    }else{

                                        return $scope.failWsConsume();
                                    }
                                }, function() {
                                    // error
                                    return $scope.failWsConsume();
                                });

                            }
                        }, function() {
                            // error
                            return $scope.failWsConsume();
                        });


                        $scope.qCities = [];


                        $scope.updateStates = function (item) {
                            $scope.qStates = [];
                            $scope.qCities = [];
                            $api.getStates({country_code : item}).then(function(r) {

                                if(r===null||r===""||r===undefined){
                                    $scope.noState=true;
                                }else{
                                    var rState = r[0].states;
                                    if (rState.length === 0){
                                        $scope.noState = true;
                                        $scope.noCity=true;
                                    }else{
                                        $scope.noState = false;
                                        $scope.noCity=false;
                                        $.each(rState, function(index, value){
                                            ($scope.qStates).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.id+')'});
                                        });
                                    }
                                }

                            }, function() {
                                // error
                                $scope.noState = false;
                                $scope.noCity=false;
                                //return $scope.failWsConsume();
                            });
                        };

                        $scope.updateCities = function (item) {
                            $scope.qCities = [];
                            if(item !== undefined){

                                item = item.split("(");
                                item = item[1].split(")");

                                $api.getCities({state_code : item[0]}).then(function(r) {

                                    if(r===null||r===""||r===undefined){
                                        $scope.noCity=true;
                                    }else{
                                        var rCity = r[0].cities;
                                        if (rCity.length === 0){
                                            $scope.noCity=true;
                                        }else{
                                            $scope.noCity=false;
                                            $.each(rCity, function(index, value){
                                                ($scope.qCities).push({name : $capitalize(value.name), value : $capitalize(value.name)+'('+value.code+')'});
                                            });
                                        }
                                    }


                                }, function() {
                                    // error
                                    return $scope.failWsConsume();
                                });
                            }
                        };

                        //Reseteo valores

                        $scope.successGuide=false;
                        $scope.successMsgGuide=[];
                        $scope.terminateGuide=false;
                        $scope.failGuide=false;
                        $scope.failMsgGuide=[];
                        $scope.loadingGuide=false;
                        $scope.terminateGuide=false;
                        $scope.failAlr=false;
                        $scope.successAlr=false;
                        $scope.failMsgAlr=[];
                        $scope.successMsgAlr=[];
                        $scope.successCost=false;
                        $scope.failCost=false;
                        $scope.failMsgCost=false;
                        $scope.successMsgCost=[];


                        $scope.noFound=true;
                        $scope.loading=false;
                        $scope.fail=false;
                        $scope.success=true;
                        $scope.terminate=true;
                    }else{
                    $scope.loading=false;
                    $scope.fail=true;
                    $scope.success=false;
                    $scope.terminate=false;
                     }

            }, function() {
                // error
                return $scope.failWsConsume();
            });
        }
    };

    $scope.submitOverrideForm = function(isValid) {
        $scope.submitted = true;
        // check to make sure the form is completely valid
        if (isValid) {
            $scope.loadingOvrr=true;

            $api.overrGuide({num_guia:$scope.guide.id}).then(function(r) {

                if(r.status===200){
                    $scope.loadingOvrr=false;
                    if(r.data.code===4){
                        $scope.failOvrr=true;
                        $scope.failMsgOvrr=r.data.description;
                    }
                }

            }, function() {
                // error
                return $scope.failWsConsume();
            });

        }
    };

    $scope.submitCotForm = function(isValid) {

        if (isValid) {


            $scope.serRedes=true;
            $scope.loadingAlr=true;
            $scope.failAlr=false;
            $scope.successAlr=false;
            $scope.failMsgAlr=[];
            $scope.successMsgAlr=[];
            $scope.successCost=false;
            $scope.failCost=false;
            $scope.failMsgCost=false;
            $scope.successMsgCost=[];
            $scope.terminateGuide=false;
            $scope.uriDwnld="";
            $scope.loadingGuide=false;

            var aditional='',body='';


            /*dState = dState[1].split(")");
            dState = dState[0];*/

            $scope.consignee.state=$scope.aditionals.state;
            $scope.consignee.city=$scope.aditionals.city;

            var dState = $scope.consignee.state;
            dState = dState.split("(");
            dState = dState[0];

            var dCity = $scope.consignee.city;
            dCity = dCity.split("(");
            dCity = dCity[0];


            $api.getStates({country_code : "CO"}).then(function(r) {
                $scope.qStates = [];
                var rState = r[0].states;

                // console.log($capitalize(dState).substring(0, 4));

                var contS=0;
                $.each(rState, function(indexS, valueS){

                    if($capitalize(valueS.name)===$capitalize(dState)){
                        contS++;
                        $scope.result.State=$capitalize(dState);
                        const idState=valueS.id;


                        $api.getCities({state_code :idState}).then(function(r) {

                            if(r===null||r===""||r===undefined){
                                $scope.failAlr=true;
                                $scope.failMsgAlr=["There are no cities for this department.","No existen ciudades para este departamento."];
                                $scope.loadingAlr=false;
                            }else{
                                var rCity = r[0].cities;
                                if (rCity.length === 0){
                                    $scope.failAlr=true;
                                    $scope.failMsgAlr=["There are no cities for this department.","No existen ciudades para este departamento."];
                                    $scope.loadingAlr=false;
                                }else{
                                    var cont=0;

                                    $.each(rCity, function(index, value){


                                        if($capitalize(value.name)===$capitalize(dCity)){
                                            cont++;
                                            $scope.result.City=$capitalize(value.name);
                                            const idCity=value.code;

                                            const trm = parseFloat(($scope.trm).replace(",", "")).toFixed(2);
                                            const priceDeclarate = parseFloat(($scope.packages.declare_value).toFixed(2));
                                            // console.log(priceDeclarate);
                                            const iva=0;

                                            $scope.iva =0;


                                            var weightLb=0,weightKl=0,measurePl={},measureCm={};

                                            weightLb = $scope.packages.weight; weightKl = (parseFloat($scope.packages.weight)*0.453592).toFixed(2);

                                            measurePl = {
                                                length : ($scope.packages.length),
                                                height : ($scope.packages.height),
                                                width : ($scope.packages.width)
                                            };
                                            measureCm = {
                                                length : (parseFloat(($scope.packages.length))*2.54).toFixed(2),
                                                height : (parseFloat(($scope.packages.height))*2.54).toFixed(2),
                                                width : (parseFloat(($scope.packages.width))*2.54).toFixed(2)
                                            };

                                            var weight = (parseFloat(weightKl).toFixed(2)); // Peso de libras a kilos
                                            var length = (parseFloat(measureCm.length)).toFixed(2);  // Medidas de pulgadas a cm
                                            var height = (parseFloat(measureCm.height)).toFixed(2);  // Medidas de pulgadas a cm
                                            var width = (parseFloat(measureCm.width)).toFixed(2);  // Medidas de pulgadas a cm

                                            // console.log(idState);
                                            if (idState === 1 || idState === 2){
                                                aditional = '0';
                                            }

                                            body = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="/distribucion/webservices/ws_cotizadorEnvios.php" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">' +
                                                '<soapenv:Header/>' +
                                                '<soapenv:Body>' +
                                                '<ws:WebServiceCotizadorEnvios.ws_cotizarEnvio soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">' +
                                                '<user xsi:type="xsd:string">'+$globalParameters.user_redservi+'</user>' +
                                                '<password xsi:type="xsd:string">'+$globalParameters.pswd_redservi+'</password>' +
                                                '<DTOCotizadorEnvios xsi:type="ws:DTOCotizadorEnvios">' +
                                                '<codCliente xsi:type="xsd:string">'+$globalParameters.nit_cliente_redservi+'</codCliente>' +
                                                '<idModoTransporte xsi:type="xsd:string">'+$globalParameters.mt_redservi+'</idModoTransporte>' +
                                                '<idFormaPago xsi:type="xsd:string">'+$globalParameters.fp_redservi+'</idFormaPago>' +
                                                '<codPoblacionOrigen xsi:type="xsd:string">'+$globalParameters.origen_redservi+'</codPoblacionOrigen>' +
                                                '<codPoblacionDestino xsi:type="xsd:string">'+String(aditional)+String(idCity)+'</codPoblacionDestino>'+
                                                '<ListaUnidadesDeclaradas xsi:type="ws:ArrayListUnidadesEnvioDeclaradas" soapenc:arrayType="ws:DTOUnidadEnvioDeclarada[]">';


                                            body += '<DTOUnidadEnvioDeclarada>'+
                                                '<cantidad xsi:type="xsd:string">'+ String("1") +'</cantidad>'+
                                                '<largoCm xsi:type="ws:string">'+ String(length) +'</largoCm>'+
                                                '<altoCm xsi:type="xsd:string">'+ String(height) +'</altoCm>'+
                                                '<anchoCm xsi:type="xsd:string">'+ String(width) +'</anchoCm>'+
                                                '<pesoKilos xsi:type="xsd:string">'+ String(weight) +'</pesoKilos>'+
                                                '<valorDeclarado xsi:type="xsd:string">'+ String(parseFloat((priceDeclarate+iva)*trm).toFixed(2)) +'</valorDeclarado>'+
                                                '</DTOUnidadEnvioDeclarada>';


                                            body += '</ListaUnidadesDeclaradas>' +
                                                '</DTOCotizadorEnvios>' +
                                                '</ws:WebServiceCotizadorEnvios.ws_cotizarEnvio>' +
                                                '</soapenv:Body>' +
                                                '</soapenv:Envelope>';

                                            $api.quotationShipments(body).then(function(response) {


                                                var res = $parseXML(response);
                                                // console.log(res);
                                                var valCod = res.getElementsByTagName("codigo");

                                                if (valCod[0].textContent === '0') {
                                                    $scope.fail=false;

                                                    var arniveles = res.getElementsByTagName("ValorCobradoTotal");
                                                    $scope.result.trm=parseFloat(trm).toFixed(2);
                                                    $scope.result.redespachoLocal=parseFloat(arniveles[0].textContent).toFixed(2);
                                                    $scope.result.redespachoUSD = parseFloat((parseFloat(arniveles[0].textContent)/parseFloat(trm)).toFixed(2)); // Valor de redespacho en dolares
                                                    $scope.result.comisionLocal=parseFloat((($scope.result.redespachoLocal)*0.25).toFixed(2));
                                                    $scope.result.comisionUSD=parseFloat((($scope.result.redespachoUSD)*0.25).toFixed(2));

                                                    // Estos totales son con el valor de comision incluido
                                                    $scope.result.totRedespachoLocal=parseFloat((($scope.result.redespachoLocal)*1.25).toFixed(2));
                                                    $scope.result.totRedespachoUSD=parseFloat((($scope.result.redespachoUSD)*1.25).toFixed(2));


                                                    $scope.consignee.customer=$scope.consignee.customer.trim();
                                                    $scope.consignee.customer=$scope.consignee.customer.replace("    "," ");
                                                    $scope.consignee.customer=$scope.consignee.customer.replace("   "," ");
                                                    $scope.consignee.customer=$scope.consignee.customer.replace("  "," ");

                                                    $scope.shipper.customer=$scope.shipper.customer.trim();
                                                    $scope.consignee.customer=$scope.consignee.customer.replace("    "," ");
                                                    $scope.shipper.customer=$scope.shipper.customer.replace("   "," ");
                                                    $scope.shipper.customer=$scope.shipper.customer.replace("  "," ");


                                                    var arrName = ($scope.consignee.customer).split(" "), arrNameS=($scope.shipper.customer).split(" "), nameDes='',apDes='',nameShip='',apShip='';


                                                    if (arrNameS.length>=5){
                                                        nameShip=$capitalize(arrNameS[0])+' '+$capitalize(arrNameS[1]);
                                                        apShip=$capitalize(arrNameS[2])+' '+$capitalize(arrNameS[3]);
                                                    }else if (arrNameS.length===4){
                                                        nameShip=$capitalize(arrNameS[0])+' '+$capitalize(arrNameS[1]);
                                                        apShip=$capitalize(arrNameS[2])+' '+$capitalize(arrNameS[3]);
                                                    }else if (arrNameS.length===3){
                                                        nameShip=$capitalize(arrNameS[0])+' '+$capitalize(arrNameS[1]);
                                                        apShip=$capitalize(arrNameS[2]);
                                                    }else if (arrNameS.length===2){
                                                        nameShip=$capitalize(arrNameS[0]);
                                                        apShip=$capitalize(arrNameS[1]);
                                                    }else if (arrNameS.length===1){
                                                        nameShip=$capitalize(arrNameS[0]);
                                                        apShip=$capitalize(arrNameS[0]);
                                                    }else{
                                                        nameShip=$capitalize(arrNameS[0]);
                                                        apShip=$capitalize(arrNameS[0]);
                                                    }


                                                    if (arrName.length>=5){
                                                        nameDes=$capitalize(arrName[0])+' '+$capitalize(arrName[1]);
                                                        apDes=$capitalize(arrName[2])+' '+$capitalize(arrName[3]);
                                                    } else if (arrName.length===4){
                                                        nameDes=$capitalize(arrName[0])+' '+$capitalize(arrName[1]);
                                                        apDes=$capitalize(arrName[2])+' '+$capitalize(arrName[3]);
                                                    }else if (arrName.length===3){
                                                        nameDes=$capitalize(arrName[0])+' '+$capitalize(arrName[1]);
                                                        apDes=$capitalize(arrName[2]);
                                                    }else if (arrName.length===2){
                                                        nameDes=$capitalize(arrName[0]);
                                                        apDes=$capitalize(arrName[1]);
                                                    }else if (arrName.length===1){
                                                        nameDes=$capitalize(arrName[0]);
                                                        apDes=$capitalize(arrName[0]);
                                                    }else{
                                                        nameDes=$capitalize(arrName[0]);
                                                        apDes=$capitalize(arrName[0]);
                                                    }

                                                    $scope.result.nameDes=nameDes;
                                                    $scope.result.apDes=apDes;

                                                    $scope.result.nameShip=nameShip;
                                                    $scope.result.apShip=apShip;

                                                    console.log(arrName.length);
                                                    console.log(arrNameS.length);
                                                    console.log("Destinatario: "+nameDes);
                                                    console.log("Remitente: "+nameShip);
                                                    console.log(apDes);


                                                    $scope.cutGuide=function() {

                                                        $scope.loadingGuide=true;
                                                        var idRem="";
                                                        $scope.shipper.phone_1!=="" && $scope.shipper.phone_1!==null && $scope.shipper.phone_1!=="0000000" ? $scope.shipper.phone_1=$scope.shipper.phone_1:$scope.shipper.phone_1="12345678";
                                                        $scope.shipper.identification!==""&&$scope.shipper.identification!==null ? idRem=$scope.shipper.identification:idRem=$scope.shipper.phone_1;
                                                        idRem=idRem.trim();
                                                        idRem=idRem.replace(" ","");
                                                        idRem=idRem.replace("(","");
                                                        idRem=idRem.replace(")","");
                                                        idRem=idRem.replace("CC ","");
                                                        idRem=idRem.replace("CC","");
                                                        idRem=idRem.replace("L","");
                                                        console.log(idRem);

                                                        var idDes="";
                                                        $scope.consignee.phone_1!=="" && $scope.consignee.phone_1!==null && $scope.consignee.phone_1 !== "0000000" ? $scope.consignee.phone_1=$scope.consignee.phone_1:$scope.consignee.phone_1="12345678";
                                                        $scope.consignee.identification!==""&&$scope.consignee.identification!==null ? idDes=$scope.consignee.identification:idDes=$scope.consignee.phone_1;
                                                        idDes=idDes.trim();
                                                        idDes=idDes.replace(" ","");
                                                        idDes=idDes.replace("(","");
                                                        idDes=idDes.replace(")","");
                                                        idDes=idDes.replace("CC ","");
                                                        idDes=idDes.replace("CC","");
                                                        idDes=idDes.replace("L","");
                                                        console.log(idDes);

                                                        var secPhoneRem=0;
                                                        $scope.shipper.phone_2!==""&&$scope.shipper.phone_2!==null ? secPhoneRem=$scope.shipper.phone_2:secPhoneRem=$scope.shipper.phone_1;
                                                        var secPhoneDes=0;
                                                        $scope.consignee.phone_2!==""&&$scope.consignee.phone_2!==null ? secPhoneDes=$scope.consignee.phone_2:secPhoneDes=$scope.consignee.phone_1;

                                                        console.log($scope.result);

                                                        var body = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="/distribucion/webservices/ws_cotizadorEnvios.php" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">' +
                                                            '<soapenv:Header/>' +
                                                            '<soapenv:Body>' +
                                                            '<ws:WebServiceRegistrarEnvios.ws_registrarEnvio soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">' +
                                                            '<user xsi:type="xsd:string">' + $globalParameters.user_redservi + '</user>' +
                                                            '<password xsi:type="xsd:string">' + $globalParameters.pswd_redservi + '</password>' +
                                                            '<DTOEnvio xsi:type="ws:DTOEnvio">' +
                                                            '<observaciones xsi:type="xsd:string">ORDER NUMBER: '+$scope.result.order_number+'</observaciones>' +

                                                            '<contenido xsi:type="ws:DTOContenido">' +
                                                            '<codigo xsi:type="xsd:string">1</codigo>' +
                                                            '</contenido>' +

                                                            '<remitente xsi:type="ws:DTOParticipanteEnvio">' +
                                                            '<persona xsi:type="ws:DTOEmpresaPersona">' +
                                                            '<identificacion xsi:type="ws:DTOIdentificacionPersona">' +
                                                            '<tipo xsi:type="xsd:string">2</tipo>' +
                                                            '<numero xsi:type="xsd:string">' + idRem + '</numero>' +
                                                            '</identificacion>' +
                                                            '<nombres xsi:type="xsd:string">' + nameShip + '</nombres>' +
                                                            '<apellidos xsi:type="xsd:string">' + apShip + '</apellidos>' +
                                                            '<listaDatosContacto xsi:type="ws:ArrayListDatosContacto" soapenc:arrayType="ws:DTODatoContacto[]">' +

                                                            '<DTODatoContacto>' +
                                                            '<tipoInformacion>0</tipoInformacion>' +
                                                            '<descripcion>' + $scope.shipper.phone_1 + '</descripcion>' +
                                                            '</DTODatoContacto>' +

                                                            '<DTODatoContacto>' +
                                                            '<tipoInformacion>1</tipoInformacion>' +
                                                            '<descripcion>' + secPhoneRem + '</descripcion>' +
                                                            '</DTODatoContacto>' +

                                                            '</listaDatosContacto>' +
                                                            '</persona>' +

                                                            '<empresa xsi:type="ws:DTOEmpresaPersona">' +
                                                            '<identificacion xsi:type="ws:DTOIdentificacionPersona">' +
                                                            '<tipo xsi:type="xsd:string">1</tipo>' +
                                                            '<numero xsi:type="xsd:string">' + $globalParameters.nit_cliente_redservi + '</numero>' +
                                                            '</identificacion>' +
                                                            '<razonSocial xsi:type="xsd:string">ANICAM ENTERPRISES INC.</razonSocial>' +
                                                            '</empresa>' +

                                                            '</remitente>' +

                                                            '<destinatario xsi:type="ws:DTOParticipanteEnvio">' +
                                                            '<persona xsi:type="ws:DTOEmpresaPersona">' +
                                                            '<identificacion xsi:type="ws:DTOIdentificacionPersona">' +
                                                            '<tipo xsi:type="xsd:string">2</tipo>' +
                                                            '<numero xsi:type="xsd:string">' + idDes + '</numero>' +
                                                            '</identificacion>' +
                                                            '<nombres xsi:type="xsd:string">' + nameDes + '</nombres>' +
                                                            '<apellidos xsi:type="xsd:string"> '+apDes+' </apellidos>' +
                                                            '<listaDatosContacto xsi:type="ws:ArrayListDatosContacto" soapenc:arrayType="ws:DTODatoContacto[]">' +

                                                            '<DTODatoContacto>' +
                                                            '<tipoInformacion>0</tipoInformacion>' +
                                                            '<descripcion>' + $scope.consignee.phone_1 + '</descripcion>' +
                                                            '</DTODatoContacto>' +

                                                            '<DTODatoContacto>' +
                                                            '<tipoInformacion>1</tipoInformacion>' +
                                                            '<descripcion>' + secPhoneDes + '</descripcion>' +
                                                            '</DTODatoContacto>' +

                                                            '</listaDatosContacto>' +
                                                            '</persona>' +
                                                            '</destinatario>' +
                                                            '<listaUnidadesDeclaradas xsi:type="ws:ArrayListUnidadesEnvioDeclaradas" soapenc:arrayType="ws:DTOUnidadEnvioDeclarada[]">';


                                                        body += '<DTOUnidadEnvioDeclarada>'+
                                                            '<cantidad xsi:type="xsd:string">'+ String("1") +'</cantidad>'+
                                                            '<largoCm xsi:type="ws:string">'+ String(length) +'</largoCm>'+
                                                            '<altoCm xsi:type="xsd:string">'+ String(height) +'</altoCm>'+
                                                            '<anchoCm xsi:type="xsd:string">'+ String(width) +'</anchoCm>'+
                                                            '<pesoKilos xsi:type="xsd:string">'+ String(weight) +'</pesoKilos>'+
                                                            '<valorDeclarado xsi:type="xsd:string">'+ String(parseFloat((priceDeclarate+iva)*trm).toFixed(2)) +'</valorDeclarado>'+
                                                            '</DTOUnidadEnvioDeclarada>';


                                                        body+= '</listaUnidadesDeclaradas>' +
                                                            '<docsRelacionados xsi:type="ws:ArrayListDocRelacionados" soapenc:arrayType="ws:DTODocRelacionado[]"/>' +

                                                            '<origenEnvio xsi:type="ws:DTOLugar">' +
                                                            '<direccion xsi:type="xsd:string">Ct Cent Nte Km 24 Z Centro F Sauzal</direccion>' +
                                                            '<codigoPostal xsi:type="xsd:string">250008</codigoPostal>' +
                                                            '<ciudad xsi:type="ws:DTOCiudad">' +
                                                            '<codigo xsi:type="xsd:string">25175</codigo>' +
                                                            '</ciudad>' +
                                                            '<barrio xsi:type="xsd:string">Vda. Sindamanoy</barrio>' +
                                                            '<bloqueCasaUnidad xsi:type="xsd:string">3</bloqueCasaUnidad>' +
                                                            '<lugarRelacionado/>' +
                                                            '</origenEnvio>' +

                                                            /*'<origenEnvio xsi:type="ws:DTOLugar">' +
                                                            '<direccion xsi:type="xsd:string">Carrera 7 No. 71-52 Torre A Oficina 902</direccion>' +
                                                            '<codigoPostal xsi:type="xsd:string">1100111</codigoPostal>' +
                                                            '<ciudad xsi:type="ws:DTOCiudad">' +
                                                            '<codigo xsi:type="xsd:string">11001</codigo>' +
                                                            '</ciudad>' +
                                                            '<barrio xsi:type="xsd:string">CASTILLA</barrio>' +
                                                            '<bloqueCasaUnidad xsi:type="xsd:string">12</bloqueCasaUnidad>' +
                                                            '<lugarRelacionado/>' +
                                                            '</origenEnvio>' +*/

                                                            '<destinoEnvio xsi:type="ws:DTOLugar">' +
                                                            '<direccion xsi:type="xsd:string">' + $scope.consignee.address + '</direccion>' +
                                                            '<codigoPostal xsi:type="xsd:string">' + $scope.consignee.zip_code + '</codigoPostal>' +
                                                            '<ciudad xsi:type="ws:DTOCiudad">' +
                                                            '<codigo xsi:type="xsd:string">' + String(aditional) + String(idCity) + '</codigo>' +
                                                            '</ciudad>' +
                                                            '<barrio xsi:type="xsd:string">' + $scope.consignee.address + '</barrio>' +
                                                            '<bloqueCasaUnidad xsi:type="xsd:string">0</bloqueCasaUnidad>' +
                                                            '<lugarRelacionado/>' +
                                                            '</destinoEnvio>' +
                                                            '<modoTransporte xsi:type="ws:DTOModoTransporte">' +
                                                            '<codigo xsi:type="xsd:string">' + $globalParameters.mt_redservi + '</codigo>' +
                                                            '</modoTransporte>' +
                                                            '<caja xsi:type="ws:DTOCaja">' +
                                                            '<idCaja xsi:type="xsd:int">' + $globalParameters.no_caja_redservi + '</idCaja>' +
                                                            '</caja >' +
                                                            '<tipoEmbalaje xsi:type="ws:DTOTipoEmbalaje">' +
                                                            '<IdTipoEmbalaje xsi:type="xsd:int">2</IdTipoEmbalaje>' +
                                                            '</tipoEmbalaje >' +
                                                            '<documentoOperacion xsi:type="ws:DTODocumentoOperacion">' +
                                                            '<formaPago xsi:type="ws:DTOFormaPago">' +
                                                            '<codFormaPago xsi:type="xsd:int">' + $globalParameters.fp_redservi + '</codFormaPago>' +
                                                            '</formaPago>' +
                                                            '<tipoEntrega xsi:type="ws:DTOTipoEntrega">' +
                                                            '<codTipoEntrega xsi:type="xsd:int">1</codTipoEntrega>' +
                                                            '</tipoEntrega>' +
                                                            '</documentoOperacion>' +
                                                            '</DTOEnvio>' +
                                                            '</ws:WebServiceRegistrarEnvios.ws_registrarEnvio>' +
                                                            '</soapenv:Body>' +
                                                            '</soapenv:Envelope>';


                                                        $api.registerShipments(body).then(function (response) {

                                                            res = $parseXML(response);
                                                            //console.log(res);

                                                            valCod = res.getElementsByTagName("codigo");

                                                            if (valCod[0] === undefined || valCod === null) {
                                                                alert('An error has been generated with the national shipping service provider, but we generate a shipping order in the next few hours');

                                                            } else if (valCod[0].textContent === '0') {
                                                                var guia = res.getElementsByTagName("num_guia");
                                                                var uriguia = res.getElementsByTagName("url_guia");
                                                                $scope.result.external_guide = String(guia[0].textContent);

                                                                $scope.successGuide=true;
                                                                $scope.successMsgGuide=["La Guia se ha cortado correctamente con el operador logistico Red Servi. Tu número de guia es: "+$scope.result.external_guide,""];
                                                                $scope.uriDwnld=String(uriguia[0].textContent);

                                                                var dataCost = {
                                                                    order_id : $scope.result.order_id,
                                                                    carrier: "REDSERVI",
                                                                    external_guide : $scope.result.external_guide,
                                                                    dien : "DIENAC0092",
                                                                    comment : "NATIONAL SHIPMENT FROM BOGOTA D.C TO "+$scope.result.State +', '+$scope.result.City,
                                                                    sub_total : $scope.result.redespachoUSD,
                                                                    fee : $scope.result.comisionUSD,
                                                                    total : $scope.result.totRedespachoUSD
                                                                };

                                                                console.log(dataCost);

                                                                $api.addGuideCost(dataCost).then(function (response) {

                                                                    if(response.status===200){
                                                                        $scope.successCost=true;
                                                                        $scope.successMsgCost=["El costo para el redespacho local se ha agregado correctamente al número de orden: "+$scope.result.order_number,""];
                                                                        $scope.loadingGuide=false;
                                                                        $scope.terminateGuide=true;
                                                                    }else if (response.status===400){
                                                                        $scope.failCost=true;
                                                                        $scope.failMsgCost=["No se logro actualizar el número de Guia en el Administrador, el número de orden "+$scope.result.order_number+" ya contiene un numero de guia asociado: "+response.message,"Por favor elimenelo de los costos, anule la guia e intentelo nuevamente."];
                                                                        $scope.loadingGuide=false;
                                                                        $scope.terminateGuide=true;
                                                                    }else{
                                                                        $scope.failCost=true;
                                                                        $scope.failMsgCost=["No se logro añadir el costo del redespacho al número de orden: "+$scope.result.order_number,"Por favor contacte al administrador."];
                                                                        $scope.loadingGuide=false;
                                                                        $scope.terminateGuide=true;
                                                                    }

                                                                }, function () {
                                                                    // error
                                                                    return $scope.failWsConsume();
                                                                });
                                                            }else{
                                                                $scope.failGuide=true;
                                                                $scope.failMsgGuide=[res.getElementsByTagName("descripcion")[0].textContent,"Error en WSDL de Red Servi"];
                                                                $scope.loadingGuide=false;
                                                            }


                                                        }, function () {
                                                            // error
                                                            $scope.failGuide=true;
                                                            $scope.failMsgGuide=["El servicio de corte de guia de Red Servi se encuentra inactivo. Por favor espere unos minutos e intentelo nuevamente.","Error en WSDL de Red Servi"];
                                                            $scope.loadingGuide=false;
                                                            return $scope.failWsConsume();
                                                        });





                                                    };

                                                    $scope.successAlr=true;
                                                    $scope.successMsgAlr=[res.getElementsByTagName("descripcion")[0].textContent+" Verifique los datos y si estan correctos proceda con el corte de guia.","Guia cotizada correctamente con operador logistico Red Servi."];
                                                    $scope.loadingAlr=false;


                                                }else {
                                                    $scope.failAlr=true;
                                                    $scope.failMsgAlr=[res.getElementsByTagName("descripcion")[0].textContent,"Error de Web Service Red Servi"];
                                                    $scope.loadingAlr=false;
                                                }

                                            }, function() {
                                                // error
                                                return $scope.failWsConsume();
                                            });


                                            console.log(trm);
                                        }else if(rCity.length===index+1 && cont===0){
                                            $scope.failAlr=true;
                                            $scope.failMsgAlr=["The city "+$capitalize(dCity)+" is not valid for the guide cut in Colombia, please rectify the Order in the administrator and try it again.","La ciudad "+$capitalize(dCity)+" no es válida para el corte de guía en Colombia, por favor rectifique la orden en el administrador e intentelo de nuevo."];
                                            $scope.loadingAlr=false;
                                        }

                                    });
                                }
                            }


                        }, function() {
                            // error
                            return $scope.failWsConsume();
                        });

                    }
                    else if(rState.length===indexS+1 && contS===0){
                        $scope.failAlr=true;
                        $scope.failMsgAlr=["The Department "+$capitalize(dState)+" is not valid for the guide cut in Colombia, please rectify the Order in the administrator and try it again.","El Departmento "+$capitalize(dState)+" no es válido para el corte de guía en Colombia, por favor rectifique la orden en el administrador e intentelo de nuevo."];
                        $scope.loadingAlr=false;
                    }
                });

            }, function() {
                // error
                return $scope.failWsConsume();
            });
        };


            };

    $scope.failWsConsume = function () {

        $scope.errorWs = true;
        $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
        alert($scope.errorWsMsg);

    };
    $scope.refresh = function(){
        $window.location.reload();
    };

}

function getCost($scope, $api, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize, FileUploader) {

    $scope.loading=false;
    $scope.success=false;
    $scope.fail=false;
    $scope.result=[];
    $scope.totalData=0;
    $scope.cost={
        trm:null
    };

    var uploader = $scope.uploader = new FileUploader({
        url: $globalParameters.AWSUrl+'upload',
        formData: [{
            dir:"nguides",
            trm: 2800
        }]
    });

    $scope.changeTRM = function (value) {
        $scope.uploader.formData[0].trm=value;
    };

    const dataT = {
        username: $globalParameters.api_client,
        password: $globalParameters.api_pswd,
        grant_type: $globalParameters.api_grant_type
    };
    $api.call('token','POST',dataT,true,true).then(function (rt){
        uploader.formData.push({tkn:rt.access_token})
    }, function() {
        // error
        return $scope.failWsConsume();
    });

    $scope.submitCostForm = function (isValid) {
        $scope.submitted = true;
        if(isValid){
            $scope.uploader.queue.upload();
        }
    };


    // FILTERS

    // only excel file
    uploader.filters.push({
        name: 'excelFile',
        fn: function(item, options) {
            console.log(item);
            return item.type==="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        }
    });

    // a sync filter
    uploader.filters.push({
        name: 'syncFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            console.log('syncFilter');
            return this.queue.length < 10;
        }
    });

    // an async filter
    uploader.filters.push({
        name: 'asyncFilter',
        fn: function(item /*{File|FileLikeObject}*/, options, deferred) {
            console.log('asyncFilter');
            setTimeout(deferred.resolve, 1e3);
        }
    });



    // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        $scope.loading=true;
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {

        $scope.success=true;
        if(response.table.length>0){
            $scope.noFound=false;
            var result=response.table,table = $('#tResult').DataTable();

            $scope.totalData=result.length;
                result.forEach(function(r,index) {

                    var row = table.row.add(
                        [ '<a target="_blank" href="https://admin.anicamenterprises.com/orders/redirect.aspx?op=update&OrderId='+r.OrderNumber+'&tabs=6">'+r.OrderNumber+'</a>',
                            r.BusinessLine,
                            r.ExternalGuide,
                            '<a target="_blank" href="https://redservi.almalogix.com/distribucion/scripts/procesamientoEnvios/pro00007.php?numero='+r.ExternalGuide+'"><img style="height: 35px" src="http://anicamboxexpress.com/css/quotation/attach.png" alt="Trazabilidad"></a>',
                            '<a target="_blank" href="https://guias.almalogix.com/img_guias//'+r.ExternalGuide+'.jpg"><img style="height: 35px" src="http://anicamboxexpress.com/css/quotation/attach.png" alt="Prueba de Entrega"></a>',
                            r.Cost.Dien,
                            r.Cost.Description,
                            r.Cost.SalePrice,
                            r.Cost.CostPrice,
                            r.Cost.utilityPrice,
                            r.Consignee.ConsigneeName,
                            r.Consignee.ConsigneeState,
                            r.Consignee.ConsigneeCity,
                            r.Consignee.ConsigneeAddress,
                            '<span>'+r.Consignee.PackageWeight+' Lb</span><br><span>'+(r.Consignee.PackageWeight*0.453592)+' Kl</span>',
                            r.Consignee.PackageHeight,
                            r.Consignee.PackageLength,
                            r.Consignee.PackageWidth ]
                    ).node();

                    if(r.Cost.utilityStatus===false){
                        $(row).addClass( 'bg-danger' );
                    }else if(r.Cost.utilityStatus==="none"){
                        $(row).addClass( 'bg-warning' );
                    }else if(r.Cost.utilityStatus==="duplicated"){
                        $(row).addClass( 'bg-info' );
                    }

                });

                table.draw();


        }else{
            $scope.noFound=true;
        }
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        $scope.loading=false;
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);

    $scope.failWsConsume = function () {

        $scope.errorWs = true;
        $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
        alert($scope.errorWsMsg);

    };
    $scope.refresh = function(){
        $window.location.reload();
    };

}

function amzTracking($scope, $api, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize, FileUploader) {

    $scope.loading=false;
    $scope.success=false;
    $scope.fail=false;
    $scope.result=[];
    $scope.totalData=0;
    $scope.cost={
        trm:null
    };

    var uploader = $scope.uploader = new FileUploader({
        url: $globalParameters.AWSUrl+'upload',
        formData: [{
            dir:"amztracking"
        }]
    });

    $scope.changeTRM = function (value) {
        $scope.uploader.formData[0].trm=value;
    };

    const dataT = {
        username: $globalParameters.api_client,
        password: $globalParameters.api_pswd,
        grant_type: $globalParameters.api_grant_type
    };
    $api.call('token','POST',dataT,true,true).then(function (rt){
        uploader.formData.push({tkn:rt.access_token})
    }, function() {
        // error
        return $scope.failWsConsume();
    });

    $scope.submitTrackingForm = function (isValid) {
        $scope.submitted = true;
        if(isValid){
            $scope.uploader.queue.upload();
        }
    };


    // FILTERS

    // only excel file
    uploader.filters.push({
        name: 'excelFile',
        fn: function(item, options) {
            console.log(item);
            return item.type==="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        }
    });

    // a sync filter
    uploader.filters.push({
        name: 'syncFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            console.log('syncFilter');
            return this.queue.length < 10;
        }
    });

    // an async filter
    uploader.filters.push({
        name: 'asyncFilter',
        fn: function(item /*{File|FileLikeObject}*/, options, deferred) {
            console.log('asyncFilter');
            setTimeout(deferred.resolve, 1e3);
        }
    });



    // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        $scope.loading=true;
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {

        $scope.success=true;
        if(response.dataResponse.items.length>0){
            $scope.noFound=false;
            var result=response.dataResponse.items,table = $('#tResult').DataTable();

            $scope.totalData=result.length;
            result.forEach(function(element) {

                var row = table.row.add(
                    [
                        element,
                        'Actualizado correctamente'
                    ]
                ).node();

            });

            table.draw();


        }else{
            $scope.noFound=true;
        }
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        $scope.loading=false;
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);







    $scope.failWsConsume = function () {

        $scope.errorWs = true;
        $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
        alert($scope.errorWsMsg);

    };
    $scope.refresh = function(){
        $window.location.reload();
    };

}

function ebayTracking($scope, $api, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize, FileUploader,$filter) {

    $scope.loading=false;
    $scope.success=false;
    $scope.fail=false;
    $scope.result=[];
    $scope.totalData=0;
    $scope.totalDataAll=0;
    $scope.cost={
        trm:null
    };
    $scope.qOrderStatus =[
        {name:'Seleccionar', value:''},
        {name:'Activas', value:'Active'},
        {name:'Todas', value:'All'},
        {name:'Canceladas', value:'Cancelled'},
        {name:'Pendiente por Cancelar', value:'CancelPending'},
        {name:'Completadas', value:'Completed'},
        {name:'Inactivas', value:'Inactive'},
        {name:'En proceso', value:'InProcess'}
    ];

    /*var now = new Date(),dateNowF=date.format(now,'YYYY-MM-DD');
    $scope.dateNowF=dateNowF;
    console.log(dateNowF);*/
    $scope.params={
        OrderStatus:"",
        OrderRole:"Buyer",
        CreateTimeFrom:"",
        CreateTimeTo:"",
        Pagination: {
            EntriesPerPage: 100,
            PageNumber: "1"
        }
    };

    $scope.totalPages=1;

    $scope.submitEbayForm = function(isValid){
        $scope.submitted = true;
        $scope.errMsg=false;

            if (isValid) {
                $scope.loading=true;
                var table = $('#tResult').DataTable();
                table.clear();

                $scope.params.CreateTimeFrom=new Date($scope.params.CreateTimeFrom).toISOString();
                $scope.params.CreateTimeTo=new Date($scope.params.CreateTimeTo).toISOString();
                console.log($scope.params);

                $api.ebayOrders({params:$scope.params}).then(function (rt) {

                    if(rt.orders.length>0){
                        $scope.noFound=false;
                        var result=rt.orders;

                        $scope.totalData=result.length;
                        $scope.totalDataAll=rt.totalResults;
                        $scope.loading=false;
                        $scope.totalPages=rt.totalPages;
                        $scope.params.Pagination.PageNumber=rt.pageNumber.toString();

                        result.forEach(function(element) {

                            element.orderDate=$filter('date')(element.orderDate, "dd/MM/yyyy","es_CO");
                            element.deliveryDate=$filter('date')(element.deliveryDate, "dd/MM/yyyy","es_CO");

                            var row = table.row.add(
                                [
                                    element.orderStatus,
                                    element.itemID,
                                    element.itemName,
                                    element.paymentMethod,
                                    element.amountPaid,
                                    element.customerName,
                                    element.orderDate,
                                    //element.orderID,
                                    element.orderA,
                                    element.orderA,
                                    element.deliveryDate,
                                    "ANI"+element.trackingNumber,
                                    element.quantity,
                                    element.carrier,
                                    element.sku
                                ]
                            ).node();

                            if(element.orderStatus!=="Completed"){
                                $(row).addClass( 'bg-danger' );
                            }

                        });

                        table.draw();
                    }else{
                        $scope.totalData=0;
                        $scope.totalDataAll=0;
                        $scope.loading=false;
                        $scope.noFound=true;
                        if(rt.error){
                            $scope.errMsg="Se genero un error: "+rt.error;
                        }else{
                            $scope.errMsg="Se ha generado un error por favor contacta a desarrollo";
                        }

                    }

                }, function() {
                    // error
                    return $scope.failWsConsume();
                });

            }
    };


    var uploader = $scope.uploader = new FileUploader({
        url: $globalParameters.AWSUrl+'upload',
        formData: [{
            dir:"amztracking"
        }]
    });



    const dataT = {
        username: $globalParameters.api_client,
        password: $globalParameters.api_pswd,
        grant_type: $globalParameters.api_grant_type
    };
    $api.call('token','POST',dataT,true,true).then(function (rt){
        uploader.formData.push({tkn:rt.access_token})
    }, function() {
        // error
        return $scope.failWsConsume();
    });

    $scope.submitTrackingForm = function (isValid) {
        $scope.submitted = true;
        if(isValid){
            $scope.uploader.queue.upload();
        }
    };


    uploader.onSuccessItem = function(fileItem, response, status, headers) {

        $scope.success=true;
        if(response.dataResponse.items.length>0){
            $scope.noFound=false;
            var result=response.dataResponse.items,table = $('#tResult').DataTable();

            $scope.totalData=result.length;
            result.forEach(function(element) {

                var row = table.row.add(
                    [
                        element,
                        'Actualizado correctamente'
                    ]
                ).node();

            });

            table.draw();


        }else{
            $scope.noFound=true;
        }
        console.info('onSuccessItem', fileItem, response, status, headers);
    };


    $scope.failWsConsume = function () {

        $scope.errorWs = true;
        $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
        alert($scope.errorWsMsg);

    };

    $scope.refresh = function(){
        $window.location.reload();
    };

}

function servientregaCost($scope, $api, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize, FileUploader) {

    $scope.loading=false;
    $scope.success=false;
    $scope.fail=false;
    $scope.result=[];
    $scope.totalData=0;
    $scope.cost={
        trm:null
    };

    var uploader = $scope.uploader = new FileUploader({
        url: $globalParameters.AWSUrl+'upload',
        formData: [{
            dir:"nservguides"
        }]
    });


    const dataT = {
        username: $globalParameters.api_client,
        password: $globalParameters.api_pswd,
        grant_type: $globalParameters.api_grant_type
    };
    $api.call('token','POST',dataT,true,true).then(function (rt){
        uploader.formData.push({tkn:rt.access_token})
    }, function() {
        // error
        return $scope.failWsConsume();
    });

    $scope.submitCostServForm = function (isValid) {
        $scope.submitted = true;
        if(isValid){
            $scope.uploader.queue.upload();
        }
    };


    // FILTERS

    // only excel file
    uploader.filters.push({
        name: 'excelFile',
        fn: function(item, options) {
            console.log(item);
            return item.type==="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        }
    });

    // a sync filter
    uploader.filters.push({
        name: 'syncFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            console.log('syncFilter');
            return this.queue.length < 10;
        }
    });

    // an async filter
    uploader.filters.push({
        name: 'asyncFilter',
        fn: function(item /*{File|FileLikeObject}*/, options, deferred) {
            console.log('asyncFilter');
            setTimeout(deferred.resolve, 1e3);
        }
    });



    // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        $scope.loading=true;
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {

        $scope.success=true;
        if(response.table.length>0){
            $scope.noFound=false;
            var result=response.table,table = $('#tResult').DataTable();

            $scope.totalData=result.length;
            result.forEach(function(r,index) {

                var row = table.row.add(
                    [
                        r.id_envio,
                        r.id_factura,
                        r.fecha_envio,
                        r.fecha_liquidacion,
                        r.codigo_arancelario,
                        r.id_estadoimpuesto,
                        r.forma_pago,
                        r.registro_dian,
                        r.fecha_registro,
                        r.pais_origen,
                        r.guia_master,
                        r.fecha_master,
                        r.flete_aereo,
                        r.arancel,
                        r.iva,
                        r.seguro,
                        r.peso_kilos_base,
                        r.valor_declarado,
                        r.flete_us,
                        r.seguro_us,
                        r.cif_us,
                        r.tasa_liquidacion,
                        r.cif_mlo,
                        r.arancel_mlo,
                        r.base_mlo_iva,
                        r.iva_mlo,
                        r.total_impuesto_mlo,
                        r.peso_kilos,
                        r.peso_libras,
                        r.order_number,
                        r.linea_negocio
                    ]
                ).node();

                if(r.linea_negocio==="" || r.linea_negocio===null){
                    $(row).addClass( 'bg-danger' );
                }

            });

            table.draw();

        }else{
            $scope.noFound=true;
        }
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        $scope.loading=false;
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);







    $scope.failWsConsume = function () {

        $scope.errorWs = true;
        $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
        alert($scope.errorWsMsg);

    };
    $scope.refresh = function(){
        $window.location.reload();
    };

}

function settlementCost($scope, $api, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize, FileUploader,$rdcLocalStorage,$translate) {

    const defLang = "es";

    $scope.languages=[
        {
            code:"es",
            text:"Español",
            uriFlag:"//www.anicamboxexpress.com/css/quotation/spain.png"
        },
        {
            code:"en",
            text:"English",
            uriFlag:"//www.anicamboxexpress.com/css/quotation/united-kingdom.png"
        }
    ];

    if ($rdcLocalStorage.get('dataLang') !== null && $rdcLocalStorage.get('dataLang') !== false) {
        $translate.use($rdcLocalStorage.get('dataLang').code);
    }else{
        $translate.use(defLang);
    }

    $.each($scope.languages, function (index, value) {
        if(value.code===$translate.use()){
            $scope.language=value;
        }
    });

    $scope.loading=false;
    $scope.success=false;
    $scope.fail=false;
    $scope.result=[];
    $scope.totalData=0;
    $scope.cost={
        trm:null
    };

    var uploader = $scope.uploader = new FileUploader({
        url: $globalParameters.AWSUrl+'upload',
        formData: [{
            dir:"settlement_guides"
        }]
    });


    const dataT = {
        username: $globalParameters.api_client,
        password: $globalParameters.api_pswd,
        grant_type: $globalParameters.api_grant_type
    };
    $api.call('token','POST',dataT,true,true).then(function (rt){
        uploader.formData.push({tkn:rt.access_token})
    }, function() {
        // error
        return $scope.failWsConsume();
    });

    $scope.submitCostServForm = function (isValid) {
        $scope.submitted = true;
        if(isValid){
            $scope.uploader.queue.upload();
        }
    };


    // FILTERS

    // only excel file
    uploader.filters.push({
        name: 'excelFile',
        fn: function(item, options) {
            console.log(item);
            return item.type==="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        }
    });

    // a sync filter
    uploader.filters.push({
        name: 'syncFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            console.log('syncFilter');
            return this.queue.length < 10;
        }
    });

    // an async filter
    uploader.filters.push({
        name: 'asyncFilter',
        fn: function(item /*{File|FileLikeObject}*/, options, deferred) {
            console.log('asyncFilter');
            setTimeout(deferred.resolve, 1e3);
        }
    });



    // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        $scope.loading=true;
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {

        $scope.success=true;
        if(response.data.length>0){
            $scope.noFound=false;
            var result=response.data,table = $('#tResult').DataTable(),fob=0;

            $scope.totalData=result.length;
            result.forEach(function(r,index) {
                r.Fob ? fob=r.Fob : fob=0
                var row = table.row.add(
                    [
                        r.Guia,
                        r.Tributo,
                        fob
                    ]
                ).node();

                /*if(r.linea_negocio==="" || r.linea_negocio===null){
                    $(row).addClass( 'bg-danger' );
                }*/

            });

            table.draw();

        }else{
            $scope.noFound=true;
        }
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        $scope.loading=false;
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);


    $scope.failWsConsume = function () {

        $scope.errorWs = true;
        $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
        alert($scope.errorWsMsg);

    };
    $scope.refresh = function(){
        $window.location.reload();
    };

}

function prealertPlus($scope, $api, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize, FileUploader,$rdcLocalStorage,$translate,$filter) {

    const defLang = "es";
    $scope.btnVal=$filter('translate')('Validate');

    $scope.languages=[
        {
            code:"es",
            text:"Español",
            uriFlag:"//www.anicamboxexpress.com/css/quotation/spain.png"
        },
        {
            code:"en",
            text:"English",
            uriFlag:"//www.anicamboxexpress.com/css/quotation/united-kingdom.png"
        }
    ];

    if ($rdcLocalStorage.get('dataLang') !== null && $rdcLocalStorage.get('dataLang') !== false) {
        $translate.use($rdcLocalStorage.get('dataLang').code);
    }else{
        $translate.use(defLang);
    }

    $.each($scope.languages, function (index, value) {
        if(value.code===$translate.use()){
            $scope.language=value;
        }
    });

    $scope.loading=false;
    $scope.success=false;
    $scope.fail=false;
    $scope.result=[];
    $scope.totalData=0;
    $scope.cost={
        trm:null,
        mode:""
    };

    $scope.validUsrStatus=false;
    $scope.terminateAlr=false;

    var uploader = $scope.uploader = new FileUploader({
        url: $globalParameters.AWSUrl+'upload',
        formData: [{
            dir:"palerts"
        }]
    });

    $scope.validUsr = function () {
        $scope.btnVal=$filter('translate')('Validating');
        $scope.loadVal=true;

        const dataT = {
            username: $globalParameters.api_client,
            password: $globalParameters.api_pswd,
            grant_type: $globalParameters.api_grant_type
        };
        $api.call('token','POST',dataT,true,true).then(function (r){

            var data = {
                email: $scope.aditionals.email,
                token: r.access_token
            };

            uploader.formData.push({tkn:data.token});

            $api.validateLocker(data).then(function (r1) {

                if(r1.message !== undefined && r1.error===true){
                    $scope.sucMsg=false;
                    $scope.validUsrStatus=false;
                    $scope.errMsg = r1.message;
                }else if (r1.account_number !== undefined){
                    $scope.errMsg=false;
                    $scope.sucMsg = 'El e-mail es valido y el número de casillero es: '+r1.account_number;
                    $scope.validUsrStatus=true;

                    // Disable #email
                    $( "#email" ).prop( "disabled", true );
                    $( "#validateEmail" ).prop( "disabled", true );

                    var data1 = {
                        token : data.token,
                        user_id : r1.user_id
                    };

                    $api.detailUser(data1).then(function (rdetail) {

                        $scope.imgProfile=rdetail.avatar;
                        $scope.msgCost="El valor de este servicio es gratis";

                        if(rdetail.business_line_id && rdetail.user_id){

                            console.log(rdetail);
                            uploader.formData.push({business_line_id:rdetail.business_line_id,
                            user_id:rdetail.user_id,
                                customer:rdetail.first_name+' '+rdetail.last_name,
                                email:rdetail.email,
                                identification:rdetail.identification,
                                country_locale:rdetail.billing[0].country_locale,
                                state:rdetail.billing[0].state,
                                city:rdetail.billing[0].city,
                                address:rdetail.billing[0].address,
                                zip_code:rdetail.billing[0].zip_code,
                                phone_1:rdetail.billing[0].phone_1,
                                phone_2:rdetail.billing[0].phone_2
                            });

                        }else if (rdetail.status === 404 ){
                            alert('Error al traer los datos del usuario');
                        }else {
                            $scope.failWsConsume();
                        }
                    }, function () {
                        // error
                        return $scope.failWsConsume();
                    });
                }

                $scope.loadVal=false;
                $scope.btnVal=$filter('translate')('Validate');

            }, function () {
                // error
                return $scope.failWsConsume();
            });

        }, function () {
            // error
            return $scope.failWsConsume();
        });
    };




    $scope.submitAlertsForm = function (isValid) {
        $scope.submitted = true;
        console.log("how here!");
        if(isValid){
            $scope.uploader.queue.upload();
        }
    };

    $scope.resetForm=function(){
        $( "#email" ).prop( "disabled", false );
        $( "#validateEmail" ).prop( "disabled", false );
        $( "#mode" ).prop( "disabled", false );
        $scope.sucMsg = false;
        $scope.validUsrStatus=false;
        uploader.formData=[{
            dir:"palerts"
        }];
        console.log(uploader.formData);
    };

    $scope.changeMode = function () {
        if($scope.cost.mode){
            uploader.formData.push({mode: $scope.cost.mode});
            // Disable #x
            $( "#mode" ).prop( "disabled", true );
            console.log(uploader.formData);
        }
    };



    // FILTERS

    // only excel file
    uploader.filters.push({
        name: 'excelFile',
        fn: function(item, options) {
            console.log(item);
            return item.type==="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        }
    });

    // a sync filter
    uploader.filters.push({
        name: 'syncFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            console.log('syncFilter');
            return this.queue.length < 10;
        }
    });

    // an async filter
    uploader.filters.push({
        name: 'asyncFilter',
        fn: function(item /*{File|FileLikeObject}*/, options, deferred) {
            console.log('asyncFilter');
            setTimeout(deferred.resolve, 1e3);
        }
    });



    // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        $scope.loading=true;
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {

        $scope.success=true;
        var result="",table=$('#tResult').DataTable();
        if(response.dataResponse.order_number){
            $scope.noFound=false;
            $scope.totalData=1;
                table.row.add(
                    [
                        response.dataResponse.comment+' <strong>Número de Orden: '+response.dataResponse.order_number+'</strong>'
                    ]
                ).node();
            table.draw();
        }else if(response.dataResponse.result.length>0){
            $scope.noFound=false;
            result=response.dataResponse.result;
            $scope.totalData=response.total;
            result.forEach(function(r,index) {
                table.row.add(
                    [
                        r
                    ]
                ).node();
            });
            table.draw();
        }
        else if(response.dataResponse.cause && response.dataResponse.cause.length>0){
            $scope.noFound=false;
            result=response.dataResponse.cause;

            $scope.totalData=response.total;
            result.forEach(function(r,index) {
                table.row.add(
                    [
                        r
                    ]
                ).node();

            });

            table.draw();
        }else{
            $scope.noFound=true;
        }
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        $scope.loading=false;
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);


    $scope.failWsConsume = function () {

        $scope.errorWs = true;
        $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
        alert($scope.errorWsMsg);

    };
    $scope.refresh = function(){
        $window.location.reload();
    };

}

function yaxaExcel($scope, $api, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize, FileUploader,$rdcLocalStorage,$translate,$filter) {

    const defLang = "es";
    $scope.btnVal=$filter('translate')('Validate');

    $scope.languages=[
        {
            code:"es",
            text:"Español",
            uriFlag:"//www.anicamboxexpress.com/css/quotation/spain.png"
        },
        {
            code:"en",
            text:"English",
            uriFlag:"//www.anicamboxexpress.com/css/quotation/united-kingdom.png"
        }
    ];

    if ($rdcLocalStorage.get('dataLang') !== null && $rdcLocalStorage.get('dataLang') !== false) {
        $translate.use($rdcLocalStorage.get('dataLang').code);
    }else{
        $translate.use(defLang);
    }

    $.each($scope.languages, function (index, value) {
        if(value.code===$translate.use()){
            $scope.language=value;
        }
    });

    $scope.loading=false;
    $scope.success=false;
    $scope.fail=false;
    $scope.result=[];
    $scope.totalData=0;
    $scope.user={
        sellerId:"",
        email:"",
        userValidate:false
    };

    $scope.validUsrStatus=false;
    $scope.terminateAlr=false;

    var uploader = $scope.uploader = new FileUploader({
        url: $globalParameters.AWSUrl+'upload',
        formData: [{
            dir:"yaxaexcel"
        }]
    });

    $scope.validUsr = function () {
        $scope.btnVal=$filter('translate')('Validating');
        $scope.loadVal=true;

        const dataT = {
            username: $globalParameters.api_client,
            password: $globalParameters.api_pswd,
            grant_type: $globalParameters.api_grant_type
        };
        $api.call('token','POST',dataT,true,true).then(function (r){

            var data = {
                email: $scope.user.email,
                token: r.access_token
            };

            $api.validateLocker(data).then(function (r1) {

                if(r1.message !== undefined && r1.error===true){
                    $scope.sucMsg=false;
                    $scope.validUsrStatus=false;
                    $scope.errMsg = r1.message;
                }else if (r1.account_number !== undefined){

                    $( "#email" ).prop( "disabled", true );
                    $( "#sellerId" ).prop( "disabled", true );

                    var data1 = {
                        token : data.token,
                        user_id : r1.user_id
                    };

                    $api.detailUser(data1).then(function (rdetail) {

                        if(rdetail.business_line_id && rdetail.user_id){

                            var dataReq = {
                                seller_id : $scope.user.sellerId,
                                user_id : rdetail.user_id
                            };

                            $api.validUser(dataReq).then(function (resVUser) {

                                if(resVUser.status===200){
                                    $scope.errMsg=false;
                                    $scope.sucMsg = 'Acceso concebido a Id Vendedor: '+dataReq.seller_id;
                                    $scope.validUsrStatus=true;

                                    $scope.imgProfile=rdetail.avatar;
                                    $scope.msgCost="El valor de este servicio es gratis";

                                    uploader.formData.push({
                                        seller_id : dataReq.seller_id,
                                        user_id: dataReq.user_id,
                                        id: resVUser.data.id,
                                        app_id: resVUser.data.app_id,
                                        secret_key: resVUser.data.secret_key
                                    });

                                }else{
                                    // Disable #email
                                    $( "#email" ).prop( "disabled", false );
                                    $( "#sellerId" ).prop( "disabled", false );

                                    $scope.sucMsg=false;
                                    $scope.validUsrStatus=false;
                                    $scope.errMsg = resVUser.error;

                                }

                            }, function () {
                                // error
                                return $scope.failWsConsume();
                            });

                        }else if (rdetail.status === 404 ){
                            alert('Error al traer los datos del usuario');
                        }else {
                            $scope.failWsConsume();
                        }
                    }, function () {
                        // error
                        return $scope.failWsConsume();
                    });
                }

                $scope.loadVal=false;
                $scope.btnVal=$filter('translate')('Validate');

            }, function () {
                // error
                return $scope.failWsConsume();
            });

        }, function () {
            // error
            return $scope.failWsConsume();
        });
    };


    $scope.submitAlertsForm = function (isValid) {
        $scope.submitted = true;
        console.log("how here!");
        if(isValid){
            $scope.uploader.queue.upload();
        }
    };

    $scope.resetForm=function(){
        $( "#email" ).prop( "disabled", false );
        $( "#sellerId" ).prop( "disabled", false );
        $scope.sucMsg = false;
        $scope.errMsg = false;
        $scope.validUsrStatus=false;
        uploader.formData=[{
            dir:"yaxaexcel"
        }];
        console.log(uploader.formData);
    };


    // FILTERS

    // only excel file
    uploader.filters.push({
        name: 'excelFile',
        fn: function(item, options) {
            console.log(item);
            return item.type==="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        }
    });

    // a sync filter
    uploader.filters.push({
        name: 'syncFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            console.log('syncFilter');
            return this.queue.length < 10;
        }
    });

    // an async filter
    uploader.filters.push({
        name: 'asyncFilter',
        fn: function(item /*{File|FileLikeObject}*/, options, deferred) {
            console.log('asyncFilter');
            setTimeout(deferred.resolve, 1e3);
        }
    });



    // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        $scope.loading=true;
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {

        $scope.success=true;
        var result="",table=$('#tResult').DataTable();
        if(response.dataResponse.order_number){
            $scope.noFound=false;
            $scope.totalData=1;
            table.row.add(
                [
                    response.dataResponse.comment+' <strong>Número de Orden: '+response.dataResponse.order_number+'</strong>'
                ]
            ).node();
            table.draw();
        }else if(response.dataResponse.result.length>0){
            $scope.noFound=false;
            result=response.dataResponse.result;
            $scope.totalData=response.total;
            result.forEach(function(r,index) {
                table.row.add(
                    [
                        r
                    ]
                ).node();
            });
            table.draw();
        }
        else if(response.dataResponse.cause && response.dataResponse.cause.length>0){
            $scope.noFound=false;
            result=response.dataResponse.cause;

            $scope.totalData=response.total;
            result.forEach(function(r,index) {
                table.row.add(
                    [
                        r
                    ]
                ).node();

            });

            table.draw();
        }else{
            $scope.noFound=true;
        }
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        $scope.loading=false;
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);


    $scope.failWsConsume = function () {

        $scope.errorWs = true;
        $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
        alert($scope.errorWsMsg);

    };
    $scope.refresh = function(){
        $window.location.reload();
    };

}

function prealertPurchasesPlus($scope, $api, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize, FileUploader,$rdcLocalStorage,$translate,$filter) {

    const defLang = "es";
    $scope.btnVal=$filter('translate')('Validate');

    $scope.languages=[
        {
            code:"es",
            text:"Español",
            uriFlag:"//www.anicamboxexpress.com/css/quotation/spain.png"
        },
        {
            code:"en",
            text:"English",
            uriFlag:"//www.anicamboxexpress.com/css/quotation/united-kingdom.png"
        }
    ];

    if ($rdcLocalStorage.get('dataLang') !== null && $rdcLocalStorage.get('dataLang') !== false) {
        $translate.use($rdcLocalStorage.get('dataLang').code);
    }else{
        $translate.use(defLang);
    }

    $.each($scope.languages, function (index, value) {
        if(value.code===$translate.use()){
            $scope.language=value;
        }
    });

    $scope.loading=false;
    $scope.success=false;
    $scope.fail=false;
    $scope.result=[];
    $scope.totalData=0;
    $scope.cost={
        trm:null
    };

    $scope.validUsrStatus=false;
    $scope.terminateAlr=false;

    var uploader = $scope.uploader = new FileUploader({
        url: $globalParameters.AWSUrl+'upload',
        formData: [{
            dir:"palertspur"
        }]
    });


    $scope.validUsr = function () {
        $scope.btnVal=$filter('translate')('Validating');
        $scope.loadVal=true;

        const dataT = {
            username: $globalParameters.api_client,
            password: $globalParameters.api_pswd,
            grant_type: $globalParameters.api_grant_type
        };
        $api.call('token','POST',dataT,true,true).then(function (r){

            var data = {
                email: $scope.aditionals.email,
                token: r.access_token
            };

            uploader.formData.push({tkn:data.token});

            $api.validateLocker(data).then(function (r1) {

                if(r1.message !== undefined && r1.error===true){
                    $scope.sucMsg=false;
                    $scope.validUsrStatus=false;
                    $scope.errMsg = r1.message;
                }else if (r1.account_number !== undefined){
                    $scope.errMsg=false;
                    $scope.sucMsg = 'El e-mail es valido y el número de casillero es: '+r1.account_number;
                    $scope.validUsrStatus=true;


                    var data1 = {
                        token : data.token,
                        user_id : r1.user_id
                    };

                    $api.detailUser(data1).then(function (rdetail) {

                        $scope.imgProfile=rdetail.avatar;
                        $scope.msgCost="El valor de este servicio es gratis";

                        if(rdetail.business_line_id && rdetail.user_id){

                            console.log(rdetail);
                            uploader.formData.push({business_line_id:rdetail.business_line_id,
                            user_id:rdetail.user_id
                            });

                        }else if (rdetail.status === 404 ){
                            alert('Error al traer los datos del usuario');
                        }else {
                            $scope.failWsConsume();
                        }
                    }, function () {
                        // error
                        return $scope.failWsConsume();
                    });
                }

                $scope.loadVal=false;
                $scope.btnVal=$filter('translate')('Validate');

            }, function () {
                // error
                return $scope.failWsConsume();
            });

        }, function () {
            // error
            return $scope.failWsConsume();
        });
    };




    $scope.submitCostServForm = function (isValid) {
        $scope.submitted = true;
        if(isValid){
            $scope.uploader.queue.upload();
        }
    };


    // FILTERS

    // only excel file
    uploader.filters.push({
        name: 'excelFile',
        fn: function(item, options) {
            console.log(item);
            return item.type==="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        }
    });

    // a sync filter
    uploader.filters.push({
        name: 'syncFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            console.log('syncFilter');
            return this.queue.length < 10;
        }
    });

    // an async filter
    uploader.filters.push({
        name: 'asyncFilter',
        fn: function(item /*{File|FileLikeObject}*/, options, deferred) {
            console.log('asyncFilter');
            setTimeout(deferred.resolve, 1e3);
        }
    });



    // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        $scope.loading=true;
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {

        $scope.success=true;
        if(response.dataResponse.result.length>0){
            $scope.noFound=false;
            var result=response.dataResponse.result,table = $('#tResult').DataTable();

            $scope.totalData=response.total;
            result.forEach(function(r,index) {
                table.row.add(
                    [
                        r
                    ]
                ).node();

            });

            table.draw();

        }else{
            $scope.noFound=true;
        }
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        $scope.loading=false;
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);


    $scope.failWsConsume = function () {

        $scope.errorWs = true;
        $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
        alert($scope.errorWsMsg);

    };
    $scope.refresh = function(){
        $window.location.reload();
    };

}

function amzCost($scope, $api, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize, FileUploader) {

    $scope.loading=false;
    $scope.success=false;
    $scope.fail=false;
    $scope.result=[];
    $scope.totalData=0;
    $scope.cost={
        trm:null
    };

    var uploader = $scope.uploader = new FileUploader({
        url: $globalParameters.AWSUrl+'upload',
        formData: [{
            dir:"nordersamz"
        }]
    });


    const dataT = {
        username: $globalParameters.api_client,
        password: $globalParameters.api_pswd,
        grant_type: $globalParameters.api_grant_type
    };
    $api.call('token','POST',dataT,true,true).then(function (rt){
        uploader.formData.push({tkn:rt.access_token})
    }, function() {
        // error
        return $scope.failWsConsume();
    });

    $scope.submitCostForm = function (isValid) {
        $scope.submitted = true;
        if(isValid){
            $scope.uploader.queue.upload();
        }
    };


    // FILTERS

    // only excel file
    uploader.filters.push({
        name: 'excelFile',
        fn: function(item, options) {
            console.log(item);
            return item.type==="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        }
    });

    // a sync filter
    uploader.filters.push({
        name: 'syncFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            console.log('syncFilter');
            return this.queue.length < 10;
        }
    });

    // an async filter
    uploader.filters.push({
        name: 'asyncFilter',
        fn: function(item /*{File|FileLikeObject}*/, options, deferred) {
            console.log('asyncFilter');
            setTimeout(deferred.resolve, 1e3);
        }
    });



    // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        $scope.loading=true;
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {

        $scope.success=true;
        if(response.table !==null && response.table.length>0){
            $scope.noFound=false;
            var result=response.table,table = $('#tResult').DataTable();

            $scope.totalData=result.length;
            result.forEach(function(r,index) {

                var row = table.row.add(
                    [ '<a target="_blank" href="https://admin.anicamenterprises.com/orders/redirect.aspx?op=update&OrderId='+r.OrderId+'&tabs=6">'+r.OrderId+'</a>',
                        r.BusinessLine,
                        r.Cost.Dien,
                        r.Cost.Quantity,
                        r.Cost.BuyingCost,
                        r.Cost.AmzCost,
                        r.Cost.utilityPrice,
                        r.Cost.Status,
                        r.Cost.SAPCode
                    ]
                ).node();

                if(r.Cost.utilityStatus===false){
                    $(row).addClass( 'bg-danger' );
                }else if(r.Cost.utilityStatus==="none"){
                    $(row).addClass( 'bg-warning' );
                }else if(r.Cost.utilityStatus==="duplicated"){
                    $(row).addClass( 'bg-info' );
                }

            });

            table.draw();


        }else{
            $scope.noFound=true;
        }
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        $scope.loading=false;
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);







    $scope.failWsConsume = function () {

        $scope.errorWs = true;
        $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
        alert($scope.errorWsMsg);

    };
    $scope.refresh = function(){
        $window.location.reload();
    };

}

function ssOne($scope, $api, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize) {

    $scope.loading=false;
    $scope.success=false;
    $scope.fail=false;
    $scope.result=[];
    $scope.totalData=0;
    $scope.one={};
    $scope.qCategories=[];
    $scope.qSubCategories=[];
    $scope.cost={
        trm:null
    };

    $scope.updateSubCats = function (val) {
        $scope.qSubCategories=[];
        $scope.one.subcat="";
        $api.ssoCategories({}).then(function (resCat) {
            $.each(resCat.categories, function(index, value){
                if(value.link===val){
                    var resSubCat=value.subcategories;
                    $.each(resSubCat, function(index2, value2){
                        $scope.qSubCategories.push({name:value2.name,value:value2.link});
                    });
                }
            });
        }, function(){
            $scope.failWsConsume();
        });
    };

    $api.ssoCategories({}).then(function (resCat) {

        $.each(resCat.categories, function(index, value){
          $scope.qCategories.push({name:value.name,value:value.link})
        });

    }, function(){
       $scope.failWsConsume();
    });



    $scope.submitOneForm = function (isValid) {
        $scope.submitted = true;
        if(isValid){
            $scope.loading=true;

            const category=$("#cat option:selected" ).text()+"|"+$("#subcat option:selected" ).text();

            $api.ssoProducts({uri:$scope.one.subcat.substring(1),category:category}).then(function (resCat) {

                var result=resCat.data,table=$('#tResult').DataTable();

                table.clear();

                if(resCat.status===200 && result.length>0){

                    result.forEach(function(r,index) {

                        if(!r.Code){
                            r.Code="99999";
                        }
                        var row = table.row.add(
                            [
                                "", // Id Interno A
                                r.Title, // B
                                r.Brand, // C
                                r.DescriptionHTML, // D
                                r.Description, // E
                                "", //F
                                "", //G
                                r.Package_height, // H
                                r.Package_width, // I
                                r.Package_length, // J
                                r.Package_weight, // K
                                "", // L
                                category, //M

                                r.Price, // N
                                r.ListPrice, // O Sale Price
                                r.Price, // P
                                r.LowPrice, // Q DistributorPrice
                                "", // R
                                "", // S

                                r.Code, // T
                                r.Code, // U
                                "", // V
                                "", // W
                                r.Size, // X
                                r.Quantity, // Y
                                "", // Z
                                "", //AA
                                "", //AB

                                r.Code, // AC Model
                                0, // AD
                                1, // AE
                                r.Color, // AF
                                "USA", // AG
                                r.Item_weight, // AH
                                "", // AI

                                r.LargeImage, // AJ
                                "", //AK
                                "", //AL
                                "", //AM
                                "", //AN
                                "", //AO
                                "", //AP
                                "", //AQ

                                r.Code, //AR
                                "", //AS
                                "", //AT
                                r.Notes, //AU
                                "", //AV
                                "" //AW
                            ]
                        ).node();

                        console.log("Indice ",index, " of ",result.length);

                        if(index+1===result.length){
                            $scope.loading=false;
                            $scope.success=true;
                            $scope.noFound=false;
                            $scope.totalData=result.length;
                        }

                    });

                    table.draw();
                }else{
                    $scope.noFound=true;
                }

            }, function(){
                $scope.failWsConsume();
            });
        }
    };

    $scope.failWsConsume = function () {

        $scope.errorWs = true;
        $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
        alert($scope.errorWsMsg);

    };
    $scope.refresh = function(){
        $window.location.reload();
    };

}

function ssOneSync($scope, $api, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize) {

    $scope.loading=false;
    $scope.success=false;
    $scope.fail=false;
    $scope.brands=[];
    $scope.cat=false;
    $scope.categories={
        p:[]
    };
    $scope.subCategories={
        p:[]
    };
    $scope.products={
        p:[]
    };

    $api.listB2BCategories({idCategoryRoot:0}).then(function (resCat) {
        $.each(resCat.data, function(index, value){
            $scope.brands.push(value);
        });
    }, function(){
        $scope.failWsConsume();
    });

    var image="";
    $scope.getCategory = function (brand) {
        if(brand.title==="SIX SIX ONE"){
            $scope.qCategories=[];
            $api.ssoCategories({}).then(function (resCat) {
                $.each(resCat.categories, function(index, value){
                    if(value.image){
                        image=value.image;
                    }else{
                        image=brand.image;
                    }
                    $scope.qCategories.push({
                        CategoryRoot:brand.title,
                        Title:value.name,
                        TitleSP:value.name,
                        UriBase: brand.uri,
                        Uri:brand.uri+value.link,
                        Image:image,
                        Factor:brand.factor,
                        Utilidad:brand.utilidad,
                        FactorAmazon:brand.factorAmazon,
                        SubCategory:value.subcategories,
                        IdCategoryRoot: brand.idCategoryRoot
                    })
                });
            }, function(){
                $scope.failWsConsume();
            });
        }else if(brand.title==="PYLE USA"){
            $scope.qCategories=[];
            $api.pyleCategories({}).then(function (resCat) {
                $.each(resCat.categories, function(index, value){
                    if(value.image){
                        image=value.image;
                    }else{
                        image=brand.image;
                    }
                    $scope.qCategories.push({
                        CategoryRoot:brand.title,
                        Title:value.name,
                        TitleSP:value.name,
                        UriBase: brand.uri,
                        Uri:brand.uri+value.link,
                        Image:image,
                        Factor:brand.factor,
                        Utilidad:brand.utilidad,
                        FactorAmazon:brand.factorAmazon,
                        SubCategory:[],
                        IdCategoryRoot: brand.idCategoryRoot
                    });
                });
            }, function(){
                $scope.failWsConsume();
            });
        }
    };

    $scope.getSubCategory = function (elm){
        $scope.qSubCategories=[];
        if(elm){
            console.log(elm);
            if(elm.CategoryRoot==="SIX SIX ONE"){
                            $.each(elm.SubCategory, function(index, value){
                                if(value.image){
                                    image=value.image;
                                }else{
                                    image=elm.Image;
                                }
                                $scope.qSubCategories.push({
                                    CategoryRoot:elm.Title,
                                    Title:value.name,
                                    TitleSP:value.name,
                                    UriBase: elm.UriBase,
                                    Uri:elm.UriBase+value.link,
                                    Image:image,
                                    Factor:elm.Factor,
                                    Utilidad:elm.Utilidad,
                                    FactorAmazon:elm.FactorAmazon,
                                    SubCategory:[],
                                    IdCategoryRoot: elm.IdCategoryRoot
                                });
                            });
            }
        }
    };

    $scope.getProducts = function (elm){
        console.log(elm);
    };

    $scope.failWsConsume = function () {

        $scope.errorWs = true;
        $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
        alert($scope.errorWsMsg);

    };
    $scope.refresh = function(){
        $window.location.reload();
    };

}

function pyleAudio($scope, $api, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize,FileUploader,$rdcLocalStorage,$translate,$filter) {

   // document.body.style.zoom = "75%";
    $scope.loading=false;
    $scope.loadingUpd=false;
    $scope.success=false;
    $scope.fail=false;
    $scope.result=[];
    $scope.totalData=0;
    $scope.totalDataUpdate=0;
    $scope.totalDataSuccess=0;
    $scope.totalDataFail=0;
    $scope.one={};
    $scope.qCategories=[];
    $scope.qSubCategories=[];
    $scope.cost={
        trm:null
    };

    $scope.updateSubCats = function (val) {
        $scope.qSubCategories=[];
        $scope.one.subcat="";
        $api.pyleCategories({}).then(function (resCat) {
            $.each(resCat.categories, function(index, value){
                if(value.link===val){
                    var resSubCat=value.subcategories;
                    $.each(resSubCat, function(index2, value2){
                        $scope.qSubCategories.push({name:value2.name,value:value2.link});
                    });
                }
            });
        }, function(){
            $scope.failWsConsume();
        });
    };


    $scope.submitPyleForm = function (isValid) {
        $scope.submitted = true;
        if(isValid){
            $scope.loading=true;

            const category=$("#cat option:selected" ).text()+"|"+$("#subcat option:selected" ).text();

            $api.ssoProducts({uri:$scope.one.subcat.substring(1),category:category}).then(function (resCat) {

                var result=resCat.data,table=$('#tResult').DataTable();

                table.clear();

                if(resCat.status===200 && result.length>0){

                    result.forEach(function(r,index) {

                        if(!r.Code){
                            r.Code="99999";
                        }
                        var row = table.row.add(
                            [
                                "", // Id Interno A
                                r.Title, // B
                                r.Brand, // C
                                r.DescriptionHTML, // D
                                r.Description, // E
                                "", //F
                                "", //G
                                r.Package_height, // H
                                r.Package_width, // I
                                r.Package_length, // J
                                r.Package_weight, // K
                                "", // L
                                category, //M

                                r.Price, // N
                                r.ListPrice, // O Sale Price
                                r.Price, // P
                                r.LowPrice, // Q DistributorPrice
                                "", // R
                                "", // S

                                r.Code, // T
                                r.Code, // U
                                "", // V
                                "", // W
                                r.Size, // X
                                r.Quantity, // Y
                                "", // Z
                                "", //AA
                                "", //AB

                                r.Code, // AC Model
                                0, // AD
                                1, // AE
                                r.Color, // AF
                                "USA", // AG
                                r.Item_weight, // AH
                                "", // AI

                                r.LargeImage, // AJ
                                "", //AK
                                "", //AL
                                "", //AM
                                "", //AN
                                "", //AO
                                "", //AP
                                "", //AQ

                                r.Code, //AR
                                "", //AS
                                "", //AT
                                r.Notes, //AU
                                "", //AV
                                "" //AW
                            ]
                        ).node();

                        console.log("Indice ",index, " of ",result.length);

                        if(index+1===result.length){
                            $scope.loading=false;
                            $scope.success=true;
                            $scope.noFound=false;
                            $scope.totalData=result.length;
                        }

                    });

                    table.draw();
                }else{
                    $scope.noFound=true;
                }

            }, function(){
                $scope.failWsConsume();
            });
        }
    };

    $scope.toUpdateB2B = function(){
        $scope.loadingUpd=true;
        $api.updateB2B($scope.reqUpdate).then(function (resUpd) {

            var table=$('#tResultUpdate').DataTable();
            $scope.totalDataUpdate=resUpd.length;
            table.clear();

            resUpd.forEach(function(r,index) {

                var row = table.row.add(
                    [ '<a target="_blank" href="http://b2b.anicamenterprises.com/umbraco#/content/content/edit/'+r.idB2B+'">'+r.idB2B+'</a>',
                        '<a target="_blank" href="http://www.pyleaudio.com/sku/'+r.sku+'">'+r.sku+'</a>',
                        r.response]
                ).node();

                if(r.status!==200){
                    $(row).addClass( 'bg-danger' );
                    $('a ',row).addClass( 'text-gray-100' );
                }

            });

            table.draw();
            $scope.loadingUpd=false;

        }, function(){
            $scope.failWsConsume();
        });
    };

    var uploader = $scope.uploader = new FileUploader({
        url: $globalParameters.AWSUrl+'upload',
        formData: [{
            dir:"stockpyle"
        }]
    });

    var uploaderNew = $scope.uploaderNew = new FileUploader({
        url: $globalParameters.WSUrl+'upload',
        formData: [{
            dir:"pylenew"
        }]
    });


    // CALLBACKS

    uploader.onBeforeUploadItem = function(item) {
        $scope.loading=true;
        console.info('onBeforeUploadItem', item);
    };

    uploader.onSuccessItem = function(fileItem, response, status, headers) {

        $scope.success=true;
        if(response.status===200){
            $scope.noFound=false;

            var result=response,table=$('#tResultB2B').DataTable(),tableN=$('#tResultNOB2B').DataTable();
            table.clear();
            tableN.clear();

            $scope.totalData=response.totalItems;
            $scope.totalDataSuccess=response.totalSuccess;
            $scope.totalDataFail=response.totalFail;

            result.dataSuccess.forEach(function(r,index) {

                var row = table.row.add(
                    [ r.id,
                        '<a target="_blank" href="https://www.amazon.com/dp/'+r.code+'">'+r.code+'</a>',
                        '<a target="_blank" href="http://www.pyleaudio.com/sku/'+r.sku+'">'+r.sku+'</a>',
                        r.hsCode,
                        r.quantity,
                        r.brand,
                        r.price,
                        r.salePrice,
                        r.dealerPrice,
                        r.distributorPrice]
                ).node();

                /*if(r.Cost.utilityStatus===false){
                    $(row).addClass( 'bg-danger' );
                }else if(r.Cost.utilityStatus==="none"){
                    $(row).addClass( 'bg-warning' );
                }else if(r.Cost.utilityStatus==="duplicated"){
                    $(row).addClass( 'bg-info' );
                }*/

            });

            table.draw();
            $scope.reqUpdate={data:result.dataSuccess};

            result.dataFail.forEach(function(r,index) {

                var row2 = tableN.row.add(
                    [ r.brand,
                        '<a target="_blank" href="http://www.pyleaudio.com/sku/'+r.sku+'">'+r.sku+'</a>',
                        r.quantity,
                        r.price,
                        r.hsCode,
                        r.salePrice,
                        r.dealerPrice,
                        r.distributorPrice]
                ).node();

            });

            tableN.draw();


        }else{
            $scope.noFound=true;
        }

        console.info('onSuccessItem', fileItem, response, status, headers);
    };


    uploaderNew.onBeforeUploadItem = function(item) {
        $scope.loading=true;
        console.info('onBeforeUploadItem', item);
    };
    uploaderNew.onSuccessItem = function(fileItem, response, status, headers) {

        $scope.success=true;
        if(response.status===200){
            $scope.noFound=false;

            var result=response,table=$('#tResultNew').DataTable(),tableN=$('#tResultFound').DataTable();
            table.clear();
            tableN.clear();

            $scope.totalData=response.totalItems;
            $scope.totalDataSuccess=response.totalSuccess;
            $scope.totalDataFail=response.totalFail;

            result.dataSuccess.forEach(function(r,index) {

                var row = table.row.add(
                    [ r.id,
                        '<a target="_blank" href="https://www.amazon.com/dp/'+r.code+'">'+r.code+'</a>',
                        '<a target="_blank" href="http://www.pyleaudio.com/sku/'+r.sku+'">'+r.sku+'</a>',
                        r.hsCode,
                        r.quantity,
                        r.brand,
                        r.price,
                        r.salePrice,
                        r.dealerPrice,
                        r.distributorPrice]
                ).node();

                /*if(r.Cost.utilityStatus===false){
                    $(row).addClass( 'bg-danger' );
                }else if(r.Cost.utilityStatus==="none"){
                    $(row).addClass( 'bg-warning' );
                }else if(r.Cost.utilityStatus==="duplicated"){
                    $(row).addClass( 'bg-info' );
                }*/

            });

            table.draw();
            $scope.reqUpdate={data:result.dataSuccess};

            result.dataFail.forEach(function(r,index) {

                var row2 = tableN.row.add(
                    [ r.brand,
                        '<a target="_blank" href="http://www.pyleaudio.com/sku/'+r.sku+'">'+r.sku+'</a>',
                        r.quantity,
                        r.price,
                        r.hsCode,
                        r.salePrice,
                        r.dealerPrice,
                        r.distributorPrice]
                ).node();

            });

            tableN.draw();


        }else{
            $scope.noFound=true;
        }

        console.info('onSuccessItem', fileItem, response, status, headers);
    };


    $scope.failWsConsume = function () {

        $scope.errorWs = true;
        $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
        alert($scope.errorWsMsg);

    };
    $scope.refresh = function(){
        $window.location.reload();
    };

}

function amzDown($scope, $api, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize,FileUploader,$rdcLocalStorage,$translate,$filter) {


    $scope.loading=false;
    $scope.success=false;
    $scope.fail=false;
    $scope.result=[];
    $scope.totalData=0;
    $scope.totalDataSuccess=0;
    $scope.totalDataFail=0;

    var uploader = $scope.uploader = new FileUploader({
        url: $globalParameters.AWSUrl+'upload',
        formData: [{
            dir:"amzDetailItem"
        }]
    });

    // FILTERS
    // only excel file
    uploader.filters.push({
            name: 'excelFile',
        fn: function(item, options) {
            console.log(item);
            return item.type==="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        }
    });

    // a sync filter
    uploader.filters.push({
        name: 'syncFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            console.log('syncFilter');
            return this.queue.length < 10;
        }
    });

    // an async filter
    uploader.filters.push({
        name: 'asyncFilter',
        fn: function(item /*{File|FileLikeObject}*/, options, deferred) {
            console.log('asyncFilter');
            setTimeout(deferred.resolve, 1e3);
        }
    });

    // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        $scope.loading=true;
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {

        $scope.success=true;
        if(response.status===200){
            $scope.noFound=false;

            var result=response,table=$('#tResultAmzDown').DataTable(),tableN=$('#tResultAmzDownN').DataTable();
            table.clear();
            tableN.clear();

            $scope.totalData=response.totalItems;
            $scope.totalDataSuccess=response.totalSuccess;
            $scope.totalDataFail=response.totalFail;

            result.dataSuccess.forEach(function(r,index) {
                var images={one:"",two:"",three:"",four:""}

                if(r.amzResult.Images[0]){
                    images.one=r.amzResult.Images[0].URL;
                }
                if(r.amzResult.Images[1]){
                    images.two=r.amzResult.Images[1].URL;
                }
                if(r.amzResult.Images[2]){
                    images.three=r.amzResult.Images[2].URL;
                }
                if(r.amzResult.Images[3]){
                    images.four=r.amzResult.Images[3].URL;
                }

                var row = table.row.add(
                    [
                        '<a target="_blank" href="https://www.amazon.com/dp/'+r.amzResult.Code+'">'+r.amzResult.Code+'</a>',
                        r.p1,
                        r.p2,
                        r.p3,
                        r.p4,
                        images.one,
                        images.two,
                        images.three,
                        images.four,
                        r.amzResult.Package_length,
                        r.amzResult.Package_height,
                        r.amzResult.Package_width,
                        r.amzResult.Package_weight,
                        r.amzResult.Price
                    ]
                ).node();

                /*if(r.Cost.utilityStatus===false){
                    $(row).addClass( 'bg-danger' );
                }else if(r.Cost.utilityStatus==="none"){
                    $(row).addClass( 'bg-warning' );
                }else if(r.Cost.utilityStatus==="duplicated"){
                    $(row).addClass( 'bg-info' );
                }*/

            });

            table.draw();
            $scope.reqUpdate={data:result.dataSuccess};

            result.dataFail.forEach(function(r,index) {

                var row2 = tableN.row.add(
                    [
                        '<a target="_blank" href="https://www.amazon.com/dp/'+r.amzResult.Code+'">'+r.amzResult.Code+'</a>',
                        r.p1,
                        r.p2,
                        r.p3,
                        r.p4,
                        "",
                        "",
                        "",
                        "",
                        0,
                        0,
                        0,
                        0,
                        r.amzResult.Price
                    ]
                ).node();

            });

            tableN.draw();


        }else{
            $scope.noFound=true;
        }

        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        $scope.loading=false;
        console.info('onCompleteAll');
    };

    //console.info('uploader', uploader);


    $scope.failWsConsume = function () {

        $scope.errorWs = true;
        $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
        alert($scope.errorWsMsg);

    };
    $scope.refresh = function(){
        $window.location.reload();
    };

}

function bti($scope, $api, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize) {

    $scope.loading=false;
    $scope.success=false;
    $scope.fail=false;
    $scope.result=[];
    $scope.totalData=0;


    $scope.submitBtiForm = function (isValid) {
        $scope.submitted = true;
        if(isValid){
            $scope.loading=true;

            $api.btiProducts({}).then(function (resCat) {

                var result=resCat.data,table=$('#tResult').DataTable();

                if(resCat.status===200 && result.length>0){

                    result.forEach(function(r,index) {

                        if(!r.Code){
                            r.Code="99999";
                        }
                        var row = table.row.add(
                            [
                                "", // Id Interno A
                                r.Title, // B
                                r.Brand, // C
                                r.DescriptionHTML, // D
                                r.Description, // E
                                "", //F
                                "", //G
                                r.Package_height, // H
                                r.Package_width, // I
                                r.Package_length, // J
                                r.Package_weight, // K
                                "", // L
                                r.Category, //M

                                r.Price, // N
                                r.ListPrice, // O Sale Price
                                r.Price, // P
                                r.LowPrice, // Q DistributorPrice
                                "", // R
                                "", // S

                                r.Code, // T
                                r.Code, // U
                                "", // V
                                "", // W
                                r.Size, // X
                                r.Quantity, // Y
                                "", // Z
                                "", //AA
                                "", //AB

                                r.Code, // AC Model
                                0, // AD
                                1, // AE
                                r.Color, // AF
                                "USA", // AG
                                r.Item_weight, // AH
                                "", // AI

                                r.LargeImage, // AJ
                                "", //AK
                                "", //AL
                                "", //AM
                                "", //AN
                                "", //AO
                                "", //AP
                                "", //AQ

                                r.Code, //AR
                                "", //AS
                                "", //AT
                                r.Notes, //AU
                                "", //AV
                                "" //AW
                            ]
                        ).node();

                        console.log("Indice ",index, " of ",result.length);

                        if(index+1===result.length){
                            $scope.loading=false;
                            $scope.success=true;
                            $scope.noFound=false;
                            $scope.totalData=result.length;
                        }

                    });

                    table.draw();
                }else{
                    $scope.noFound=true;
                }

            }, function(){
                $scope.failWsConsume();
            });
        }
    };

    $scope.failWsConsume = function () {

        $scope.errorWs = true;
        $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
        alert($scope.errorWsMsg);

    };
    $scope.refresh = function(){
        $window.location.reload();
    };

}

function quoteProduct($scope, $api, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize, FileUploader) {

    $scope.qProducts = [{value: 1}];
    $scope.question={
        toEmail:"aleida@micompraexpress.com",
        //toEmail:$globalParameters.emailOwner,
        marketplace:"Mi Compra Express",
        products:[]
    };
    $scope.product=[{
        quantity:1
    }];
    $scope.loadingQF=false;
    $scope.successQuoteForm=false;
    $scope.sinceQuoteForm=false;

    $scope.addProduct = function () {

        const cont = $scope.qProducts.length;
        $scope.qProducts.push({value: cont+1});

        $scope.product[cont] = {
            quantity : 1
        };

    };


    var uploader = $scope.uploader = new FileUploader({
        url: $globalParameters.AWSUrl+'upload/file',
        formData: [{
            dir:"quotFormStore",
            date:date.format(new Date(),'DDMMYYYY_HHmmss')
        }]
    });


    $scope.submitQuestionForm = function (isValid) {
        $scope.submitted = true;
        // check to make sure the form is completely valid
        if (isValid) {

            $scope.loading = true;
            $scope.loadingQF = true;

            $('#quoteProductModal').modal('hide');
            $('#loadingModal').modal('show');

            if($scope.uploader.queue.length>0){

                $.each($scope.uploader.queue,function (index, value) {
                    $scope.uploader.queue[index].upload();
                    $scope.product[index].image="https://anicam.ws:3006/files/"+value.formData[0].dir+'_'+value.formData[0].date+'_'+value.file.name;

                    if(index===$scope.uploader.queue.length-1){
                        console.log($scope.uploader.queue);
                        $scope.question.products=$scope.product;
                        console.log($scope.question);

                        $api.quoteForm($scope.question).then(function (r) {
                            console.log(r);
                            $scope.loadingQF = false;
                            $scope.successQuoteForm=true;

                            $scope.loading = false;
                            $scope.sinceQuoteForm=true;
                            $scope.successTit="Cotización enviada correctamente";
                            $scope.successMsg='Tu cotización se ha enviado de forma satisfactoria, se enviara una respuesta durante las proximas 48 horas al correo: '+ $scope.question.Email;
                            $('#loadingModal').modal('hide');
                            $timeout(function () {
                                $('#successModal').modal('show');
                            }, 100);

                        }, function () {
                            // error
                        });

                    }
                });
            }else{
                $scope.question.products=$scope.product;
                console.log($scope.question);
                $api.quoteForm($scope.question).then(function (r) {
                    console.log(r);
                    $scope.loadingQF = false;
                    $scope.successQuoteForm=true;

                    $scope.loading = false;
                    $scope.sinceQuoteForm=true;
                    $scope.successTit="Cotización enviada correctamente";
                    $scope.successMsg='Tu cotización se ha enviado de forma satisfactoria, se enviara una respuesta durante las proximas 48 horas al email: '+ $scope.question.Email;
                    $('#loadingModal').modal('hide');
                    $timeout(function () {
                        $('#successModal').modal('show');
                    }, 100);

                }, function () {
                    // error
                });
            }

        }
    };

    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };


    $scope.failWsConsume = function () {

        $scope.errorWs = true;
        $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
        alert($scope.errorWsMsg);

    };
    $scope.refresh = function(){
        $window.location.reload();
    };

}

function internalServices($scope, $api, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize, FileUploader,$rdcLocalStorage,$translate,$filter) {

    const defLang = "es";
    $scope.btnVal=$filter('translate')('Validate');

    $scope.menus=[
        {
            title:"Amazon Services",
            icon:"https://www.anicamboxexpress.com/css/quotation/amz-icon.png",
            components:[
                {
                    title:"Tracking Update",
                    timesUsed:12,
                    description:"Update the tracking of a period of time of purchases made in Amazon to the administrator of Anicam",
                    uri:"/trackingupdate.html",
                    uploadDate:"12/07/2019"
                },
                {
                    title:"Compare Costs",
                    timesUsed:6,
                    description:"Compare Amazon costs with the order system registered in Anicam",
                    uri:"/amzcost.html",
                    uploadDate:"25/03/2019"
                },
                {
                    title:"Product Download",
                    timesUsed:3,
                    description:"Download products with description, weights and corresponding measures to an excel",
                    uri:"/amzproduct.html",
                    uploadDate:"25/03/2019"
                }
            ]
        },
        {
            title:"Yaxa Services",
            icon:"https://www.anicamboxexpress.com/css/quotation/logo-yaxa.png",
            components:[
                {
                    title:"Upload Products",
                    timesUsed:3,
                    description:"Upload products with description, weights and corresponding measures to an excel",
                    uri:"/yaxaproduct.html",
                    uploadDate:"09/03/2019"
                }
            ]
        },
        {
            title:"BTI Services",
            icon:"http://www.bti-usa.com/images/btiLogo.png",
            components:[
                {
                    title:"Product Download",
                    timesUsed:3,
                    description:"Download products with description, weights and corresponding measures to an excel",
                    uri:"/btiproduct.html",
                    uploadDate:"03/09/2019"
                }
            ]
        },
        {
            title:"SixSixOne Services",
            icon:"https://cdn.shopify.com/s/files/1/0090/8646/8155/files/661-New-Icon-Stack_181x.png?v=1554986211",
            components:[
                {
                    title:"Product Synchronization",
                    timesUsed:1,
                    description:"Synchronize products to B2B with description, weights and corresponding measures to an excel",
                    uri:"/ssosyncproduct.html",
                    uploadDate:"20/08/2019"
                },
                {
                    title:"Product Download",
                    timesUsed:3,
                    description:"Download products with description, weights and corresponding measures to an excel",
                    uri:"/ssoproduct.html",
                    uploadDate:"20/08/2019"
                }
            ]
        },
        {
            title:"Pyle Services",
            icon:"http://www.pyleaudio.com/images/logo.jpg",
            components:[
                {
                    title:"Product Download",
                    timesUsed:3,
                    description:"Download products with description, weights and corresponding measures to an excel",
                    uri:"/pyleproduct.html",
                    uploadDate:"25/10/2019"
                },
                {
                    title:"Update Stock",
                    timesUsed:3,
                    description:"Upload stock products, quantity and corresponding costs to an excel",
                    uri:"/pylestock.html",
                    uploadDate:"23/03/2020"
                }
            ]
        },
        {
            title:"Ebay Services",
            icon:"https://cdn2.iconfinder.com/data/icons/social-icons-circular-color/512/ebay-512.png",
            components:[
                {
                    title:"Tracking Update",
                    timesUsed:12,
                    description:"Update the tracking of a period of time of purchases made in eBay to the administrator of Anicam",
                    uri:"/trackingupdate.html",
                    uploadDate:"12/07/2019"
                },
                {
                    title:"Shopping Report",
                    timesUsed:1,
                    description:"EBay shopping reports with orders and filters",
                    uri:"/ebayreports.html",
                    uploadDate:"25/07/2019"
                }
            ]
        },
        {
            title:"Billing Services",
            icon:"https://marketinglokaal.nl/wp-content/uploads/2018/10/invoice.png",
            components:[
                {
                    title:"Shipping Quote - RedServi",
                    timesUsed:36,
                    description:"Check the value of a national shipment with the RedServi logistics operator with the option to generate the guide to the order number",
                    uri:"/generateguide.html",
                    uploadDate:"18/05/2018"
                },
                {
                    title:"Costs Report by Guide",
                    timesUsed:45,
                    description:"Consult the costs and details of an order by the guide number. This service only applies to Colombia with the logistic operator Red Servi",
                    uri:"/getcost.html",
                    uploadDate:"12/07/2019"
                },
                {
                    title:"Costs Report by Guide",
                    timesUsed:36,
                    description:"Consult the costs and details of an order by the guide number. This service only applies to Colombia with the logistic operator Red Servi",
                    uri:"/getcost.html",
                    uploadDate:"12/07/2019"
                }
            ]
        },
        {
            title:"Pre-Alert Services",
            icon:"https://cdn0.iconfinder.com/data/icons/business-437/128/Business-34-512.png",
            components:[
                {
                    title:"Massive Pre-alerts",
                    timesUsed:14,
                    description:"Service to pre-alert a bulk number of packages, each package will correspond to an order number",
                    uri:"/prealertplus.html",
                    uploadDate:"12/03/2019"
                },
                {
                    title:"Pre purchase alert",
                    timesUsed:8,
                    description:"Service to pre-alert a wholesale quantity of purchases, each purchase will correspond to an order number",
                    uri:"/prealertpurchases.html",
                    uploadDate:"04/06/2019"
                }
            ]
        },
        {
            title:"Picture Services",
            icon:"https://cdn0.iconfinder.com/data/icons/business-437/128/Business-34-512.png",
            components:[
                {
                    title:"Massive Pre-alerts",
                    timesUsed:14,
                    description:"Service to pre-alert a bulk number of packages, each package will correspond to an order number",
                    uri:"/prealertplus.html",
                    uploadDate:"12/03/2019"
                }
            ]
        }
    ];

    $scope.languages=[
        {
            code:"es",
            text:"Español",
            uriFlag:"//www.anicamboxexpress.com/css/quotation/spain.png"
        },
        {
            code:"en",
            text:"English",
            uriFlag:"//www.anicamboxexpress.com/css/quotation/united-kingdom.png"
        }
    ];

    if ($rdcLocalStorage.get('dataLang') !== null && $rdcLocalStorage.get('dataLang') !== false) {
        $translate.use($rdcLocalStorage.get('dataLang').code);
    }else{
        $translate.use(defLang);
    }

    $.each($scope.languages, function (index, value) {
        if(value.code===$translate.use()){
            $scope.language=value;
        }
    });

    $scope.loading=false;
    $scope.success=false;
    $scope.fail=false;
    $scope.result=[];
    $scope.totalData=0;
    $scope.cost={
        trm:null
    };

    $scope.validUsrStatus=false;
    $scope.terminateAlr=false;


    $scope.failWsConsume = function () {

        $scope.errorWs = true;
        $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
        alert($scope.errorWsMsg);

    };
    $scope.refresh = function(){
        $window.location.reload();
    };

}

function apm($scope, $api, $parseXML, $formatNumber, $rellSelect,$globalParameters,$window,$timeout,$capitalize, FileUploader,$rdcLocalStorage,$translate,$filter) {

    const defLang = "es";
    $scope.btnVal=$filter('translate')('Validate');

    $scope.menus=[
        {
            title:"Amazon Services",
            icon:"https://www.anicamboxexpress.com/css/quotation/amz-icon.png",
            components:[
                {
                    title:"Tracking Update",
                    timesUsed:12,
                    description:"Update the tracking of a period of time of purchases made in Amazon to the administrator of Anicam",
                    uri:"/trackingupdate.html",
                    uploadDate:"12/07/2019"
                },
                {
                    title:"Compare Costs",
                    timesUsed:6,
                    description:"Compare Amazon costs with the order system registered in Anicam",
                    uri:"/amzcost.html",
                    uploadDate:"25/03/2019"
                },
                {
                    title:"Product Download",
                    timesUsed:3,
                    description:"Download products with description, weights and corresponding measures to an excel",
                    uri:"/amzproduct.html",
                    uploadDate:"25/03/2019"
                }
            ]
        },
        {
            title:"Yaxa Services",
            icon:"https://www.anicamboxexpress.com/css/quotation/logo-yaxa.png",
            components:[
                {
                    title:"Upload Products",
                    timesUsed:3,
                    description:"Upload products with description, weights and corresponding measures to an excel",
                    uri:"/yaxaproduct.html",
                    uploadDate:"09/03/2019"
                }
            ]
        },
        {
            title:"BTI Services",
            icon:"http://www.bti-usa.com/images/btiLogo.png",
            components:[
                {
                    title:"Product Download",
                    timesUsed:3,
                    description:"Download products with description, weights and corresponding measures to an excel",
                    uri:"/btiproduct.html",
                    uploadDate:"03/09/2019"
                }
            ]
        },
        {
            title:"SixSixOne Services",
            icon:"https://cdn.shopify.com/s/files/1/0090/8646/8155/files/661-New-Icon-Stack_181x.png?v=1554986211",
            components:[
                {
                    title:"Product Synchronization",
                    timesUsed:1,
                    description:"Synchronize products to B2B with description, weights and corresponding measures to an excel",
                    uri:"/ssosyncproduct.html",
                    uploadDate:"20/08/2019"
                },
                {
                    title:"Product Download",
                    timesUsed:3,
                    description:"Download products with description, weights and corresponding measures to an excel",
                    uri:"/ssoproduct.html",
                    uploadDate:"20/08/2019"
                }
            ]
        },
        {
            title:"Pyle Services",
            icon:"http://www.pyleaudio.com/images/logo.jpg",
            components:[
                {
                    title:"Product Download",
                    timesUsed:3,
                    description:"Download products with description, weights and corresponding measures to an excel",
                    uri:"/pyleproduct.html",
                    uploadDate:"25/10/2019"
                },
                {
                    title:"Update Stock",
                    timesUsed:3,
                    description:"Upload stock products, quantity and corresponding costs to an excel",
                    uri:"/pylestock.html",
                    uploadDate:"23/03/2020"
                }
            ]
        },
        {
            title:"Ebay Services",
            icon:"https://cdn2.iconfinder.com/data/icons/social-icons-circular-color/512/ebay-512.png",
            components:[
                {
                    title:"Tracking Update",
                    timesUsed:12,
                    description:"Update the tracking of a period of time of purchases made in eBay to the administrator of Anicam",
                    uri:"/trackingupdate.html",
                    uploadDate:"12/07/2019"
                },
                {
                    title:"Shopping Report",
                    timesUsed:1,
                    description:"EBay shopping reports with orders and filters",
                    uri:"/ebayreports.html",
                    uploadDate:"25/07/2019"
                }
            ]
        },
        {
            title:"Billing Services",
            icon:"https://marketinglokaal.nl/wp-content/uploads/2018/10/invoice.png",
            components:[
                {
                    title:"Servientrega Costs and Business Lines",
                    timesUsed:39,
                    description:"Compare the costs, total value and obtain the business lines of anicam with the Servientrega reports",
                    uri:"/servientregacosts.html",
                    uploadDate:"22/06/2018"
                },
                {
                    title:"Shipping Quote - RedServi",
                    timesUsed:36,
                    description:"Check the value of a national shipment with the RedServi logistics operator with the option to generate the guide to the order number",
                    uri:"/generateguide.html",
                    uploadDate:"18/05/2018"
                },
                {
                    title:"Costs Report by Guide",
                    timesUsed:45,
                    description:"Consult the costs and details of an order by the guide number. This service only applies to Colombia with the logistic operator Red Servi",
                    uri:"/getcost.html",
                    uploadDate:"12/07/2019"
                },
                {
                    title:"Costs Report by Guide",
                    timesUsed:36,
                    description:"Consult the costs and details of an order by the guide number. This service only applies to Colombia with the logistic operator Red Servi",
                    uri:"/getcost.html",
                    uploadDate:"12/07/2019"
                }
            ]
        },
        {
            title:"Pre-Alert Services",
            icon:"https://cdn0.iconfinder.com/data/icons/business-437/128/Business-34-512.png",
            components:[
                {
                    title:"Massive Pre-alerts",
                    timesUsed:14,
                    description:"Service to pre-alert a bulk number of packages, each package will correspond to an order number",
                    uri:"/prealertplus.html",
                    uploadDate:"12/03/2019"
                },
                {
                    title:"Pre purchase alert",
                    timesUsed:8,
                    description:"Service to pre-alert a wholesale quantity of purchases, each purchase will correspond to an order number",
                    uri:"/prealertpurchases.html",
                    uploadDate:"04/06/2019"
                }
            ]
        }
    ];

    $scope.languages=[
        {
            code:"es",
            text:"Español",
            uriFlag:"//www.anicamboxexpress.com/css/quotation/spain.png"
        },
        {
            code:"en",
            text:"English",
            uriFlag:"//www.anicamboxexpress.com/css/quotation/united-kingdom.png"
        }
    ];

    if ($rdcLocalStorage.get('dataLang') !== null && $rdcLocalStorage.get('dataLang') !== false) {
        $translate.use($rdcLocalStorage.get('dataLang').code);
    }else{
        $translate.use(defLang);
    }

    $.each($scope.languages, function (index, value) {
        if(value.code===$translate.use()){
            $scope.language=value;
        }
    });

    $scope.loading=false;
    $scope.success=false;
    $scope.fail=false;
    $scope.result=[];
    $scope.totalData=0;
    $scope.ms={
        loadingDown:false,
        totalProductsCat:0,
        catSel:""
    };
    $scope.ac={
        loading:false,
        success:false,
        fail:false
    };
    $scope.qCategories=[];
    $scope.allCategories=[];
    $scope.qSubCategories=[];
    $scope.qSubCategories2=[];
    $scope.cost={
        trm:null
    };

    $scope.updateSubCats = function (val,nu) {
        if(nu){
            $scope.qSubCategories2=[];
            $scope.ms.subcat2="";

            $api.listB2BCategories({idCategoryRoot:val}).then(function (resCat) {
                if(resCat.status!==404){
                    $.each(resCat.data, function(index, value){
                        $scope.qSubCategories2.push({name:value.title,value:value.id});
                    });
                }
            }, function(){
                $scope.failWsConsume();
            });
        }else{
            $scope.qSubCategories=[];
            $scope.ms.subcat="";
            $scope.qSubCategories2=[];
            $scope.ms.subcat2="";

            $api.listB2BCategories({idCategoryRoot:val}).then(function (resCat) {
                $.each(resCat.data, function(index, value){
                        $scope.qSubCategories.push({name:value.title,value:value.id});
                });
            }, function(){
                $scope.failWsConsume();
            });
        }

    };

    $scope.setRootCategory = function() {
        var x=$("#catB2b option:selected").text().split(">");

        for(var i=0; i<x.length;i++){
            console.log(x[i]);
            if(i+1===x.length){
                $scope.ac.catB2bRoot=x[i];
            }
        }
    };

    $api.listB2BCategories({idCategoryRoot:0}).then(function (resCat) {

        $.each(resCat.data, function(index, value){

            $scope.allCategories.push({name:value.title,value:value.id});

            $api.listB2BCategories({idCategoryRoot:value.id}).then(function (resCat0) {

                if(resCat0.status===200){
                    $.each(resCat0.data, function(index0, value0){

                        $scope.allCategories.push({name:value.title+'>'+value0.title,value:value0.id});

                        $api.listB2BCategories({idCategoryRoot:value0.id}).then(function (resCat1) {
                            if(resCat1.status===200){
                                $.each(resCat1.data, function(index1, value1){

                                    $scope.allCategories.push({name:value.title+'>'+value0.title+'>'+value1.title,value:value1.id});

                                });
                            }

                        }, function(){
                            //$scope.failWsConsume();
                        });

                    });
                }

            }, function(){
                //$scope.failWsConsume();
            });

        });

    }, function(){
        //$scope.failWsConsume();
    });

    $api.listB2BCategories({idCategoryRoot:0}).then(function (resCat) {

        $.each(resCat.data, function(index, value){
            $scope.qCategories.push({name:value.title,value:value.id})

        });

    }, function(){
        //$scope.failWsConsume();
    });

    $scope.searchProducts = function (cat) {
        $scope.ms.loadingDown=true;
        $scope.ms.catSel=cat;
        var cont=0,contR=0,table=$('#tResultCat').DataTable();

            $api.getProductsB2B({cat:cat}).then(function (result) {

                table.clear();
                var data=result.data[0].items;

                data.forEach(function(r,index) {
                    var brand=r.brand,arc=r.hs_code;


                    if(!arc || !brand){
                        if(brand==="PyleHome"){
                            brand="PYLE-HOME";
                        }else if(brand==="Pyle" || !brand){
                            brand="PYLE";
                        }else if(brand==="PylePro"){
                            brand="PYLE-PRO";
                        }
                        arc="9807.20.00.00";
                        var row = table.row.add([
                            '<a target="_blank" href="http://b2b.anicamenterprises.com/umbraco#/content/content/edit/'+r.id+'">'+r.id+'</a>',
                            brand,
                            '<a target="_blank" href="http://www.pyleaudio.com/sku/'+r.seller_sku+'">'+r.seller_sku+'</a>',
                            r.quantity,
                            r.price,
                            arc,
                            0.00,
                            0.00,
                            0.00]).node();
                        contR++;
                    }

                    cont++;

                    if(index+1===data.length){
                        $scope.ms.loadingDown=false;
                        table.draw();
                        $scope.ms.totalProductsCat=cont;
                        $scope.ms.totalProductsCatAva=contR;
                    }

                });

            }, function(){
                table.clear();
                $scope.ms.loadingDown=false;
            });
    };

    /* Acciones de subir archivo Json **/

    var now = new Date(),dateNowF=$filter('date')(now, "ddMMyyyyhhmmss","es_CO");
    $scope.jsonModel={
        ei:''
    };

    var up = $scope.up = new FileUploader({
        url: $globalParameters.AWSUrl+'upload/json',
        formData: [{
            dir:"jsonPh",
            ei:'',
            dr:dateNowF
        }]
    });

    // only excel file
    up.filters.push({
        name: 'jsFile',
        fn: function(item, options) {
                return item.type==="application/json";
        }
    });

    up.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    up.onAfterAddingFile = function(fileItem) {
        $scope.up.formData[0].ei=$scope.jsonModel.ei;
        up.formData[0].ei=$scope.jsonModel.ei;
        fileItem.formData[0].ei=$scope.jsonModel.ei;
        console.log(up.formData);
        console.info('onAfterAddingFile', fileItem);
    };
    up.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    up.onBeforeUploadItem = function(item) {
        $scope.loading=true;
        console.info('onBeforeUploadItem', item);
    };
    up.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    up.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    up.onSuccessItem = function(fileItem, response, status, headers) {

        $scope.success=true;
        /*if(response.table.length>0){
            $scope.noFound=false;
            var result=response.table,table = $('#tResult').DataTable();

            $scope.totalData=result.length;
            result.forEach(function(r,index) {

                var row = table.row.add(
                    [ '<a target="_blank" href="https://admin.anicamenterprises.com/orders/redirect.aspx?op=update&OrderId='+r.OrderNumber+'&tabs=6">'+r.OrderNumber+'</a>',
                        r.BusinessLine,
                        r.ExternalGuide,
                        '<a target="_blank" href="https://redservi.almalogix.com/distribucion/scripts/procesamientoEnvios/pro00007.php?numero='+r.ExternalGuide+'"><img style="height: 35px" src="http://anicamboxexpress.com/css/quotation/attach.png" alt="Trazabilidad"></a>',
                        '<a target="_blank" href="https://guias.almalogix.com/img_guias//'+r.ExternalGuide+'.jpg"><img style="height: 35px" src="http://anicamboxexpress.com/css/quotation/attach.png" alt="Prueba de Entrega"></a>',
                        r.Cost.Dien,
                        r.Cost.Description,
                        r.Cost.SalePrice,
                        r.Cost.CostPrice,
                        r.Cost.utilityPrice,
                        r.Consignee.ConsigneeName,
                        r.Consignee.ConsigneeState,
                        r.Consignee.ConsigneeCity,
                        r.Consignee.ConsigneeAddress,
                        '<span>'+r.Consignee.PackageWeight+' Lb</span><br><span>'+(r.Consignee.PackageWeight*0.453592)+' Kl</span>',
                        r.Consignee.PackageHeight,
                        r.Consignee.PackageLength,
                        r.Consignee.PackageWidth ]
                ).node();

                if(r.Cost.utilityStatus===false){
                    $(row).addClass( 'bg-danger' );
                }else if(r.Cost.utilityStatus==="none"){
                    $(row).addClass( 'bg-warning' );
                }else if(r.Cost.utilityStatus==="duplicated"){
                    $(row).addClass( 'bg-info' );
                }

            });

            table.draw();


        }else{
            $scope.noFound=true;
        }*/
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    up.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    up.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    up.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    up.onCompleteAll = function() {
        $scope.loading=false;
        console.info('onCompleteAll');
    };

    $scope.toHideNav=function(x,el,cli){
        if(x==="two"){
            $('a').removeClass("active");
            $('li').removeClass("active");
            $(el).addClass("active");
            $('li.'+cli).addClass("active");
            $("a[data-toggle~='collapse']").addClass("collapsed");
            $("div.collapse").removeClass("show");
        }else if(x==="one"){
            $('a.'+x).removeClass("active");
            $('li.'+x).removeClass("active");
            $(el).addClass("active");
            $('li.'+cli).addClass("active");
        }
    };

    $scope.failWsConsume = function () {

        $scope.errorWs = true;
        $scope.errorWsMsg = 'Alguno de los servidores se encuentra en mantenimiento por favor esperar unos minutos e intentarlo nuevamente.';
        alert($scope.errorWsMsg);

    };
    $scope.refresh = function(){
        $window.location.reload();
    };

}

/**
 * translateCtrl - Controller for translate any words
 */
function translateCtrl($translate, $scope,$rdcLocalStorage) {
    $scope.changeLanguage = function (langKey) {
        console.log(langKey);
        $translate.use(langKey.code);
        $rdcLocalStorage.set('dataLang',langKey);
        $scope.language=langKey;
    };
}

/**
 *
 * @return String
 */
function getParameter() {
    this.byName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };

}

function $globalParameters(getParameter) {

    var business_line_id=$('span#BusinessLineId').text(),user_id=$('span#UserId').text();

    if(getParameter.byName('bl')){
        business_line_id = getParameter.byName('bl');
    }

    if(getParameter.byName('usr')){
        user_id = getParameter.byName('usr');
    }

        return  {
            user_id : user_id,
            business_line_id : business_line_id,
            order_type : "260221E9-D6B7-4517-A103-5D6C47058CC3",
            url : "http://merlin.anicamenterprises.com/services/",
            AnUrl: "https://api.anicamenterprises.com/",
            AWSUrl: "https://anicam.ws:3006/",
            WSUrl: "https://ws.anicam.ws:4005/",
            //AWSUrl: "https://anicam.ws:3005/",
            AWSUrlTest : "http://dev.awsanicam.com:3000/",
            APIUrl : "https://api.anicamenterprises.com/",
            api_client : $('span#APIClient').text(),
            api_pswd : $('span#APISecret').text(),
            api_grant_type : 'password',
            user_redservi : "Anicam",
            pswd_redservi : "YjMxYmZhZGNlZjYzMmUxZjA2MjUyY2M2MzNiMDA1M2IwODdhZTc1YQ==",
            url_redservi_test : "https://redservipruebas.almalogix.com:443/distribucion/webservices/",
            url_redservi : "https://redservi.almalogix.com:443/distribucion/webservices/",
            mt_redservi : "2",  // Variable para metodo de transporte
            fp_redservi : "1",  // Varaible para forma de pago
            origen_redservi : "11001", // Variable de ciudad de origen
            nit_cliente_redservi : "830129657", // Variable de NIT del cliente en este caso ae
            nombre_cliente_redservi : "E-COMMERCE ANICAM - COTIZADOR",
            no_caja_redservi : "359",
            place_default : 3
        };

}

function $parseXML() {
    return function (xml) {
            var dom = null;
            if (window.DOMParser) {
                try {
                    dom = (new DOMParser()).parseFromString(xml, "text/xml");
                }
                catch (e) { dom = null; }
            }
            else if (window.ActiveXObject) {
                try {
                    dom = new ActiveXObject('Microsoft.XMLDOM');
                    dom.async = false;
                    if (!dom.loadXML(xml)) // parse error ..

                        window.alert(dom.parseError.reason + dom.parseError.srcText);
                }
                catch (e) { dom = null; }
            }
            else
                alert("cannot parse xml string!");
            return dom;
    }
}

function $formatNumber(){
    return function (input)
    {
        var num = input.replace(/\./g,'');
        if(!isNaN(num)){
            num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,');
            num = num.split('').reverse().join('').replace(/^[\.]/,'');
            return num;
        }

        else{ alert('Solo se permiten numeros');
            return input.replace(/[^\d\.]*/g,'');
        }
    }
}

/**
 *
 * @return String
 */
function $rdcLocalStorage() {
    return {
        set: function(k, value){
            localStorage.setItem(k, JSON.stringify(value));
        },
        get: function(k){
            var data = localStorage[k];

            if(data === undefined) return false;

            return JSON.parse(data);
        },
        remove: function(k){
            localStorage.removeItem(k);
        }
    };
}

/**
 *
 * @return String
 */
function $rdcSessionStorage() {
    return {
        set: function(k, value){
            sessionStorage.setItem(k, JSON.stringify(value));
        },
        get: function(k){
            var data = sessionStorage[k];

            if(data === undefined) return false;

            return JSON.parse(data);
        },
        remove: function(k){
            sessionStorage.removeItem(k);
        }
    };
}


/**
 *
 * @return String
 */
function $GenerateParams() {
    return {

        get: function (data) {

            var params='', cont=0;

            if(Object.keys(data).length > 0){
                $.each(data, function(index, value){

                    params += index+"="+value;
                    cont ++;

                    cont === Object.keys(data).length ? params += "" : params += "&";

                });
            }
           return params;

        },

        token: function ($api,$globalParameters) {

            const dataT = {
                username: $globalParameters.api_client,
                password: $globalParameters.api_pswd,
                grant_type: $globalParameters.api_grant_type
            };

            $api.call('token','POST',dataT,true,true).then(function (rt){
                return rt;
            },function(){
                return false;
            });


        }

    };
}


/**
 *
 *
 */
function $api($http, $q, $rdcLocalStorage, $globalParameters, $parseXML,$GenerateParams) {

    const call = function (uri,method,data,json,params) {
        const deferred = $q.defer(), promise = deferred.promise;
        const url = $globalParameters.AnUrl + uri;
        if(params){
            data=$GenerateParams.get(data);
        }
        $http({
            method: method,
            url: url,
            data: data,
            json: json,
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        }).success(function (r) {
            deferred.resolve(r);
        }).error(function (err) {
            deferred.resolve(err)
        });
        return promise;
    };

    const callTkn = function (uri,method,data,json,params,tkn) {
        const deferred = $q.defer(), promise = deferred.promise;
        const url = $globalParameters.AnUrl + uri;
        if(params){
            data=$GenerateParams.get(data);
        }
        //$http.defaults.headers.common['Authorization'] = 'Bearer ' + login
        //$http.defaults.headers.common.Authorization = 'Bearer ' + tkn;
        $http({
            method: method,
            url: url,
            data: data,
            withCredentials: false,
            cache: false,
            responseType: 'json',
            body : "",
            headers: {
                'Authorization' : 'Bearer '+tkn,
                'Access-Control-Allow-Credentials' : 'true',
                'Accept' : '*/*',
                'Access-Control-Allow-Headers' : 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).success(function (r) {
            deferred.resolve(r);
        }).error(function (err) {
            deferred.resolve(err)
        });
        return promise;
    };

    const getToken = function () {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl + 'token';
        const data = {
            username: $globalParameters.api_client,
            password: $globalParameters.api_pswd,
            grant_type: $globalParameters.api_grant_type
        };
        $http({
            method: "POST",
            url: url,
            data: $GenerateParams.get(data),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).success(function (r) {

            defered.resolve(r);

        }).error(function (err) {

            defered.resolve(err)
        });

        return promise;
    };

    /* Funcion para calcular el shipping a Colombia por tanto se  */
    const shippingCosts = function (data) {
        var defered = $q.defer(), promise = defered.promise;

        /*data.delivery_country = "1";
        data.delivery_state = "11";
        data.delivery_city ="11001";*/

        const url = $globalParameters.AWSUrl +"rates/shipping";


        $http({
            method: "POST",
            url: url,
            data: $GenerateParams.get(data),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };

    const getCountries = function () {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl + 'locations/countries';
        $http({
            method: "POST",
            url: url,
            data: '',
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        }).success(function (r) {
            defered.resolve(r);
        })
            .error(function (err) {
                defered.reject(err)
            });
        return promise;
    };

    const getCarrier = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl + 'carrier/'+data.country_code;
        $http({
            method: "GET",
            url: url,
            data: '',
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        }).success(function (r) {
            defered.resolve(r);
        })
            .error(function (err) {
                defered.reject(err)
            });
        return promise;
    };

    const getCountry = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl + 'locations/country';
        $http({
            method: "POST",
            url: url,
            data: data,
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        }).success(function (r) {
            defered.resolve(r);
        })
            .error(function (err) {
                defered.reject(err)
            });
        return promise;
    };

    const getStates = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl + 'locations/states';
        $http({
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            },
            method: "POST",
            url: url,
            data: data,
            json: true
        }).success(function (r) {
            defered.resolve(r);
        })
            .error(function (err) {
                defered.reject(err)
            });
        return promise;
    };

    const getCities = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl + 'locations/cities';
        $http({
            method: "POST",
            url: url,
            data: data,
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        }).success(function (r) {
            defered.resolve(r);
        })
            .error(function (err) {
                defered.reject(err)
            });
        return promise;
    };

    const getCategory = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl+'tariff';
        $http({
            method: "POST",
            url: url,
            data: data,
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };

    const ssoCategories = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl+'ssone/categories';
        $http({
            method: "POST",
            url: url,
            data: JSON.parse(JSON.stringify(data)),
            json: true,
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };

    const listB2BCategories = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl+'b2b/listCategory';
        $http({
            method: "POST",
            url: url,
            data: JSON.parse(JSON.stringify(data)),
            json: true,
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };

    const getProductsB2B = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl+'b2b/getProductsCategory';
        $http({
            method: "POST",
            url: url,
            data: JSON.parse(JSON.stringify(data)),
            json: true,
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).success(function(r) {
            defered.resolve(r);
        }).error(function(err) {
                defered.reject(err)
            });
        return promise;
    };

    const pyleCategories = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl+'pyle/categories';
        $http({
            method: "POST",
            url: url,
            data: JSON.parse(JSON.stringify(data)),
            json: true,
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };

    const ssoProducts = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl+'ssone/products';
        $http({
            method: "POST",
            url: url,
            data: JSON.parse(JSON.stringify(data)),
            json: true,
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };

    const updateB2B = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl+'b2b/updateStock';
        $http({
            method: "POST",
            url: url,
            data: JSON.parse(JSON.stringify(data)),
            json: true,
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };

    const btiProducts = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl+'bti/products';
        $http({
            method: "POST",
            url: url,
            data: JSON.parse(JSON.stringify(data)),
            json: true,
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };

    const getTRM = function () {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl+'trm';
        $http({
            method: "POST",
            url: url,
            data: '',
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };

    const validateLocker = function (data) {
        var defered = $q.defer(), promise = defered.promise;

        const url = $globalParameters.AWSUrl + 'pre-alert/user/validate';

        $http({
            method: "POST",
            url: url,
            data: data,
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        }).success(function (r) {

            defered.resolve(r);

        }).error(function (err) {

            defered.resolve(err)
        });

        return promise;
    };

    const loginUser = function (data) {

        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl + 'user/login';

        $http({
            method: "POST",
            url: url,
            data: $GenerateParams.get(data),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).success(function (r, status, headers, config) {
            // console.log(config);
            defered.resolve(r);

        }).error(function (err) {
            defered.resolve(err)
        });

        return promise;
    };

    const detailUser = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl + 'user/detail';

        $http({
            method: "POST",
            url: url,
            data: data,
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        }).success(function (r, status, headers, config) {
            // console.log(config);
            defered.resolve(r);

        }).error(function (err) {
            defered.resolve(err)
        });

        return promise;
    };

    const validUser = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl + 'user/titan/validate';

        $http({
            method: "POST",
            url: url,
            data: data,
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        }).success(function (r, status, headers, config) {
            // console.log(config);
            defered.resolve(r);

        }).error(function (err) {
            defered.resolve(err)
        });

        return promise;
    };

    const tracing = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl + 'orders/tracking';

        $http({
            method: "POST",
            url: url,
            data: $GenerateParams.get(data),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).success(function (r, status, headers, config) {
            // console.log(config);
            defered.resolve(r);

        }).error(function (err) {
            defered.resolve(err)
        });

        return promise;
    };

    const quoteForm = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl + 'quoteForm';

        $http({
            method: "POST",
            url: url,
            data: JSON.parse(JSON.stringify(data)),
            json: true,
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        }).success(function (r, status, headers, config) {
            // console.log(config);
            defered.resolve(r);

        }).error(function (err) {
            defered.resolve(err)
        });

        return promise;
    };

    const quoteRedservi = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl + 'quotation/redservi';
        $http({
            method: "POST",
            url: url,
            data: JSON.parse(JSON.stringify(data)),
            json: true,
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        }).success(function (r, status, headers, config) {
            defered.resolve(r);
        }).error(function (err) {
            defered.resolve(err)
        });
        return promise;
    };

    const quoteServientrega = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl + 'quotation/servientrega';
        $http({
            method: "POST",
            url: url,
            data: JSON.parse(JSON.stringify(data)),
            json: true,
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        }).success(function (r, status, headers, config) {
            defered.resolve(r);
        }).error(function (err) {
            defered.resolve(err)
        });
        return promise;
    };

    const quoteEnvia = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl + 'quotation/envia';
        $http({
            method: "POST",
            url: url,
            data: JSON.parse(JSON.stringify(data)),
            json: true,
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        }).success(function (r, status, headers, config) {
            defered.resolve(r);
        }).error(function (err) {
            defered.resolve(err)
        });
        return promise;
    };

    const quoteDhl = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl + 'quotation/dhl';
        $http({
            method: "POST",
            url: url,
            data: JSON.parse(JSON.stringify(data)),
            json: true,
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        }).success(function (r, status, headers, config) {
            defered.resolve(r);
        }).error(function (err) {
            defered.resolve(err)
        });
        return promise;
    };

    const ebayOrders = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl + 'ebay/getOrders';

        $http({
            method: "POST",
            url: url,
            data: JSON.parse(JSON.stringify(data)),
            json: true,
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        }).success(function (r, status, headers, config) {
            // console.log(config);
            defered.resolve(r);

        }).error(function (err) {
            defered.resolve(err)
        });

        return promise;
    };

    const detailOrder = function (orderId) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.APIUrl+'v1/private/admin/order/'+orderId;

        $http({
            method: "GET",
            url: url,
            data: "",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).success(function (r, status, headers, config) {
            // console.log(config);
            defered.resolve(r);

        }).error(function (err) {
            defered.resolve(err)
        });

        return promise;
    };

    const addGuideCost = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl+'admin/updatecarrier';

        $http({
            method: "POST",
            url: url,
            data: JSON.parse(JSON.stringify(data)),
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        }).success(function (r, status, headers, config) {
            defered.resolve(r);
        }).error(function (err) {
            defered.resolve(err)
        });

        return promise;
    };

    const preAlert = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl + 'pre-alert/preorder';

        $http({
            method: "POST",
            url: url,
            data: JSON.parse(JSON.stringify(data)),
            json: true,
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        }).success(function (r, status, headers, config) {
            defered.resolve(r);
        }).error(function (err) {
            defered.resolve(err)
        });

        return promise;
    };

    const recoveryOrder = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl + 'pre-alert/recovery';

        $http({
            method: "POST",
            url: url,
            data: JSON.parse(JSON.stringify(data)),
            json: true,
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        }).success(function (r, status, headers, config) {
            defered.resolve(r);
        }).error(function (err) {
            defered.resolve(err)
        });

        return promise;
    };

    const updatePackage = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl + 'pre-alert/update';

        $http({
            method: "POST",
            url: url,
            data: JSON.parse(JSON.stringify(data)),
            json: true,
            headers: {
                'Content-Type': 'application/json; charset=UTF-8'
            }
        }).success(function (r, status, headers, config) {
            defered.resolve(r);
        }).error(function (err) {
            defered.resolve(err)
        });

        return promise;
    };

    const overrGuide = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.AWSUrl + 'override/guide';

        $http({
            method: "POST",
            url: url,
            data: $GenerateParams.get(data),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).success(function (r, status, headers, config) {
            defered.resolve(r);
        }).error(function (err) {
            defered.resolve(err)
        });

        return promise;
    };

    const validateAuth = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = "/services/shop/UserService.asmx/IsAuthenticated";
        $http({
            method: "POST",
            url: url,
            data: '',
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };

    const quotationShipments = function (body) {

        var defered = $q.defer(), promise = defered.promise;

        //const url = "https://redservipruebas.almalogix.com:443/distribucion/webservices/ws_cotizadorEnvios.php?wsdl";
        const url = $globalParameters.url_redservi+"ws_cotizadorEnvios.php?wsdl";

        $http({
            method: "POST",
            url: url,
            data: body,
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8; text/xml'
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };

    const quotationShipmentsT = function (body) {

        var defered = $q.defer(), promise = defered.promise;

        //const url = "https://redservipruebas.almalogix.com:443/distribucion/webservices/ws_cotizadorEnvios.php?wsdl";
        const url = $globalParameters.url_redservi_test+"ws_cotizadorEnvios.php?wsdl";

        $http({
            method: "POST",
            url: url,
            data: body,
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8; text/xml'
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };

    const registerShipments = function (body) {

        var defered = $q.defer(), promise = defered.promise;

        const url = $globalParameters.url_redservi+"ws_registroEnvios.php?wsdl";

        $http({
            method: "POST",
            url: url,
            data: body,
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8; text/xml'
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };

    const registerShipmentsT = function (body) {

        var defered = $q.defer(), promise = defered.promise;

        const url = $globalParameters.url_redservi_test+"ws_registroEnvios.php?wsdl";

        $http({
            method: "POST",
            url: url,
            data: body,
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8; text/xml'
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };

    /* Funcion para hacer el pago virtual */
    const virtualPayment = function (data) {
        var defered = $q.defer(), promise = defered.promise;

        const url = $globalParameters.url+"orders.asmx/VirtualPayment";

        $http({
            method: "POST",
            url: url,
            data: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };

    /* Función para hacer solicitud de la orden */
    const requestOrder = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        data.user_id = $globalParameters.user_id;
        data.business_line_id = $globalParameters.business_line_id;
        const url = $globalParameters.url+"orders.asmx/RequestOrder";

        $http({
            method: "POST",
            url: url,
            data: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };


    /* Función para añadir articulos a la orden */
    const requestItem = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        data.user_id = $globalParameters.user_id;
        data.business_line_id = $globalParameters.business_line_id;
        const url = $globalParameters.url+"orders.asmx/RequestItem";

        $http({
            method: "POST",
            url: url,
            data: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };

    /* Función para añadir costos de articulos a la orden */
    const orderCosts = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        data.user_id = $globalParameters.user_id;
        data.business_line_id = $globalParameters.business_line_id;
        const url = $globalParameters.url+"orders.asmx/OrderCosts";

        $http({
            method: "POST",
            url: url,
            data: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };

    /* Función para notificar al vendedor y cliente de la orden creada */
    const notificationOrder = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.url+"local/notifications.asmx/SendNotification ";

        $http({
            method: "POST",
            url: url,
            data: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };

    /* Función para notificar al vendedor y cliente de la orden creada */
    const setContable = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.url+"local/internal-products.asmx/SendToSapMerlinOrder";

        $http({
            method: "POST",
            url: url,
            data: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };

    /* Función para hacer tracking a una orden */
    const orderTracking = function (data) {
        var defered = $q.defer(), promise = defered.promise;
        const url = $globalParameters.url+"orders.asmx/RedServiOrderTracking";

        $http({
            method: "POST",
            url: url,
            data: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).success(function(r) {
            defered.resolve(r);
        })
            .error(function(err) {
                defered.reject(err)
            });
        return promise;
    };


    return {
        call : call,
        callTkn : callTkn,
        getToken : getToken,
        shippingCosts: shippingCosts,
        getCountries : getCountries,
        getCarrier : getCarrier,
        getCountry : getCountry,
        getStates : getStates,
        getCities : getCities,
        getTRM : getTRM,
        validateLocker : validateLocker,
        loginUser : loginUser,
        detailUser : detailUser,
        validUser : validUser,
        getCategory : getCategory,
        ssoCategories : ssoCategories,
        pyleCategories : pyleCategories,
        listB2BCategories : listB2BCategories,
        getProductsB2B : getProductsB2B,
        ssoProducts : ssoProducts,
        updateB2B : updateB2B,
        btiProducts : btiProducts,
        preAlert : preAlert,
        recoveryOrder : recoveryOrder,
        updatePackage : updatePackage,
        overrGuide : overrGuide,
        tracing: tracing,
        quoteForm: quoteForm,
        quoteRedservi: quoteRedservi,
        quoteServientrega: quoteServientrega,
        quoteEnvia: quoteEnvia,
        quoteDhl: quoteDhl,
        ebayOrders: ebayOrders,
        detailOrder : detailOrder,
        addGuideCost : addGuideCost,
        validateAuth : validateAuth,
        quotationShipments : quotationShipments,
        quotationShipmentsT : quotationShipmentsT,
        registerShipments : registerShipments,
        registerShipmentsT : registerShipmentsT,
        virtualPayment : virtualPayment,
        requestOrder : requestOrder,
        requestItem : requestItem,
        orderCosts : orderCosts,
        notificationOrder : notificationOrder,
        setContable : setContable,
        orderTracking : orderTracking
    }
}


function reverse(){
    return function(items) {
        return items.slice().reverse();
    };
}

function range(){
    return function(input, total) {
        total = parseInt(total);

        for (var i=1; i<total+1; i++) {
            input.push(i);
        }

        return input;
    };
}

function reduceCapitalize(){

    return function(string,char) {
        if(string !== undefined && char !== undefined){
            return string.substr(0,char);
        }else{
            return string;
        }
    };
}

function addSpace(){

    return function(string) {
        if(string !== undefined){
            return string.replace(/([A-Z])/g, ' $1').trim();
        }else{
            return string;
        }
    };
}

function capitalize(){
    return function(input) {
        if(input.indexOf(' ') !== -1){
            var inputPieces,
                i;

            input = input.toLowerCase();
            inputPieces = input.split(' ');

            for(i = 0; i < inputPieces.length; i++){
                inputPieces[i] = capitalizeString(inputPieces[i]);
            }

            return inputPieces.toString().replace(/,/g, ' ');
        }
        else {
            input = input.toLowerCase();
            return capitalizeString(input);
        }

        function capitalizeString(inputString){
            return inputString.substring(0,1).toUpperCase() + inputString.substring(1);
        }
    };
}

function myDirective(){
    return {
        require: 'ngModel'
}
}

function $rellSelect(){
    return function(total) {
        total = parseInt(total);
        var input = [];

        for (var i=1; i<total+1; i++) {
            input.push({name:i, value:i});
        }

        return JSON.parse(JSON.stringify(input));
    };
}

/**
 *
 * Pass all functions into module
 */
angular
    .module('anicamquote')
    .config(function ($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
    })
    .controller('MainCtrl', MainCtrl)
    .controller('AlertCtrl', AlertCtrl)
    .controller('CutGuide', CutGuide)
    .controller('RecoveryCtrl', RecoveryCtrl)
    .controller('getCostCtrl', getCost)
    .controller('amzTrackingCtrl', amzTracking)
    .controller('ebayTrackingCtrl', ebayTracking)
    .controller('servientregaCostCtrl', servientregaCost)
    .controller('settlementCostCtrl', settlementCost)
    .controller('prealertPlusCtrl', prealertPlus)
    .controller('yaxaExcelCtrl', yaxaExcel)
    .controller('prealertPurchasesPlusCtrl', prealertPurchasesPlus)
    .controller('amzCostCtrl', amzCost)
    .controller('ssOneCtrl', ssOne)
    .controller('ssOneSyncCtrl', ssOneSync)
    .controller('pyleAudioCtrl', pyleAudio)
    .controller('amzDownCtrl', amzDown)
    .controller('btiCtrl', bti)
    .controller('quoteProductCtrl', quoteProduct)
    .controller('internalServicesCtrl', internalServices)
    .controller('apmCtrl', apm)
    .controller('translateCtrl', translateCtrl)
    .directive('myDirective',myDirective)
    .service('reverse', reverse)
    .service('$rellSelect', $rellSelect)
    .service('getParameter', getParameter)
    .service('$rdcLocalStorage', $rdcLocalStorage)
    .service('$rdcSessionStorage', $rdcSessionStorage)
    .service('$GenerateParams', $GenerateParams)
    .service('$globalParameters', $globalParameters)
    .service('$parseXML', $parseXML)
    .service('$formatNumber', $formatNumber)
    .service('$reduceCapitalize', reduceCapitalize)
    .service('$capitalize', capitalize)
    .filter('reverse', reverse)
    .filter('capitalize', capitalize)
    .filter('range', range)
    .filter('reduceCapitalize', reduceCapitalize)
    .filter('addSpace', addSpace)
    .service('$api', $api);
