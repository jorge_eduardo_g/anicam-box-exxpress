/**
 * pageTitle - Directive for set Page title - mata title
 */
function firstImage() {
    return {
        restrict: 'E',
        template:'<div class="prod">\n' +
        '            <div class="clearfix">\n' +
        '                <%--<a href="<%=ProdItem.LargeImage %>" class="jqzoom" rel=\'gal1\' title="<%=ProdItem.Title %>">\n' +
        '                    <img src="<%=ProdItem.LargeImage %>" style="border: 4px solid #e5e5e5; max-width: 320px; max-height: 320px;">\n' +
        '                </a>--%>\n' +
        '                \n' +
        '                <a href="{{largeImage}}" class="jqzoom" rel=\'gal1\' title="{{title}}">\n' +
        '                    <img src="{{largeImage}}" style="border: 4px solid #e5e5e5; max-width: 320px; max-height: 320px;">\n' +
        '                </a>\n' +
        '            <div class="clearfix">\n' +
        '                <ul ng-if="imageSets.length > 0 " id=\'thumblist\' class=\'clearfix\'>\n' +
        '                <li ng-repeat="imgset in imageSets">\n' +
        '                <a class="{{imgset.LargeImage.URL == largeImage ? \'zoomThumbActive\' : \'\'}}" href="javascript:void(0);" rel="{gallery: \'gal1\', smallimage: \'{{imgset.LargeImage.URL}}\',\n' +
        '                    largeimage: \'{{imgset.LargeImage.URL}}\'}">\n' +
        '                <img src="{{imgset.SmallImage.URL}}" style="max-width: 50px; max-height: 50px"></a>\n' +
        '                </li>\n' +
        '                </ul>\n' +
        '            </div>\n' +
        '        </div>'
    };
}

function ngElevateZoom(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            console.log("Linking");

            //Will watch for changes on the attribute
            attrs.$observe('zoomImage',function(){
                linkElevateZoom();
            });

            function linkElevateZoom(){
                //Check if its not empty
                if (!attrs.zoomImage) return;
                element.attr('data-zoom-image',attrs.zoomImage);
                $(element).elevateZoom({
                    zoomWindowFadeIn: 500,
                    zoomWindowFadeOut: 500,
                    lensFadeIn: 500,
                    lensFadeOut: 500,
                    gallery:'gal1',
                    cursor: 'pointer',
                    galleryActiveClass: 'active',
                    imageCrossfade: true,
                    loadingIcon: 'http://www.elevateweb.co.uk/spinner.gif'
                });
            }
            linkElevateZoom();
        }
    };
}

function passwordVerify() {
    return {
        restrict: 'A', // only activate on element attribute
        require: '?ngModel', // get a hold of NgModelController
        link: function (scope, elem, attrs, ngModel) {
            if (!ngModel) return; // do nothing if no ng-model

            // watch own value and re-validate on change
            scope.$watch(attrs.ngModel, function () {
                validate();
            });

            // observe the other value and re-validate on change
            attrs.$observe('passwordVerify', function (val) {
                validate();
            });

            var validate = function () {
                // values
                var val1 = ngModel.$viewValue;
                var val2 = attrs.passwordVerify;

                // set validity
                ngModel.$setValidity('passwordVerify', val1 === val2);
            };
        }
    };
}

function usernameVerify() {
    return {
        restrict: 'A', // only activate on element attribute
        require: '?ngModel', // get a hold of NgModelController
        link: function (scope, elem, attrs, ngModel) {
            if (!ngModel) return; // do nothing if no ng-model

            // watch own value and re-validate on change
            scope.$watch(attrs.ngModel, function () {
                validate();
            });

            // observe the other value and re-validate on change
            attrs.$observe('usernameVerify', function (val) {
                validate();
            });

            var validate = function () {
                // values
                var val1 = ngModel.$viewValue;
                var val2 = attrs.usernameVerify;

                // set validity
                ngModel.$setValidity('usernameVerify', val1 === val2);
            };
        }
    };
}

function format($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;

            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue)
            });

            elem.bind('blur', function(event) {
                var plainNumber = elem.val().replace(/[^\d|\-+|\.+]/g, '');
                elem.val($filter(attrs.format)(plainNumber));
            });
        }
    };
}

/**
 *
 * Pass all functions into module
 */
angular
    .module('anicamquote')
    .directive('format', format)
    .directive('firstImage', firstImage)
    .directive('passwordVerify', passwordVerify)
    .directive('usernameVerify', usernameVerify)
    .directive('ngThumb', ['$window', function($window) {
        var helper = {
            support: !!($window.FileReader && $window.CanvasRenderingContext2D),
            isFile: function(item) {
                return angular.isObject(item) && item instanceof $window.File;
            },
            isImage: function(file) {
                var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        };

        return {
            restrict: 'A',
            template: '<canvas/>',
            link: function(scope, element, attributes) {
                if (!helper.support) return;

                var params = scope.$eval(attributes.ngThumb);

                if (!helper.isFile(params.file)) return;
                if (!helper.isImage(params.file)) return;

                var canvas = element.find('canvas');
                var reader = new FileReader();

                reader.onload = onLoadFile;
                reader.readAsDataURL(params.file);

                function onLoadFile(event) {
                    var img = new Image();
                    img.onload = onLoadImage;
                    img.src = event.target.result;
                }

                function onLoadImage() {
                    var width = params.width || this.width / this.height * params.height;
                    var height = params.height || this.height / this.width * params.width;
                    canvas.attr({ width: width, height: height });
                    canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
                }
            }
        };
    }])
    .directive('ngElevateZoom', ngElevateZoom);